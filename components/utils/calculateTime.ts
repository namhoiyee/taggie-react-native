const week = [
  '星期日',
  '星期一',
  '星期二',
  '星期三',
  '星期四',
  '星期五',
  '星期六',
];

export function formatTime(timeNow: number, msgTime: number) {
  const timePassed = timeNow - msgTime;
  const days = Math.floor(timePassed / 1000 / 60 / 60 / 24);

  if (days > 7) {
    return new Date(msgTime).toLocaleDateString();
  }
  if (days > 0) {
    return week[new Date(msgTime).getDay()];
  }
  const hour = Math.floor((timePassed / 1000 / 60 / 60) % 60);
  if (hour > 0) {
    return `${hour}小時前`;
  }
  const minute = Math.floor((timePassed / 1000 / 60) % 60);
  if (minute > 0) {
    return `${minute}分鐘前`;
  }
  return '現在';
  // const second = Math.floor(timePassed / 1000 % 60)
  // const millisecond = Math.floor(timePassed % 1000)

  // return `${days}:${hour}:${minute}:${second}:${millisecond}`
}

export function translateTime(date: string) {
  let hour = new Date(date).getHours();
  let minute = new Date(date).getMinutes();
  let hourForDisplace = hour % 12;
  if (hour % 12 === 0) {
    hourForDisplace = 12;
  }
  const translatedTime =
    hourForDisplace +
    ':' +
    ('0' + minute).slice(-2) +
    ' ' +
    (hour >= 12 ? 'PM' : 'AM');

  return translatedTime;
}
