import React from 'react';
import {useTheme} from 'react-native-paper';
import {createStackNavigator} from '@react-navigation/stack';
import WhoLikedYouScreen from '../screen/WhoLikedYouScreen';

const MatchHistoryStack = createStackNavigator();

export const MatchHistoryStackNavigator = ({_navigation}: any) => {
  const {colors} = useTheme();

  return (
    <MatchHistoryStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#efefef',
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
      }}>
      <MatchHistoryStack.Screen
        name="MatchHistory"
        component={WhoLikedYouScreen}
        options={{
          headerShown: false,
        }}
      />
    </MatchHistoryStack.Navigator>
  );
};
