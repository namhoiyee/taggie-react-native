import {CrushFriendInfo} from './state';

export function failed(msg: string) {
  return {
    type: '@@Crush/failed' as const,
    msg,
  };
}

export function crushHappened(crushHappenedFriend: CrushFriendInfo) {
  return {
    type: '@@Crush/crushHappened' as const,
    crushHappenedFriend,
  };
}

export function crushHappenHandled() {
  return {
    type: '@@Crush/crushHappenHandled' as const,
  };
}

export type CrushAction =
  | ReturnType<typeof failed>
  | ReturnType<typeof crushHappened>
  | ReturnType<typeof crushHappenHandled>;
