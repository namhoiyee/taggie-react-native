export const googleResult = {
  html_attributions: [],
  next_page_token:
    'Aap_uEBVaCJCU59a3AW_0e8E0gFvqMU87TkQy667IKzZjmOMKDwTDjLbJAi_4cV4RhwC9Ve-9DT3SqsgCMyzejQrLd4hFrECW7qFFZJKMzDX3fZhdvft11W-c_nB8U8K5n3qc4s0c3kjcwcVK2iMBpsipuZGLtuoHIKmCi4oJxq1XLHs-0eNOFDUrQB4osdI9H3g4MJE0hrOvClw2Usmc5PdDTJC27F-5FracaOsgujO6-0oAFR-I6wHyVRse3C6aOmRBqX327A67-HD7AQk_zsSE4FHfL38VLEaoR-06ILiQyuVz184q8Uo_3LEo4tm4qQCMdQKDlSIjQORto4S-knGJLqyiXSHfRRGpnjwJas87Zb6RcQg23bKgAmN9JuGOObIiVImJreNpGK_Kkb3NywSenxiLufGliQyXJiOvt-4mW0lO8zNwRc3S9LJvVO3BA',
  results: [
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港上環荷李活道',
      geometry: {
        location: {
          lat: 22.2859498,
          lng: 114.1475646,
        },
        viewport: {
          northeast: {
            lat: 22.28723102989272,
            lng: 114.1488508298927,
          },
          southwest: {
            lat: 22.28453137010727,
            lng: 114.1461511701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '荷李活道公園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 3096,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/102202193519070589988"\u003ekumar rahul\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEA4iUPscTFz_8T3C8Skstt7FAuhijb4JneUmpRHzTnPsR-Lv-F-gkokYdNYbk3-QP3M0d4x2BuSOUlESxKPW1Xq1hajfVladm2HQO2CR5Zi1APUkQte6pXpb1uU55FJKjWrGJCfges3zThBHjZcXVDGFeUYCG43LC-1KfHNmnwKtzCo',
          width: 4128,
        },
      ],
      place_id: 'ChIJlajU534ABDQRPxvJ5FQhGsQ',
      plus_code: {
        compound_code: '74PX+92 上環',
        global_code: '7PJP74PX+92',
      },
      rating: 3.9,
      reference: 'ChIJlajU534ABDQRPxvJ5FQhGsQ',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 609,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港上環文咸西街',
      geometry: {
        location: {
          lat: 22.287329,
          lng: 114.1479549,
        },
        viewport: {
          northeast: {
            lat: 22.28872142989272,
            lng: 114.1493454798927,
          },
          southwest: {
            lat: 22.28602177010728,
            lng: 114.1466458201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '皇后街休憩花園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 1536,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/117832408789555351833"\u003eBradJill HK\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uECTEPPd_l4lU8odUy8rUV55JICM7niWBL1RJreMp_CUjUwFCMz-EAOsH6DwYiEvLNogPLotdfp-9LUSXasdVzmKjF3N6k1J7sDwANYcoBj8-UN-F7tLLQDBspsuiC71YBlymky7ewXpCRSn7Ryeh_xrDvn25DUDa1dVOF81WzL_15c6',
          width: 2048,
        },
      ],
      place_id: 'ChIJ6RLmWH4ABDQRtsbu0-6mx0Q',
      plus_code: {
        compound_code: '74PX+W5 上環',
        global_code: '7PJP74PX+W5',
      },
      rating: 3.7,
      reference: 'ChIJ6RLmWH4ABDQRtsbu0-6mx0Q',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 51,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港太平山',
      geometry: {
        location: {
          lat: 22.285026,
          lng: 114.148453,
        },
        viewport: {
          northeast: {
            lat: 22.28637582989272,
            lng: 114.1498028298927,
          },
          southwest: {
            lat: 22.28367617010728,
            lng: 114.1471031701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '差館上街休憩處',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 1920,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/112028610441100856294"\u003enatalie kwong\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEAUJmImF-xrSUjX5IBpUWoLFziaDE81a6yQ_KkqSzJNKbJgLhE4ubvzK-T3qJ274haOQb7JSCVWWqbL3nHgOCiexliadpL6rts3LHRKyCDuOegE0n4HW7hF2CshDidDIATf4vz1WrxIpAJxqq5y-Q9PINrTAvXDP28WSmnezM4JH9xQ',
          width: 1080,
        },
      ],
      place_id: 'ChIJjbTpt34ABDQRuYlxBSbkbRE',
      plus_code: {
        compound_code: '74PX+29 太平山',
        global_code: '7PJP74PX+29',
      },
      rating: 3.4,
      reference: 'ChIJjbTpt34ABDQRuYlxBSbkbRE',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 5,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港上環普興坊與, 交界 Kui In Fong, Sheung Wan, 香港',
      geometry: {
        location: {
          lat: 22.2843195,
          lng: 114.1481218,
        },
        viewport: {
          northeast: {
            lat: 22.28548397989272,
            lng: 114.1497498298927,
          },
          southwest: {
            lat: 22.28278432010728,
            lng: 114.1470501701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '卜公花園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 2976,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/103123838611006717501"\u003eyy shao\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEDgiR0lm-exb5L3FADuz9FrLG6TmfOuf7Av-Y5Oar1At_w4VTjzw7Un_C0M6T1md-_RWnLDczHRUxN1eKga8umj8rOrmDE4lKyJV-scc7CG0IHJkVGLaaNhsfSZo7nG5wDfj0gMLR1fadA8wqdLYal_I5IT1892wUC_XxUmAKAZG0Z0',
          width: 3968,
        },
      ],
      place_id: 'ChIJKcuSNHkABDQRob0wNzajxcY',
      plus_code: {
        compound_code: '74MX+P6 上環',
        global_code: '7PJP74MX+P6',
      },
      rating: 4,
      reference: 'ChIJKcuSNHkABDQRob0wNzajxcY',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 384,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港上環',
      geometry: {
        location: {
          lat: 22.2856924,
          lng: 114.1507625,
        },
        viewport: {
          northeast: {
            lat: 22.28704222989272,
            lng: 114.1521123298927,
          },
          southwest: {
            lat: 22.28434257010728,
            lng: 114.1494126701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '急庇利街休憩處',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 1536,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/117832408789555351833"\u003eBradJill HK\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uECPpp-_Ilbf86NwiGi5K6p53fHO0d4dpwGeR5CDMzzvE8v3TW100VWIxj9mriyNWvorC61CE6KpZ8LP1IZ5e2FG417X0rLRNMnjpq2VrhK9RtgVhG8sGNEsEY1Ik7tUbd7uOddm3pmtj8bnwdD1RJeVhnGSfIe8783PtrO_xEFGCuNF',
          width: 2048,
        },
      ],
      place_id: 'ChIJBzBmPHwABDQRLMAt0Zor6kQ',
      plus_code: {
        compound_code: '75P2+78 上環',
        global_code: '7PJP75P2+78',
      },
      rating: 3.5,
      reference: 'ChIJBzBmPHwABDQRLMAt0Zor6kQ',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 122,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港半山堅巷2號',
      geometry: {
        location: {
          lat: 22.2833621,
          lng: 114.1481404,
        },
        viewport: {
          northeast: {
            lat: 22.28469997989272,
            lng: 114.1494844298927,
          },
          southwest: {
            lat: 22.28200032010728,
            lng: 114.1467847701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '堅巷花園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 1200,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/110828075184815194857"\u003eKaySin\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEAEZxEMAzyiVwKUmobWNCvcrePY4PbrfDxONb420mDfEp6p7u1nfZO8lI2qd00Rj2QzvdgcQiICrsK7moyljJAKFERRJml5ZyeE9LohYyuvdTsOlTRFGSpljGriV9QOqRdlqI8otvE4IpGWI6dEDZW6k21-dDMqfPtLxI53PKSiVCWs',
          width: 1600,
        },
      ],
      place_id: 'ChIJYYbUOHkABDQR1NVO0IF4dPs',
      plus_code: {
        compound_code: '74MX+87 半山',
        global_code: '7PJP74MX+87',
      },
      rating: 4,
      reference: 'ChIJYYbUOHkABDQR1NVO0IF4dPs',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 73,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港上環中港道',
      geometry: {
        location: {
          lat: 22.2892717,
          lng: 114.1496948,
        },
        viewport: {
          northeast: {
            lat: 22.29044822989272,
            lng: 114.1509919298927,
          },
          southwest: {
            lat: 22.28774857010728,
            lng: 114.1482922701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '中西區海濱長廊－上環段',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 3024,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/109044208843827618512"\u003eMay Wong\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEDtti8f0jCdn-xMa4t2yuDyHP4GSKUxG4V4PFhd6gABIat7hqvEOmZWGiFNoTdef71AZn-X3ogO3fwy_q9Lej_gahOwA0OGK_tFi3Jl_wT8B2nxP9b58s7ikg3XmDrscybavTQl0OYOUsO_fNe5IiCoaocijHdRE7Q0x19IV51iV2jv',
          width: 4032,
        },
      ],
      place_id: 'ChIJc-3D4n0ABDQRyt4fxSGd96U',
      plus_code: {
        compound_code: '74QX+PV 上環',
        global_code: '7PJP74QX+PV',
      },
      rating: 4.2,
      reference: 'ChIJc-3D4n0ABDQRyt4fxSGd96U',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 284,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港半山般咸道',
      geometry: {
        location: {
          lat: 22.2836435,
          lng: 114.1466792,
        },
        viewport: {
          northeast: {
            lat: 22.28499457989272,
            lng: 114.1480288798927,
          },
          southwest: {
            lat: 22.28229492010728,
            lng: 114.1453292201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '般咸道休憩花園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 2340,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/106843390798403047936"\u003eKa Wang Lok\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEACyp5QtkjZ2nKYtqHPP6EhV4S30FYhFj28TczhH0OrXaEf4ifpJ9daeipi4afpcTUll9RocAXPgZS_VXTWcVj1jMKGIgTB99j28AVlcs2APSwLp5QIyBrYulrUt1bda2wv58MggC_fcSbiFoOQSo0d3kaYEzukzoo3K65agFO8ZeVX',
          width: 4160,
        },
      ],
      place_id: 'ChIJk7U-2XgABDQRmVOG_L4C2-0',
      plus_code: {
        compound_code: '74MW+FM 半山',
        global_code: '7PJP74MW+FM',
      },
      rating: 2.8,
      reference: 'ChIJk7U-2XgABDQRmVOG_L4C2-0',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 6,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: 'Hospital Rd, Sai Ying Pun, 香港',
      geometry: {
        location: {
          lat: 22.2856206,
          lng: 114.1444114,
        },
        viewport: {
          northeast: {
            lat: 22.28720532989272,
            lng: 114.1457878798927,
          },
          southwest: {
            lat: 22.28450567010728,
            lng: 114.1430882201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '香港佐治五世紀念公園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 4032,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/100204006319374015394"\u003eA Google User\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEBfzwL13blY5VRpmpXQwotEnfalEaWe015SgTSNBgPlTSZ1lAhLlUuBQw2BY9VPCrrCosNh6tjVR-N5gSIHcnUFZuhyQcb6GdO68E00YNVKrGt0mwWJqdARLbstcNYuTfy5R-CKLr1wKurOmHRimBHCAC0b4jBhA9_KtR1Ldd_M0VBM',
          width: 3024,
        },
      ],
      place_id: 'ChIJg_WfUH8ABDQRUpnVtqDlMhc',
      plus_code: {
        compound_code: '74PV+6Q 西營盤',
        global_code: '7PJP74PV+6Q',
      },
      rating: 4,
      reference: 'ChIJg_WfUH8ABDQRUpnVtqDlMhc',
      types: [
        'tourist_attraction',
        'park',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 581,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '號, 30 Sai Woo Ln, Sai Ying Pun, 香港',
      geometry: {
        location: {
          lat: 22.2873815,
          lng: 114.1441656,
        },
        viewport: {
          northeast: {
            lat: 22.28895152989272,
            lng: 114.1454756298927,
          },
          southwest: {
            lat: 22.28625187010728,
            lng: 114.1427759701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '西湖里公園',
      photos: [
        {
          height: 2637,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/101766765907249615956"\u003eLarry Suen\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEAx1_h52DCgVxcsUIodwdaLKi6vHVR6pabQlDCTxHAdn8TVmYHYEWqU9j3zUq4-H5cxh17RqGgwzkQ2HVsGGK1xpBhVSBUlbjyPS2ibcHBuiUu0SKaw3zCIwC1ZuFEWFqCnrruPGCv77corchrenITfLKX9sCDaXq8XfcGFTT4Cg32F',
          width: 3956,
        },
      ],
      place_id: 'ChIJ94RQfH8ABDQRtjEzY-LGxbM',
      plus_code: {
        compound_code: '74PV+XM 西營盤',
        global_code: '7PJP74PV+XM',
      },
      rating: 3.5,
      reference: 'ChIJ94RQfH8ABDQRtjEzY-LGxbM',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 4,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港中環中和里',
      geometry: {
        location: {
          lat: 22.282886,
          lng: 114.1515229,
        },
        viewport: {
          northeast: {
            lat: 22.28432162989272,
            lng: 114.1529059298927,
          },
          southwest: {
            lat: 22.28162197010728,
            lng: 114.1502062701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '中和里休憩處',
      photos: [
        {
          height: 2448,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/109501787441195662508"\u003eLEE RICO\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEAfxESQEhHNdXNRFaHIYo6pWTBSHdi1f0FsHb-O-bjQbKFlPIIFmY6X1fitHoGFtEjiZYNfnDLjdhZgtFvtrYsXFAEpWUxmcwcDGWp_7RC9wbTGUZt3YmMmevVxzHxJnYZGu9FBxmIOEbuvQgZCOMj8okYzOwNbKLKwpUcedRXUVQix',
          width: 3264,
        },
      ],
      place_id: 'ChIJ4b59k3sABDQRtcZQr7kjuf8',
      plus_code: {
        compound_code: '75M2+5J 中環',
        global_code: '7PJP75M2+5J',
      },
      rating: 0,
      reference: 'ChIJ4b59k3sABDQRtcZQr7kjuf8',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 0,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港中環',
      geometry: {
        location: {
          lat: 22.2843503,
          lng: 114.152904,
        },
        viewport: {
          northeast: {
            lat: 22.28576057989272,
            lng: 114.1542728798927,
          },
          southwest: {
            lat: 22.28306092010728,
            lng: 114.1515732201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '九如坊兒童遊樂場',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 3024,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/100454608950879681999"\u003exe hannibal\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uECB1veaO-sWF4FKlIzhjYMjAOH2Qg0_CDOhC7Cz9epHcP5lOqr8tCASjC7nJoeyhV_nLota7V32IHHYVNKt-tTDL88CO3Z9Mlw93NeYnGIGhWGgy3EBEYkG-8_myZ-a2KlAYXkkN3wR5juVQiH-sGlLbDFboBSBHaJLIn_isniFYyJx',
          width: 4032,
        },
      ],
      place_id: 'ChIJbef6h3wABDQRpzKcBkF5B4o',
      plus_code: {
        compound_code: '75M3+P5 中環',
        global_code: '7PJP75M3+P5',
      },
      rating: 3.6,
      reference: 'ChIJbef6h3wABDQRpzKcBkF5B4o',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 33,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港西營盤東邊街北16號',
      geometry: {
        location: {
          lat: 22.2902364,
          lng: 114.1454925,
        },
        viewport: {
          northeast: {
            lat: 22.290922,
            lng: 114.14905445,
          },
          southwest: {
            lat: 22.2881796,
            lng: 114.14180865,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '中山紀念公園',
      opening_hours: {},
      photos: [
        {
          height: 2448,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/102196899138071281607"\u003ePatrick Brousseau\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEBKfImF-mxJXGFnASS9WVLTPTm9qxTTBWpcCFVeNEaQ_oZXLpzhRyk_W2IYwlZ7TK17NL7aGGYpL6gflLqI1EMYgG2HYyMJtDlXnOngvpfUP3WOULjud_z9DcZKsBoUb3XZ_mWZiBIKkN_gzMOcJfdWM4HPlmLhWQv9oPsETtaIFXyD',
          width: 3264,
        },
      ],
      place_id: 'ChIJASM1w38ABDQRrgLa3IYv1DU',
      plus_code: {
        compound_code: '74RW+35 西營盤',
        global_code: '7PJP74RW+35',
      },
      rating: 4.3,
      reference: 'ChIJASM1w38ABDQRrgLa3IYv1DU',
      types: [
        'tourist_attraction',
        'park',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 3726,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港中環鴨巴甸街41-49號',
      geometry: {
        location: {
          lat: 22.2824746,
          lng: 114.151792,
        },
        viewport: {
          northeast: {
            lat: 22.28390737989272,
            lng: 114.1530479798927,
          },
          southwest: {
            lat: 22.28120772010728,
            lng: 114.1503483201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '光漢台花園',
      opening_hours: {},
      photos: [
        {
          height: 3120,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/103734375131198229068"\u003eKawah Pang\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEDxkysV9ULwfXyOa-MRvTkJb2UzdjCok7BbsX8lzU1XS8fTW1RRUZ59qHzbZgGLiMtITkGS416FccdYNNtZgW3bYcjHV0zs80qn2OiKmMmjxE3FytFISday8nObl-ZOBVcQtkw-5birKd0xLvDh5lpQXimIBQr_GPbAV_hjx8dojXzS',
          width: 4160,
        },
      ],
      place_id: 'ChIJ2cb_vXsABDQRzsbv-7K_-OU',
      plus_code: {
        compound_code: '75J2+XP 中環',
        global_code: '7PJP75J2+XP',
      },
      rating: 4.2,
      reference: 'ChIJ2cb_vXsABDQRzsbv-7K_-OU',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 62,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '1 Pak Tsz Ln, Central, 香港',
      geometry: {
        location: {
          lat: 22.2834346,
          lng: 114.1528874,
        },
        viewport: {
          northeast: {
            lat: 22.28482122989272,
            lng: 114.1542888798927,
          },
          southwest: {
            lat: 22.28212157010728,
            lng: 114.1515892201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '百子里公園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 1200,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/114325967034446325933"\u003eNick Yung\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uECVdyEFFakAfrjckEV1ZKFKRspuw1zqKdTHDDXjkyzlgQGF99aIt0vZbaaSBsVdUG8U3bTYDv5IsZsKsmsF955TlmxOKrtcnXCoTo3IYtjRsQKlLCIaq7Ga4Q3RG32gWyEFDUyFGrJMVpSiF6djudcoYfBq_6clLcRIJPHN3fvEEW-r',
          width: 1600,
        },
      ],
      place_id: 'ChIJdd5Wf3sABDQRtPrr-eNrWYA',
      plus_code: {
        compound_code: '75M3+95 中環',
        global_code: '7PJP75M3+95',
      },
      rating: 4,
      reference: 'ChIJdd5Wf3sABDQRtPrr-eNrWYA',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 139,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港中環',
      geometry: {
        location: {
          lat: 22.2839689,
          lng: 114.1534347,
        },
        viewport: {
          northeast: {
            lat: 22.28531872989272,
            lng: 114.1547845298927,
          },
          southwest: {
            lat: 22.28261907010728,
            lng: 114.1520848701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '華安里休憩處',
      opening_hours: {},
      photos: [
        {
          height: 3000,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/111371499899554356672"\u003eFrancis\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEDOiHJ3fBeSxvSZCFEbZbezWysa9UgEJSsn6vW4vuNXwtr6rusjbWG-ALQqQYHIJJFZT6O_izPX5m0uxnvvUlhJBpp_3Uzvqpa-FFCcd-aH6-QkEfwvTEnU3zBih0Hs9DSXsLoQQ8N99gr6r1OD442x0a3Oxww11d1vb4By20b8OKT8',
          width: 4000,
        },
      ],
      place_id: 'ChIJjeB9g3wABDQRUEyQ-8QtlIs',
      plus_code: {
        compound_code: '75M3+H9 中環',
        global_code: '7PJP75M3+H9',
      },
      rating: 5,
      reference: 'ChIJjeB9g3wABDQRUEyQ-8QtlIs',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 1,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港半山柏道',
      geometry: {
        location: {
          lat: 22.2835986,
          lng: 114.143138,
        },
        viewport: {
          northeast: {
            lat: 22.28510462989273,
            lng: 114.1445294798927,
          },
          southwest: {
            lat: 22.28240497010728,
            lng: 114.1418298201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '城西公園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 3024,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/105619697371959356267"\u003eHannah Muirhead\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEBBeRY0RWtO30_8Xj6mqrSvNRMkbih-BoCmh-8nBxzdzEXD4Ct5nvDX79upZiALTcX7ijsqqk8dUKB1uo6_DAnsk05BtFBMZRi7-cvsokD_ARPIvGqh8fzPGlS4gN8rAbfeZ8lSa01um4MohkPkrG1RZ66ozrA2K6NeoNxKC4DY2o0z',
          width: 4032,
        },
      ],
      place_id: 'ChIJZ8aOaIf_AzQRRFalf05GIME',
      plus_code: {
        compound_code: '74MV+C7 半山',
        global_code: '7PJP74MV+C7',
      },
      rating: 3.7,
      reference: 'ChIJZ8aOaIf_AzQRRFalf05GIME',
      types: [
        'park',
        'tourist_attraction',
        'point_of_interest',
        'establishment',
      ],
      user_ratings_total: 99,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港中環',
      geometry: {
        location: {
          lat: 22.2822939,
          lng: 114.1529072,
        },
        viewport: {
          northeast: {
            lat: 22.28367322989272,
            lng: 114.1542270298927,
          },
          southwest: {
            lat: 22.28097357010728,
            lng: 114.1515273701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '嘉咸街休憇處',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 1200,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/110828075184815194857"\u003eKaySin\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEDGyWERHrf10QM29IBrNKcw3bp4_rNKyp-6Lb7iubF-chO1uiBIPIzG11pQCxRZ5RtJBbuCGLy3aV17lxvNczrKs3vIs7mtMLcffVoOnuUnX1FrVpBAAVaSVka4s3nm7SwP6FcmcHWwLbZa1-wgOK1MNRT_hJI-4JgoTeOZx6pXaX2e',
          width: 1600,
        },
      ],
      place_id: 'ChIJITOyCnsABDQRJds2FjfRfOQ',
      plus_code: {
        compound_code: '75J3+W5 中環',
        global_code: '7PJP75J3+W5',
      },
      rating: 5,
      reference: 'ChIJITOyCnsABDQRJds2FjfRfOQ',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 1,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港中環上亞厘畢道1號',
      geometry: {
        location: {
          lat: 22.2779648,
          lng: 114.1564146,
        },
        viewport: {
          northeast: {
            lat: 22.27948642989272,
            lng: 114.1579429798927,
          },
          southwest: {
            lat: 22.27678677010728,
            lng: 114.1552433201073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '噴水池平台花園',
      opening_hours: {},
      photos: [
        {
          height: 4000,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/115064927302759914672"\u003ekiiltochii\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uEALM4P06e_emRqmEGntdJLzb-oYVYhfioOWVd6FaHJvKgeTUjIyr46uHM_UDTQov5yxFQsxRZ7qIKieVbvp9LX4etaGZH7EoGiRtexsR824QTQm27_tJGVXbUhCcWZxUu6dnEYQ1HBYhR-fvBjuYtLqb_Q8vj7StzCExNSnMkUOGu5l',
          width: 6000,
        },
      ],
      place_id: 'ChIJb5DQUGUABDQRjvPwYuOAZks',
      plus_code: {
        compound_code: '75H4+5H 中環',
        global_code: '7PJP75H4+5H',
      },
      rating: 4.2,
      reference: 'ChIJb5DQUGUABDQRjvPwYuOAZks',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 85,
    },
    {
      business_status: 'OPERATIONAL',
      formatted_address: '香港西環',
      geometry: {
        location: {
          lat: 22.284731,
          lng: 114.135594,
        },
        viewport: {
          northeast: {
            lat: 22.28608082989273,
            lng: 114.1369438298927,
          },
          southwest: {
            lat: 22.28338117010728,
            lng: 114.1342441701073,
          },
        },
      },
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/park-71.png',
      name: '山道花園',
      opening_hours: {
        open_now: true,
      },
      photos: [
        {
          height: 2322,
          html_attributions: [
            '\u003ca href="https://maps.google.com/maps/contrib/107543913833527356530"\u003edolma hongkong\u003c/a\u003e',
          ],
          photo_reference:
            'Aap_uECtyfak5OvVPhTnfVY9mkR9KknlXh1VBpObMhPOlSSpP1RkAXFXn3HlFelzUfeANSFZ7bmemCRbZUUm-sI31mN-z7WLNGBgC6zEfYwtPICYtwoKgXn3PYFGYtZ_4lrA7VjwcA9MZ8o6oxDlpshHuOR9akmbz4YREPqJxAB2KV7E7epU',
          width: 4128,
        },
      ],
      place_id: 'ChIJa6eIXIT_AzQRGsgarJTxB1c',
      plus_code: {
        compound_code: '74MP+V6 西環',
        global_code: '7PJP74MP+V6',
      },
      rating: 3.3,
      reference: 'ChIJa6eIXIT_AzQRGsgarJTxB1c',
      types: ['park', 'point_of_interest', 'establishment'],
      user_ratings_total: 39,
    },
  ],
  status: 'OK',
};
