/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import GoogleMapComponent from '../googleMap/GoogleMapComponent';
import {isAndroid} from '../../utils/platform';
import {Colors} from '../../utils/Colors';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import LoadingComponent from './Loading/LoadingComponent';
import {loadFriendProfileThunk} from '../../redux/friendProfile/thunk';
import {S3Link} from '../../helpers/api';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CrushScreen from './Crush/CrushScreen';
import {TagsEmoji} from '../../helpers/types';

const {width, height} = Dimensions.get('window');

export default function FriendProfileScreen({navigation, route}: any) {
  const friendProfile = useSelector(
    (state: IRootState) => state.friendProfile.friendProfile,
  );
  const coordinate = useSelector(
    (state: IRootState) => state.friendProfile.coordinate,
  );
  const text_description = useSelector(
    (state: IRootState) => state.friendProfile.text_description,
  );
  const loadingStatus = useSelector(
    (state: IRootState) => state.friendProfile.status,
  );
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );

  const dispatch = useDispatch();

  let friend_id = route?.params?.friend_id;
  // let friend_name = route?.params?.name
  let profilePic = route?.params?.profilePic;
  console.log('friend_id received from chat: ', friend_id);

  useEffect(() => {
    dispatch(loadFriendProfileThunk(friend_id, profilePic));
  }, [dispatch, friend_id, profilePic]);

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  let matchTags: string[] = [];
  if (friendProfile?.matchedTags) {
    for (let i = 0; i < friendProfile?.matchedTags.length; i++) {
      matchTags.push(friendProfile?.matchedTags[i].tag);
    }
  }

  console.log('matchedTags: ', friendProfile?.matchedTags);

  return (
    <View style={styles.imageContainer}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <View>
            <FastImage
              source={{
                uri: profilePic,
                priority: FastImage.priority.high,
              }}
              style={styles.image}
            />
            <View
              style={{
                ...styles.matchedUserDetail,
                top: height - 120,
              }}>
              {friendProfile && (
                <Text style={styles.matchedUserDetailText}>
                  {friendProfile?.username}&nbsp;
                  {friendProfile?.userAge}歲
                </Text>
              )}
            </View>
            {friendProfile && (
              <View style={styles.tagsContainer}>
                {text_description && (
                  <>
                    <Text style={styles.matchedUserDetailTextTitle}>
                      {friendProfile?.username}的一句:
                    </Text>
                    <Text style={styles.matchedUserDetailTextDescription}>
                      {text_description}
                    </Text>
                  </>
                )}
                {friendProfile?.user_tags.map(
                  (item, index) =>
                    item.tag !== '女' &&
                    item.tag !== '男' &&
                    item.tag !== 'male' &&
                    item.tag !== 'female' &&
                    item.tag !== '其他' && (
                      <View
                        style={[
                          styles.tagContainer,
                          // {backgroundColor: 'rgba(238, 164, 127, 1)'},
                          {
                            backgroundColor: matchTags.includes(item.tag)
                              ? '#FF5740'
                              : 'rgba(238, 164, 127, 1)',
                          },
                        ]}
                        key={'tag' + index}>
                        <Text style={styles.text}>
                          {
                            // @ts-ignore
                            item.tag + ' ' + TagsEmoji[item.tag]
                          }
                        </Text>
                      </View>
                    ),
                )}
              </View>
            )}
            {friendProfile?.gallery.map((item, index) =>
              profilePic === S3Link + item.url ? null : (
                <FastImage
                  source={{
                    uri: S3Link + item.url,
                    priority: FastImage.priority.high,
                  }}
                  key={'gallery' + index}
                  style={{
                    width,
                    height,
                    borderRadius: isAndroid ? 0 : 20,
                  }}
                  // style={styles.image}
                />
              ),
            )}
          </View>
          <View
            style={{
              alignItems: 'center',
              height: 20,
              backgroundColor: Colors.backgroundGray,
              borderRadius: 20,
              justifyContent: 'center',
            }}>
            <Text>你們相遇的地點:</Text>
          </View>
          <View style={styles.container}>
            <GoogleMapComponent
              //@ts-ignore
              location={
                coordinate !== undefined
                  ? coordinate
                  : {x: 114.1631584, y: 22.2784441}
              }
            />
          </View>
          {/* <View style={{height: tabBarHeight}} /> */}
        </View>
      </ScrollView>
      <View style={styles.return}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <MaterialIcons
            name={isAndroid ? 'arrow-back' : 'arrow-back-ios'}
            size={isAndroid ? 30 : 25}
            color="#333333"
            style={{transform: [{translateX: isAndroid ? 0 : 5}]}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  image: {
    width,
    height,
    borderRadius: isAndroid ? 0 : 20,
  },
  tagsContainer: {
    backgroundColor: Colors.mainBlue,
    borderRadius: isAndroid ? 0 : 20,
    padding: 20,
    flexDirection: 'row',
    width: width,
    flexWrap: 'wrap',
  },
  tagContainer: {
    borderRadius: 20,
    padding: 10,
    alignSelf: 'stretch',
    marginRight: 10,
    marginVertical: 10,
    justifyContent: 'center',
    alignContent: 'center',
    minWidth: 50,
  },
  matchedUserDetail: {
    position: 'absolute',
    left: 20,
    backgroundColor: 'rgba(0,0,0, 0.2)',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  matchedUserDetailTextTitle: {
    color: '#fff',
    fontSize: 16,
    textShadowColor: 'black',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
    width: '100%',
  },
  matchedUserDetailTextDescription: {
    color: '#fff',
    fontSize: 24,
    textShadowColor: 'black',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
    width: '100%',
    marginBottom: 20,
  },
  matchedUserDetailText: {
    color: '#fff',
    fontSize: 24,
    textShadowColor: 'black',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
  },
  iconContainerLeft: {
    position: 'absolute',
    left: 20,
    height: 40,
    width: 40,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    borderRadius: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainerRight: {
    position: 'absolute',
    right: 20,
    height: 40,
    width: 40,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    borderRadius: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconLeft: {
    position: 'absolute',
  },
  iconRight: {
    position: 'absolute',
    top: 7.5,
  },
  text: {
    fontSize: 16,
    color: 'rgb(255,255,255)',
    textAlign: 'center',
  },
  container: {
    height: 300,
    width: '100%',
    borderRadius: 20,
  },
  return: {
    position: 'absolute',
    top: isAndroid ? 10 : 50,
    justifyContent: 'center',
    alignItems: 'center',
    left: 10,
    backgroundColor: 'white',
    padding: isAndroid ? 3 : 6,
    borderRadius: 50,
  },
});
