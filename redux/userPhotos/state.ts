import {UserGallery} from '../../helpers/types';

export type UserPhotosState =
  | {
      status: 'loading';
      msg?: string;
      userGallery?: UserGallery;
    }
  | {
      status: 'error';
      msg: string;
      userGallery?: UserGallery;
    }
  | {
      status: 'loadedPhotos';
      msg?: string;
      userGallery: UserGallery;
    }
  | {
      status: 'deletedPhotos';
      msg?: string;
      userGallery: UserGallery;
    }
  | {
      status: 'insertedPhotos';
      msg?: string;
      userGallery: UserGallery;
    };

export const initialState: UserPhotosState = {
  status: 'loading',
};
