import {api} from '../../helpers/api';
import {putMarker, failed, loading} from './action';
import {IRootState, IRootThunkDispatch} from '../store';
import {MarkerInfo, Region} from '../../helpers/types';
import {Alert} from 'react-native';
// import {loaded, loading} from '../loading/action';

export type WhoLikeUInfo = {
  id: number;
  match_coordinates: {x: number; y: number};
  user_id: number;
  username: string;
  profile: string;
  oProfile: string;
};

export function getWhoLikesU() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    try {
      dispatch(loading());
      const userToken = getState().auth.token;
      const location = getState().match.location;
      if (!location) {
        Alert.alert('讀取你的location時失敗', '請稍後重試', [
          {text: '確認', style: 'default'},
        ]);
        dispatch(failed('failed to get location, no location'));
        // dispatch(loaded());
      }
      const region: Region = {
        latitude: location!.latitude,
        longitude: location!.longitude,
        latitudeDelta: 0.04864195044303443,
        longitudeDelta: 0.040142817690068,
      };
      const res = await api.get('/whoLikeU', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result: {
        isSuccess: Boolean;
        data: WhoLikeUInfo[];
      } = await res.json();
      // console.log("markers:" ,result)
      let whoLikesU: MarkerInfo[] = [];
      for (let person of result.data) {
        whoLikesU.push({
          coordinate: {
            latitude: person.match_coordinates.y,
            longitude: person.match_coordinates.x,
          },
          username: person.username,
          profilePic: person.profile,
          originalPic: person.oProfile,
          userId: person.user_id,
        });
      }
      dispatch(putMarker(whoLikesU, region));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取Like歷史時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed(
          'failed to get people who likes u: ' + error.msg + error.message,
        ),
      );
      // dispatch(loaded());
      return;
    }
  };
}
