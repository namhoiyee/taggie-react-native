import {LoadingState, initialState} from './state';
import {LoadingAction} from './action';

export const LoadingReducer = (
  state: LoadingState = initialState,
  action: LoadingAction,
): LoadingState => {
  switch (action.type) {
    case '@@Loading/loaded':
      return {
        status: 'loaded',
      };
    case '@@Loading/loading':
      return {
        status: 'loading',
      };
    default:
      return state;
  }
};
