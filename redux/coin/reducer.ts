import {CoinRecordState, initialState} from './state';
import {CoinRecordAction} from './action';

export const CoinRecordReducer = (
  state: CoinRecordState = initialState,
  action: CoinRecordAction,
): CoinRecordState => {
  switch (action.type) {
    case '@@CoinRecord/loadCoinRecord':
      return {
        ...state,
        status: 'ready',
        coinRecord: action.coinRecord,
      };

    case '@@CoinRecord/loaded':
      return {
        ...state,
        status: 'loaded',
      };

    case '@@CoinRecord/loading':
      return {
        ...state,
        status: 'loading',
      };

    case '@@CoinRecord/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };

    default:
      return state;
  }
};
