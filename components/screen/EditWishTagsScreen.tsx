/* eslint-disable react-native/no-inline-styles */
// Example of Collapsible/Accordion/Expandable List View in React Native
// https://aboutreact.com/collapsible-accordion-expandable-view/

import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import {useAnimatedRef} from 'react-native-reanimated';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {Tag} from '../../helpers/types';
import {
  Transition,
  Transitioning,
  TransitioningView,
} from 'react-native-reanimated';
import {useEffect} from 'react';
import {
  addWishTag,
  editWishTags,
  loadAllClassesAndTags,
} from '../../redux/tags/thunk';
import {useRef} from 'react';
import {useCallback} from 'react';
import LoadingComponent from './Loading/LoadingComponent';
import {imageReq} from '../../helpers/types';
import LinearGradient from 'react-native-linear-gradient';
import CrushScreen from './Crush/CrushScreen';

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={200} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={200} />
  </Transition.Together>
);

const EditWishTagsScreen = ({route, navigation}: any) => {
  const loadingStatus = useSelector((state: IRootState) => state.tags?.status);
  const userWishTagsDetails = useSelector(
    (state: IRootState) => state.tags?.userWishTags,
  );
  const userWishTags = userWishTagsDetails?.data?.wishTags;
  const [userWishTagsId, setUserWishTagsId] = useState(
    userWishTags?.map(tag => tag.id),
  );
  const userSelectedTagWhenEnterOrAfterAdd = useRef<Tag | undefined>(
    route?.params?.item?.id === -1 ? undefined : route?.params?.item,
  );
  const userLastSelectedTag = useRef<Tag>(route?.params?.item);
  const [userSelectedTag, setUserSelectedTag] = useState<Tag | undefined>(
    route?.params?.item,
  );
  const [isDispatchEdit, setIsDispatchEdit] = useState(false);
  const [isDispatchAdd, setIsDispatchAdd] = useState(false);
  const [isClassOpened, setIsClassOpened] = useState(false);
  const [classSelected, setClassSelected] = useState<null | number>(null);
  const [pressControl, setPressControl] = useState(true);

  console.log('userSelectedTag', userSelectedTag);
  const tagClassesAndTags = useSelector(
    (state: IRootState) => state.tags?.tagClassesAndTags,
  );
  const ref = useAnimatedRef<TransitioningView>();
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadAllClassesAndTags());
  }, [dispatch]);

  const updateList = useCallback(() => {
    setUserWishTagsId(userWishTags?.map(tag => tag.id));
  }, [userWishTags]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('beforeRemove', () => {
      if (
        userSelectedTagWhenEnterOrAfterAdd.current &&
        isDispatchEdit &&
        userSelectedTag
      ) {
        dispatch(
          editWishTags({
            oldTag: userLastSelectedTag.current,
            newTag: userSelectedTag,
          }),
        );
        userLastSelectedTag.current = userSelectedTag;
        updateList();
        setIsDispatchEdit(false);
      }
    });

    return unsubscribe;
  }, [dispatch, isDispatchEdit, userSelectedTag, updateList, navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('beforeRemove', () => {
      if (
        !userSelectedTagWhenEnterOrAfterAdd.current &&
        isDispatchAdd &&
        userSelectedTag
      ) {
        dispatch(addWishTag(userSelectedTag));
        userLastSelectedTag.current = userSelectedTag;
        userSelectedTagWhenEnterOrAfterAdd.current = userSelectedTag;
        updateList();
        setIsDispatchAdd(false);
      }
    });

    return unsubscribe;
  }, [dispatch, isDispatchAdd, userSelectedTag, updateList, navigation]);


  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#333333'}}>
      <ScrollView>
        <Transitioning.View
          ref={ref}
          transition={transition}
          style={styles.container}>
          {tagClassesAndTags &&
            userSelectedTag &&
            tagClassesAndTags.map(({id, tags, class: tagClass}) => {
              return (
                <TouchableOpacity
                  key={tagClass + id}
                  onPress={() => {
                    ref?.current?.animateNextTransition();
                    setUserSelectedTag(prev => {
                      if (prev) {
                        return {...prev, classId: id, class: tagClass};
                      }
                    });
                    setIsClassOpened(opened =>
                      userSelectedTag.classId === id ? !opened : false,
                    );
                    if (classSelected === null || classSelected !== id) {
                      setClassSelected(id);
                    } else {
                      setClassSelected(null);
                    }
                  }}
                  style={[
                    styles.cardContainer,
                    {
                      backgroundColor: '#333333',
                      minHeight: 60,
                    },
                  ]}>
                  <Image
                    // @ts-ignore
                    source={imageReq[tagClass]}
                    style={{
                      position: 'absolute',
                      height: '100%',
                      width: '100%',
                      top: 12,
                    }}
                    resizeMode="cover"
                  />
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={[
                      'rgba(0, 0, 0, 0)',
                      'rgba(0, 0, 0, 0.2)',
                      'rgba(0, 0, 0, 0.3)',
                      'rgba(0, 0, 0, 0.4)',
                      'rgba(0, 0, 0, 0.5)',
                      'rgba(0, 0, 0, 0.6)',
                      'rgba(0, 0, 0, 0.7)',
                      'rgba(0, 0, 0, 0.6)',
                      'rgba(0, 0, 0, 0.5)',
                      'rgba(0, 0, 0, 0.4)',
                      'rgba(0, 0, 0, 0.3)',
                      'rgba(0, 0, 0, 0.2)',
                      'rgba(0, 0, 0, 0)',
                    ]}
                    style={{
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      top: 12,
                    }}
                  />
                  <View style={[styles.card]}>
                    <Text style={[styles.heading]}>
                      {tagClass === '習慣1'
                        ? '吸煙習慣'
                        : tagClass === '習慣2'
                        ? '飲酒習慣'
                        : tagClass}
                    </Text>
                    {id === userSelectedTag?.classId && !isClassOpened && (
                      <View style={styles.subCategoriesList}>
                        {tags &&
                          tags.map(
                            tag =>
                              (!userWishTagsId?.includes(tag.id) ||
                                tag.id ===
                                  userSelectedTagWhenEnterOrAfterAdd.current
                                    ?.id) && (
                                <TouchableOpacity
                                  key={tag.tag + tag.id}
                                  // disabled={userWishTagsId?.includes(tag.id)}
                                  style={styles.tagItem}
                                  onPress={() => {
                                    if (pressControl === false) {
                                      return;
                                    }
                                    console.log(tag.tag);
                                    if (tag.id !== userSelectedTag.id) {
                                      setUserSelectedTag(
                                        prev =>
                                          prev && {
                                            ...tag,
                                            dbPrimaryId: prev.dbPrimaryId,
                                          },
                                      );
                                      if (
                                        userSelectedTagWhenEnterOrAfterAdd.current
                                      ) {
                                        setIsDispatchEdit(true);
                                      } else {
                                        setIsDispatchAdd(true);
                                      }
                                      setPressControl(false);
                                      setTimeout(() => {
                                        navigation.goBack();
                                        setPressControl(true);
                                      }, 500);
                                    }
                                  }}>
                                  <Text
                                    style={[
                                      styles.body,
                                      tag.id === userSelectedTag?.id && {
                                        color: 'red',
                                      },
                                    ]}>
                                    {tag.tag}
                                  </Text>
                                </TouchableOpacity>
                              ),
                          )}
                      </View>
                    )}
                  </View>
                </TouchableOpacity>
              );
            })}
        </Transitioning.View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default EditWishTagsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    justifyContent: 'center',
  },
  cardContainer: {
    flexGrow: 1,
    paddingVertical: 10,
  },
  card: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 26,
    fontWeight: '500',
    color: '#fff',
    // textTransform: 'uppercase',
    // letterSpacing: -2,
    textShadowColor: 'rgb(0, 0, 0)',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 2,
  },
  body: {
    fontSize: 20,
    // lineHeight: 20 * 1.5,
    textAlign: 'center',
    color: '#fff',
    textShadowColor: 'rgb(0, 0, 0)',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
  },
  subCategoriesList: {
    marginTop: 20,
    width: '100%',
  },
  tagItem: {
    paddingVertical: 5,
  },
});
