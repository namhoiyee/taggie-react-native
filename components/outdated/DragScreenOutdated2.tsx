import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import data, {Data} from './data';
import {
  Transition,
  Transitioning,
  TransitioningView,
} from 'react-native-reanimated';
import {useAnimatedRef} from 'react-native-reanimated';

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={200} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={200} />
  </Transition.Together>
);

export default function DragScreen() {
  const [currentIndex, setCurrentIndex] = useState<number | null>(null);
  const ref = useAnimatedRef<TransitioningView>();

  return (
    <ScrollView>
      <Transitioning.View
        ref={ref}
        transition={transition}
        style={styles.container}>
        {data.map(({bg, color, category, subCategories}: Data, index) => {
          return (
            <TouchableOpacity
              key={category + index}
              onPress={() => {
                ref?.current?.animateNextTransition();
                setCurrentIndex(index === currentIndex ? null : index);
              }}
              style={styles.cardContainer}
              activeOpacity={0.9}>
              <View style={[styles.card, {backgroundColor: bg}]}>
                <Text style={[styles.heading, {color}]}>{category}</Text>
                {index === currentIndex && (
                  <View style={styles.subCategoriesList}>
                    {subCategories.map(subCategory => (
                      <Text key={subCategory} style={[styles.body, {color}]}>
                        {subCategory}
                      </Text>
                    ))}
                  </View>
                )}
              </View>
            </TouchableOpacity>
          );
        })}
      </Transitioning.View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  cardContainer: {
    flexGrow: 1,
  },
  card: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 38,
    fontWeight: '900',
    textTransform: 'uppercase',
    letterSpacing: -2,
  },
  body: {
    fontSize: 20,
    lineHeight: 20 * 1.5,
    textAlign: 'center',
  },
  subCategoriesList: {
    marginTop: 20,
  },
});
