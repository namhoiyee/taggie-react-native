export function logout() {
  return {
    type: '@@Auth/logout' as const,
  };
}

export function loading() {
  return {
    type: '@@Auth/loading' as const,
  };
}

export function loadToken(token: string) {
  console.log(token);
  return {
    type: '@@Auth/load_token' as const,
    token,
  };
}

export function failed(msg: string) {
  console.log('failed auth', msg);
  return {
    type: '@@Auth/failed' as const,
    msg,
  };
}

export function confirmedToBeRegStatThree() {
  return {
    type: '@@Auth/confirmedToBeRegStatThree' as const,
  };
}

// export function signUp(msg: string) {
//   return {
//     type: '@@Auth/sign_up' as const,
//     msg,
//   };
// }

// export function toggleTheme() {
//   return {};
// }

export function updatedToken(token: string) {
  return {
    type: '@@Auth/updatedToken' as const,
    token,
  };
}

export type AuthAction =
  | ReturnType<typeof loadToken>
  | ReturnType<typeof updatedToken>
  | ReturnType<typeof loading>
  | ReturnType<typeof confirmedToBeRegStatThree>
  | ReturnType<typeof logout>
  | ReturnType<typeof failed>;
