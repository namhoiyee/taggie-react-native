import {SuggestionState, initialState} from './state';
import {SuggestionAction} from './action';

export const SuggestionReducer = (
  state: SuggestionState = initialState,
  action: SuggestionAction,
): SuggestionState => {
  switch (action.type) {
    case '@@Suggestion/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Suggestion/loading': {
      return {
        ...state,
        status: 'loading',
      };
    }
    case '@@Suggestion/updatedCurrentRoomId': {
      return {
        ...state,
        status: 'updatedCurrentRoomId',
        roomId: action.roomId,
      };
    }
    case '@@Suggestion/ready': {
      return {
        ...state,
        status: 'ready',
      };
    }
    case '@@Suggestion/loadedMapSuggestion': {
      return {
        ...state,
        status: 'loadedMapSuggestion',
        markers: action.markers,
      };
    }
    case '@@Suggestion/loadedSuggestPlaces': {
      return {
        ...state,
        status: 'loadedSuggestPlaces',
        suggestion: action.suggestion,
        location: action.location,
      };
    }
    case '@@Suggestion/wentOutFromMap': {
      return {
        ...state,
        status: 'wentOutFromMap',
        markers: undefined,
      };
    }
    default:
      return state;
  }
};
