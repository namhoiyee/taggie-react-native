import {ChatroomState, initialState} from './state';
import {ChatroomAction} from './action';

export const ChatroomReducer = (
  state: ChatroomState = initialState,
  action: ChatroomAction,
): ChatroomState => {
  switch (action.type) {
    case '@@Chatroom/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    // case '@@Chatroom/loading':
    //   return {
    //     ...state,
    //     status: 'loading',
    //   };
    case '@@Chatroom/loadingChatrooms':
      return {
        ...state,
        status: 'loadingChatrooms',
      };
    case '@@Chatroom/loadedChatroom': {
      return {
        ...state,
        status: 'loadedChatroom',
        chatroom: action.chatroom,
      };
    }
    case '@@Chatroom/updatedUnreadCount': {
      return {
        ...state,
        status: 'updatedUnreadCount',
        unread: action.unread,
      };
    }
    case '@@Chatroom/loadedChatroomMessages': {
      return {
        ...state,
        status: 'loadedChatroomMessages',
        messages: action.messages,
      };
    }
    case '@@Chatroom/wentOutFromChatroom': {
      return {
        ...state,
        status: 'wentOutFromChatroom',
        messages: undefined,
      };
    }
    case '@@Chatroom/updatedRoomId': {
      return {
        ...state,
        status: 'updatedRoomId',
        currentRoomId: action.currentRoomId,
      };
    }
    case '@@Chatroom/postedChatroomMessage': {
      return {
        ...state,
        status: 'postedChatroomMessage',
        msg: action.msg,
      };
    }
    case '@@Chatroom/readMessagesAlready': {
      return {
        ...state,
        status: 'readMessagesAlready',
        unread: action.unread,
      };
    }
    case '@@Chatroom/gotUnreadCount': {
      return {
        ...state,
        status: 'gotUnreadCount',
        unread: action.unread,
      };
    }
    default:
      return state;
  }
};
