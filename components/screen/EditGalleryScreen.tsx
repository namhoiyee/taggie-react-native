/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {useRef} from 'react';
import {useCallback} from 'react';
import {useState} from 'react';
import {useEffect} from 'react';
import {
  FlatList,
  Text,
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Alert,
  SafeAreaView,
} from 'react-native';
import {IRootState} from '../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {
  deletePhoto,
  insertGallery,
  loadUserPhotos,
} from '../../redux/userPhotos/thunk';
import {S3Link} from '../../helpers/api';
import {isAndroid} from '../../utils/platform';
import {ImageUpload, Photo} from '../../helpers/types';
import LoadingComponent from './Loading/LoadingComponent';

const {width, height} = Dimensions.get('window');
const addPhotoButton = {id: 'add', url: 'add'};
const IMAGE_SIZE = 100;
const SPACING = 10;

const EditGalleryScreen = ({navigation}: any) => {
  const loadingStatus = useSelector(
    (state: IRootState) => state.userPhotos.status,
  );
  const userId = useSelector((state: IRootState) => state.auth?.user?.id);
  const topRef = useRef<FlatList<any> | null>(null);
  const thumbRef = useRef<FlatList<any> | null>(null);
  const [activeIndex, setActiveIndex] = useState(0);
  const userGallery = useSelector(
    (state: IRootState) => state.userPhotos?.userGallery,
  );
  const [modalVisible, setModalVisible] = useState(false);
  const [newPhotoUpload, setNewPhotoUpload] =
    useState<ImageUpload | undefined>();

    const dispatch = useDispatch();

  useEffect(() => {
    if (newPhotoUpload) {
      dispatch(insertGallery(newPhotoUpload));
      setNewPhotoUpload(undefined);
      setModalVisible(false);
    }
  }, [dispatch, newPhotoUpload]);

  const scrollToActiveIndex = useCallback((index: number) => {
    console.log('scroll', index);
    setActiveIndex(index);
    topRef?.current?.scrollToOffset({
      offset: index * width,
      animated: true,
    });
    thumbRef?.current?.scrollToOffset({
      offset: index * (IMAGE_SIZE + SPACING) - width / 2 + IMAGE_SIZE / 2,
      animated: true,
    });
  }, []);

  useEffect(() => {
    dispatch(loadUserPhotos());
  }, [dispatch]);

  const deleteGallery = useCallback(
    (photo: Photo) => {
      Alert.alert('刪除相簿', '確認刪除？將無法恢復', [
        {
          text: '取消',
          style: 'cancel',
        },
        {
          text: '確認',
          style: 'destructive',
          onPress: () => dispatch(deletePhoto(photo)),
        },
      ]);
    },
    [dispatch],
  );

  const cropPhotosForGallery = useCallback(
    (imagePath: string) => {
      ImagePicker.openCropper({
        path: imagePath,
        mediaType: 'photo',
        width: 600,
        height: 1000,
        compressImageQuality: 0.8,
        forceJpg: true,
      })
        .then(croppedImage => {
          if (croppedImage) {
            setNewPhotoUpload({
              uri: croppedImage.path,
              name: `gallery-${Date.now()}-${userId}`,
              type: croppedImage.mime,
            });
            console.log('iosLibImage', croppedImage);
          }
        })
        .catch(e => console.log('crop failed', e));
    },
    [userId],
  );

  const takePhotoFromCameraForGallery = useCallback(() => {
    launchCamera(
      {
        mediaType: 'photo',
      },
      res => {
        if (res?.assets && res?.assets[0]?.uri) {
          cropPhotosForGallery(res?.assets[0]?.uri);
        }

        console.log(res);
      },
    );
  }, [cropPhotosForGallery]);

  const choosePhotoFromLibraryForProfile = useCallback(() => {
    if (!isAndroid) {
      ImagePicker.openPicker({
        mediaType: 'photo',
        compressImageQuality: 0.8,
      })
        .then(image => {
          cropPhotosForGallery(image.path);
        })
        .catch(() => {
          console.log('user did not allow camera / no camera');
        });
    } else {
      launchImageLibrary(
        {
          mediaType: 'photo',
        },
        res => {
          if (res?.assets && res?.assets[0]?.uri) {
            cropPhotosForGallery(res?.assets[0]?.uri);
          }

          console.log(res);
        },
      );
    }
  }, [cropPhotosForGallery]);

  if (userGallery?.data?.length === 0 || loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <View style={{flex: 1, backgroundColor: '#333333'}}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={{flex: 1, backgroundColor: '#333333'}}>
          <TouchableOpacity
            style={styles.centeredView}
            onPress={() => setModalVisible(!modalVisible)}>
            <View style={styles.modalView}>
              <TouchableOpacity
                style={[styles.button, styles.buttonClose, {marginTop: 10}]}
                onPress={() => takePhotoFromCameraForGallery()}>
                <MaterialIcons name="photo-camera" size={20} color="#DFD8C8" />
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttonClose, {marginTop: 10}]}
                onPress={() => choosePhotoFromLibraryForProfile()}>
                <MaterialCommunityIcons
                  name="circle-edit-outline"
                  size={20}
                  color="#DFD8C8"
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttonClose, {marginTop: 10}]}
                onPress={() => setModalVisible(false)}>
                <Text style={styles.textStyle}>返回</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </View>
      </Modal>
      {userGallery && (
        <FlatList
          ref={topRef}
          data={userGallery.data}
          keyExtractor={item => item.id.toString()}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onMomentumScrollEnd={ev => {
            console.log(
              'scroll end gallery',
              ev.nativeEvent.contentOffset.x,
              width,
              ev.nativeEvent.contentOffset.x / width,
            );
            scrollToActiveIndex(
              Math.ceil(ev.nativeEvent.contentOffset.x / width),
            );
          }}
          renderItem={({item}) => {
            return (
              <View style={{width, height}}>
                <FastImage
                  source={{
                    uri: S3Link + item.url,
                    priority: FastImage.priority.high,
                  }}
                  style={[StyleSheet.absoluteFillObject]}
                />
              </View>
            );
          }}
        />
      )}

      {userGallery && (
        <FlatList
          ref={thumbRef}
          data={[...userGallery.data, addPhotoButton]}
          keyExtractor={item => item.id.toString()}
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{position: 'absolute', bottom: 10}}
          contentContainerStyle={{paddingHorizontal: SPACING}}
          renderItem={({item, index}) => {
            return (
              <SafeAreaView>
                {item.id !== 'add' ? (
                  <TouchableOpacity onPress={() => scrollToActiveIndex(index)}>
                    <FastImage
                      source={{
                        uri: S3Link + item.url,
                        priority: FastImage.priority.high,
                      }}
                      style={{
                        width: IMAGE_SIZE,
                        height: IMAGE_SIZE,
                        borderRadius: 12,
                        marginRight: SPACING,
                        borderWidth: 2,
                        borderColor:
                          index === activeIndex ? '#fff' : 'transparent',
                      }}
                    />
                    {index !== 0 && index === activeIndex && (
                      <TouchableOpacity
                        style={{position: 'absolute', bottom: 25, right: 38}}
                        onPress={() => deleteGallery(item)}>
                        <FontAwesome5
                          name="trash"
                          size={50}
                          color="rgba(255, 255, 255, 0.7)"
                        />
                      </TouchableOpacity>
                    )}
                  </TouchableOpacity>
                ) : (
                  index < 15 && (
                    <TouchableOpacity
                      onPress={() => setModalVisible(true)}
                      style={{
                        width: IMAGE_SIZE,
                        height: IMAGE_SIZE,
                        borderRadius: 12,
                        marginRight: SPACING,
                        // borderWidth: 2,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#33333377',
                      }}>
                      <MaterialCommunityIcons
                        name="plus-thick"
                        color="#fff"
                        size={50}
                      />
                    </TouchableOpacity>
                  )
                )}
              </SafeAreaView>
            );
          }}
        />
      )}
      <View style={styles.return}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <MaterialIcons
            name={isAndroid ? 'arrow-back' : 'arrow-back-ios'}
            size={isAndroid ? 40 : 25}
            color="#333333"
            style={{transform: [{translateX: isAndroid ? 0 : 5}]}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  return: {
    position: 'absolute',
    top: isAndroid ? 10 : 50,
    justifyContent: 'center',
    alignItems: 'center',
    left: 10,
    backgroundColor: 'white',
    padding: isAndroid ? 3 : 6,
    borderRadius: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default EditGalleryScreen;
