import {UserProfileState, initialState} from './state';
import {UserProfileAction} from './action';

export const UserProfileReducer = (
  state: UserProfileState = initialState,
  action: UserProfileAction,
): UserProfileState => {
  switch (action.type) {
    case '@@Profile/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Profile/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@Profile/loadedUserProfile': {
      return {
        ...state,
        status: 'loadedUserProfile',
        userProfile: action.userProfile,
      };
    }
    case '@@Profile/loadedUserProfilePic': {
      console.log('coins');
      return {
        ...state,
        status: 'loadedUserProfilePic',
        userProfilePic: action.userProfilePic,
      };
    }
    case '@@Profile/editedUserProfile': {
      return {
        ...state,
        status: 'editedUserProfile',
        userProfile: action.userProfile,
      };
    }
    case '@@Profile/editedUserProfilePic': {
      return {
        ...state,
        status: 'editedUserProfilePic',
        userProfilePic: action.userProfilePic,
      };
    }
    case '@@Profile/updateUserCoin': {
      return {
        ...state,
        userProfile: action.userProfile,
      };
    }
    case '@@Profile/editCoins': {
      let newUserProfile = state.userProfile;
      if (state.userProfile) {
        newUserProfile = {...state.userProfile};
        newUserProfile.info.coins = action.coins;

        return {
          ...state,
          status: 'editedUserProfile',
          userProfile: newUserProfile,
        };
      } else {
        return {
          ...state,
          status: 'error',
          msg: 'unable to edit coins on profile',
        };
      }
    }
    default:
      return state;
  }
};
