import {SocketState, initialState} from './state';
import {SocketAction} from './action';

export const SocketReducer = (
  state: SocketState = initialState,
  action: SocketAction,
): SocketState => {
  switch (action.type) {
    case '@@Socket/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Socket/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@Socket/loadedSocket': {
      return {
        ...state,
        status: 'loadedSocket',
        socket: action.socket,
      };
    }
    default:
      return state;
  }
};
