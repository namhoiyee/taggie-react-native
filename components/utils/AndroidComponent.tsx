import React from 'react';
import {
  Text,
  Pressable,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from 'react-native';
import {isAndroid} from '../../utils/platform';

export const AndroidButton = (props: {
  onPress: () => void;
  title: string;
  buttonStyles: {};
  textStyles: {};
}) => {
  const {onPress, title, buttonStyles, textStyles} = props;
  return (
    <Pressable style={buttonStyles} onPress={onPress}>
      <Text style={textStyles}>{title}</Text>
    </Pressable>
  );
};

let TouchableCmp;

if (isAndroid && Platform.Version >= 21) {
  TouchableCmp = TouchableNativeFeedback;
} else {
  TouchableCmp = TouchableOpacity;
}

export const PlatformTouchable: any = TouchableCmp;
