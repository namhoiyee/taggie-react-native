import {pointLocation} from '../../helpers/types';

export type ChatroomDetail = {
  true_friend_id: number;
  username: string;
  url: string;
  user_id: number;
  friend_id: number;
  message: string;
  created_at: Date;
  unread?: string;
  room_id: string;
  place_name?: string;
};

export type ChatroomMessage = {
  id: number;
  relationship_id: number;
  message: string;
  status: string;
  created_at: string;
  sender_id: number;
  roomId?: string;
  meet_up_schedule_id: number | null;
  inviter_id: number | null;
  invitee_id: number | null;
  is_accepted: boolean | null;
  scheduled_at: string | null;
  description: string | null;
  place_name: string | null;
  location: pointLocation | null;
  rating: number | null;
  vicinity: string | null;
};

export type ChatroomState =
  | {
      status: 'loading';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'loadingChatrooms';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'error';
      msg: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'wentOutFromChatroom';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages: undefined;
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'loadedChatroomMessages';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages: ChatroomMessage[];
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'postedChatroomMessage';
      msg: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'readMessagesAlready';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread: number;
      currentRoomId?: string;
    }
  | {
      status: 'gotUnreadCount';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread: number;
      currentRoomId?: string;
    }
  | {
      status: 'loadedChatroom';
      msg?: string;
      chatroom: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread?: number;
      currentRoomId?: string;
    }
  | {
      status: 'updatedRoomId';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread?: number;
      currentRoomId: string;
    }
  | {
      status: 'updatedUnreadCount';
      msg?: string;
      chatroom?: ChatroomDetail[];
      messages?: ChatroomMessage[];
      unread: number;
      currentRoomId?: string;
    };

export const initialState: ChatroomState = {
  status: 'loading',
};
