import {ILocation, pointLocation} from '../../helpers/types';

export type GoogleSuggestMarker = {
  coordinate: ILocation;
  name: string;
  rating: number;
  detail: string;
  address: string;
  place_id: string;
  photo?: string;
};

export type Place = {
  category: string;
  place: string;
};

export type SuggestionState =
  | {
      status: 'loading';
      msg?: string;
      markers?: GoogleSuggestMarker[][];
      suggestion?: Place[];
      location?: pointLocation;
      roomId?: string;
    }
  | {
      status: 'loadedMapSuggestion';
      msg?: string;
      markers: GoogleSuggestMarker[][];
      suggestion?: Place[];
      location?: pointLocation;
      roomId?: string;
    }
  | {
      status: 'loadedSuggestPlaces';
      msg?: string;
      markers?: GoogleSuggestMarker[][];
      suggestion: Place[];
      location: pointLocation;
      roomId?: string;
    }
  | {
      status: 'updatedCurrentRoomId';
      msg?: string;
      markers?: GoogleSuggestMarker[][];
      suggestion?: Place[];
      location?: pointLocation;
      roomId: string;
    }
  | {
      status: 'wentOutFromMap';
      msg?: string;
      markers: undefined;
      suggestion?: Place[];
      location?: pointLocation;
      roomId?: string;
    }
  | {
      status: 'ready';
      msg?: string;
      markers?: GoogleSuggestMarker[][];
      suggestion?: Place[];
      location?: pointLocation;
      roomId?: string;
    }
  | {
      status: 'error';
      msg: string;
      markers?: GoogleSuggestMarker[][];
      suggestion?: Place[];
      location?: pointLocation;
      roomId?: string;
    };

export const initialState: SuggestionState = {
  status: 'loading',
};
