import {Region} from 'react-native-maps';
import {MarkerInfo} from '../../helpers/types';

export type MapState =
  | {
      status: 'loading';
      msg?: string;
      whoLikeU?: MarkerInfo[];
      region?: Region;
    }
  | {
      status: 'error';
      msg: string;
      whoLikeU?: MarkerInfo[];
      region?: Region;
    }
  | {
      status: 'ready';
      msg?: string;
      whoLikeU: MarkerInfo[];
      region?: Region;
    };

export const initialState: MapState = {
  status: 'loading',
  whoLikeU: [],
  region: {
    latitude: 22.2860375,
    longitude: 114.1486955,
    latitudeDelta: 0.004864195044303443,
    longitudeDelta: 0.0040142817690068,
  },
};
