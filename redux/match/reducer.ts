import {MatchState} from './state';
import {initialState} from './state';
import {MatchAction} from './action';

export const matchReducer = (
  state: MatchState = initialState,
  action: MatchAction,
): MatchState => {
  switch (action.type) {
    case '@@Match/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Match/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@Match/init':
      return {
        ...state,
        status: 'located',
        location: action.userLocation,
      };
    case '@@Match/matched':
      return {
        ...state,
        status: 'matched',
        matchResult: action.matchResult,
      };
    case '@@Match/updatedMatchResult':
      return {
        ...state,
        status: 'updatedMatchResult',
        matchResult: action.matchResult,
      };
    case '@@Match/inserted':
      return {
        ...state,
        status: 'insertedGeolocation',
        insertedGeoTime: action.time,
      };
    case '@@Match/likedPeople':
      return {
        ...state,
        status: 'likedPeople',
        likeStatus: action.likeStatus,
      };
    case '@@Match/unlikedPeople':
      return {
        ...state,
        status: 'unlikedPeople',
        likeStatus: action.likeStatus,
      };
    case '@@Match/checkedIfAfter15MinsFromLastMatch':
      return {
        ...state,
        status: 'checkedIfAfter15MinsFromLastMatch',
        isAfter15Mins: action.isAfter15Mins,
      };
    case '@@Match/user-permission':
      return {
        ...state,
        status: 'gotUserPermission',
        userPermitted: action.userPermitted,
      };
    default:
      return state;
  }
};
