/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';
import {View, Button, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import LottieView from 'lottie-react-native';
import {crushHappenHandle} from '../../../redux/crush/thunk';
import {S3Link} from '../../../helpers/api';
import {CrushFriendInfo} from '../../../redux/crush/state';
import FastImage from 'react-native-fast-image';

function CrushScreen(props: {navigation: any; userPresent: CrushFriendInfo}) {
  const dispatch = useDispatch();

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: tabBarHeight,
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                marginVertical: 15,
                borderRadius: 200,
                overflow: 'hidden',
              }}>
              <FastImage
                source={{uri: S3Link + props?.userPresent.picture?.url}}
                style={{height: 200, width: 200}}
              />
            </View>
            <View style={{marginVertical: 15}}>
              <Text style={{fontSize: 24}}>
                🥳{props?.userPresent.username}和你結為朋友!🥳
              </Text>
            </View>
            <LottieView
              source={require('../../../src/animations/handshake.json')}
              autoPlay
              loop
              style={{height: 100, width: 100}}
            />
            <View style={{marginVertical: 15}}>
              <Button
                title="進入房間"
                onPress={() => {
                  dispatch(crushHappenHandle());
                  props?.navigation.navigate('ChatroomDetail', {
                    detail: {
                      name: props?.userPresent.username,
                      profilePic: S3Link + props?.userPresent.picture.url,
                      room_id: props?.userPresent.room,
                    },
                  });
                }}
              />
            </View>
          </View>
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
}
export default CrushScreen;
