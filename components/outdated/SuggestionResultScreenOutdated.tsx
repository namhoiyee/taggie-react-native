/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useCallback, useState} from 'react';
import {
  StyleSheet,
  Text,
  // TextInput,
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  Dimensions,
  Platform,
  // SafeAreaView,
  Modal,
  Button,
  FlatList,
  TouchableWithoutFeedback,
  // Image,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';

// import Ionicons from 'react-native-vector-icons/Ionicons';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import Fontisto from 'react-native-vector-icons/Fontisto';
import {useAnimatedRef} from 'react-native-reanimated';

// import { markers } from '../../model/mapData';
import {mapDarkStyle, mapStandardStyle} from '../../../model/googleMapStyle';
// import StarRating from '../../model/StarRating';
import {useTheme} from '@react-navigation/native';
// import FastImage from 'react-native-fast-image';
import {IRootState} from '../../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
// import {S3Link} from '../../helpers/api';
// import Modal from 'react-native-modal';
import LoadingComponent from '../Loading/LoadingComponent';
import {isAndroid} from '../../../utils/platform';
import {placeImageReq, Region} from '../../../helpers/types';
import {googleResult} from '../../googleMap/mapData';
import StarRating from '../../../model/StarRating';
import FastImage from 'react-native-fast-image';
import {GoogleSuggestMarker} from '../../../redux/suggestion/state';
import {
  loadMapSuggestion,
  loadSuggestPlaces,
} from '../../../redux/suggestion/thunk';
// import {markers} from '../../model/mapData';

const {width} = Dimensions.get('window');
const CARD_HEIGHT = 200;
const CARD_WIDTH = width * 0.4;
const SPACING_FOR_CARD_INSET = width * 0.3 - 10;

// const markers: GoogleSuggestMarker[] = googleResult.results
//   .filter(res => res.business_status === 'OPERATIONAL')
//   .map(res => ({
//     coordinate: {
//       latitude: res.geometry.location.lat,
//       longitude: res.geometry.location.lng,
//     },
//     name: res.name,
//     rating: res.rating,
//     detail: res.detail,
//     address: res.address,
//     place_id: res.place_id,
//     photo: res.photo,
//   }));

const SuggestionResultScreen = ({navigation, route}: any) => {
  const theme = useTheme();
  const loadingStatus = useSelector(
    (state: IRootState) => state.suggestion.status,
  );
  // const room_id = useSelector(
  //   (state: IRootState) => state.chatroom.currentRoomId,
  // );
  const {room_id} = route?.params;
  console.log('room_id', room_id);
  const markersByCategories = useSelector(
    (state: IRootState) => state.suggestion.markers,
  );
  const suggestion = useSelector(
    (state: IRootState) => state.suggestion.suggestion,
  );
  const location = useSelector(
    (state: IRootState) => state.suggestion.location,
  );
  // const [state] = useState();
  const region = useSelector((state: IRootState) => state.map.region);
  // const markers = useSelector((state: IRootState) => state.map.whoLikeU);
  const mapIndex = useRef(0);
  // const selectedUserIndex = useRef<number | undefined>();
  const mapAnimation = useRef(new Animated.Value(0)).current;
  const _map = useAnimatedRef<MapView>();
  const _scrollView = useAnimatedRef<ScrollView>();
  const [modalVisible, setModalVisible] = useState(false);
  const [currentClickedIndex, setCurrentClickedIndex] = useState(0);
  const currentRegion = useRef<Region | undefined>(region);
  const [currentClickedPlaceDetail, setCurrentClickedPlaceDetail] =
    useState<GoogleSuggestMarker | undefined>();
  // const selectedMarker = markers[selectedUserIndex.current!];
  const markers = useRef<GoogleSuggestMarker[]>(
    googleResult.results
      .filter((res: any) => res.business_status === 'OPERATIONAL')
      .map((res: any) => ({
        coordinate: {
          latitude: res.geometry.location.lat,
          longitude: res.geometry.location.lng,
        },
        name: res.name,
        rating: res.rating,
        detail: '中菜',
        address: res.formatted_address,
        place_id: res.place_id,
        photo: res.photos?.[0]?.photo_reference,
      })),
    // .slice(0, 5),
  );

  const dispatch = useDispatch();

  // useEffect(() => {
  //   if (room_id && room_id !== '') {
  //     dispatch(loadSuggestPlaces(room_id));
  //   }
  // }, [dispatch, room_id]);

  // console.log('loadingStatus', loadingStatus, location, suggestion);
  // useEffect(() => {
  //   console.log(
  //     loadingStatus === 'loadedSuggestPlaces',
  //     !!location,
  //     // !!places.suggestion,
  //     // !!places.location,
  //   );
  //   if (loadingStatus === 'loadedSuggestPlaces' && location && suggestion) {
  //     console.log('hey');
  //     dispatch(loadMapSuggestion(suggestion, location));
  //   }
  // }, [dispatch, loadingStatus, location, suggestion]);

  // console.log('map load....', markersByCategories);
  useEffect(() => {
    // if (markersByCategories) {
    const tempMarkerArray: GoogleSuggestMarker[] = [];
    // markersByCategories.forEach(cat =>
    markers.current.forEach(marker => tempMarkerArray.push(marker));
    // );
    markers.current = tempMarkerArray;
    console.log(
      'markers.current',
      markers.current.map(m => m.name),
    );
    // }
  }, []);

  // console.log('makers arr:', markers);
  useEffect(() => {
    mapAnimation.addListener(({value}) => {
      // if (loadingStatus !== 'loadedMapSuggestion' && !markers.current) {
      //   return;
      // }
      let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
      console.log('scroll', value, index);
      if (
        markers.current &&
        markers.current.length > 0 &&
        index >= markers.current.length
      ) {
        index = markers.current.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }

      // setTimeout(() => {

      if (
        mapIndex.current !== index &&
        markers.current &&
        region &&
        currentRegion.current
      ) {
        mapIndex.current = index;
        const {coordinate} = markers.current[index];
        _map?.current?.animateToRegion(
          {
            ...coordinate,
            latitudeDelta: currentRegion.current.latitudeDelta,
            longitudeDelta: currentRegion.current.longitudeDelta,
          },
          1000,
        );
      }
      // }, 10);

      // clearTimeout(regionTimeout);
    });
  }, [
    mapAnimation,
    region?.latitudeDelta,
    region?.longitudeDelta,
    _map,
    region,
    loadingStatus,
  ]);

  // const interpolations =
  //   markers.current &&
  //   markers.current.map((marker, index) => {
  //     const inputRange = [
  //       (index - 1) * CARD_WIDTH,
  //       index * CARD_WIDTH,
  //       (index + 1) * CARD_WIDTH,
  //     ];

  //     const scale = mapAnimation.interpolate({
  //       inputRange,
  //       outputRange: [1, 2, 1],
  //       extrapolate: 'clamp',
  //     });

  //     return {scale};
  //   });

  // const toggleModal = () => {
  //   setModalVisible(!isModalVisible);
  // };

  // const onPhotoPress = (index: number) => {
  //   selectedUserIndex.current = index;
  //   toggleModal();
  // };

  const onMarkerPress = useCallback(
    (index: number) => {
      // const markerID = mapEventData._targetInst.return.key;
      // console.log('marker pressed', index);
      console.log('google map index', index);
      mapIndex.current = index;

      let x = (index + 1) * CARD_WIDTH + (index + 1) * 20;
      if (Platform.OS === 'ios') {
        x = x - SPACING_FOR_CARD_INSET;
      }

      _scrollView?.current?.scrollTo({x: x, y: 0, animated: true});
    },
    [_scrollView],
  );

  // const scrollToActiveIndex = useCallback((index: number) => {
  //   console.log('scroll', index);
  //   setActiveIndex(index);
  //   topRef?.current?.scrollToOffset({
  //     offset: index * width,
  //     animated: true,
  //   });
  //   thumbRef?.current?.scrollToOffset({
  //     offset: index * (IMAGE_SIZE + SPACING) - width / 2 + IMAGE_SIZE / 2,
  //     animated: true,
  //   });
  //   // } else {
  //   //   thumbRef?.current?.scrollToOffset({
  //   //     offset: 0,
  //   //     animated: true,
  //   //   });
  //   // }
  // }, []);

  const onRegionChangeComplete = useCallback((mapRegion: Region) => {
    currentRegion.current = mapRegion;
  }, []);

  // if (loadingStatus === 'loading') {
  //   return <LoadingComponent navigation={navigation} />;
  // }

  return (
    <View style={styles.container}>
      {markers && (
        <>
          <MapView
            ref={_map}
            initialRegion={
              markers.current && region
                ? {
                    latitude: markers.current[0].coordinate.latitude,
                    longitude: markers.current[0].coordinate.longitude,
                    latitudeDelta: region?.latitudeDelta,
                    longitudeDelta: region?.longitudeDelta,
                  }
                : region
            }
            style={styles.container}
            provider={PROVIDER_GOOGLE}
            onRegionChangeComplete={onRegionChangeComplete}
            customMapStyle={theme.dark ? mapDarkStyle : mapStandardStyle}>
            {markers.current &&
              markers.current.map((marker, index) => {
                const scaleStyle = 0
                  ? {
                      transform: [
                        {
                          scale: index === mapIndex.current ? 2 : 1,
                        },
                      ],
                    }
                  : {};
                return (
                  <Marker
                    key={index}
                    coordinate={marker.coordinate}
                    onPress={() => onMarkerPress(index)}>
                    <Animated.View style={[styles.markerWrap]}>
                      <Animated.Image
                        // @ts-ignore
                        source={placeImageReq[marker.detail]}
                        // source={require('../../src/image/map_marker.png')}
                        style={[styles.marker, scaleStyle]}
                        resizeMode="cover"
                      />
                    </Animated.View>
                  </Marker>
                );
              })}
          </MapView>
          <FlatList
            ref={topRef}
            horizontal
            pagingEnabled
            // scrollEventThrottle={100}
            showsHorizontalScrollIndicator={false}
            style={styles.scrollView}
            contentInset={{
              top: 0,
              left: SPACING_FOR_CARD_INSET,
              bottom: 0,
              right: SPACING_FOR_CARD_INSET,
            }}
            contentContainerStyle={{
              paddingHorizontal:
                Platform.OS === 'android' ? SPACING_FOR_CARD_INSET : 0,
            }}
            onMomentumScrollEnd={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: mapAnimation,
                    },
                  },
                },
              ],
              {useNativeDriver: true},
            )}>
            {markers.current &&
              markers.current.map((marker, index) => (
                <TouchableOpacity
                  style={styles.card}
                  key={index}
                  onPress={() => {
                    setCurrentClickedPlaceDetail({
                      coordinate: marker.coordinate,
                      name: marker.name,
                      rating: marker.rating,
                      detail: marker.detail,
                      address: marker.address,
                      place_id: marker.place_id,
                      photo: marker.photo,
                    });
                    setModalVisible(true);
                  }}>
                  {/* <Image
              source={marker.image}
              style={styles.cardImage}
              resizeMode="cover"
            /> */}
                  <View style={styles.textContent}>
                    <View style={{marginVertical: 5}}>
                      <Text numberOfLines={2} style={styles.cardtitle}>
                        {marker.name}
                      </Text>
                    </View>
                    <View style={{marginVertical: 5}}>
                      <Text numberOfLines={1} style={styles.cardtitle}>
                        {marker.detail}
                      </Text>
                    </View>
                    <View style={{marginVertical: 5}}>
                      <Text numberOfLines={3} style={styles.cardtitle}>
                        {marker.address}
                      </Text>
                    </View>

                    <StarRating ratings={marker.rating} />
                    {/* <Text numberOfLines={1} style={styles.cardDescription}>
                {marker.description}
              </Text> */}
                    <View style={styles.button}>
                      <TouchableOpacity
                        onPress={() => {
                          navigation.navigate('Scheduling', {
                            venue: marker.name,
                            detail: marker.detail,
                            address: marker.address,
                            location: marker.coordinate,
                            rating: marker.rating,
                            place_id: marker.place_id,
                            room_id: room_id,
                          });
                        }}
                        style={[
                          styles.signIn,
                          {
                            borderColor: '#FF6347',
                            borderWidth: 1,
                          },
                        ]}>
                        <Text
                          style={[
                            styles.textSign,
                            {
                              color: '#FF6347',
                            },
                          ]}>
                          在此見面
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
              ))}
          </Animated.ScrollView>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(false);
            }}>
            <TouchableWithoutFeedback
              style={styles.centeredView}
              onPress={() => setModalVisible(false)}>
              {currentClickedPlaceDetail && (
                <View style={styles.modalView}>
                  <View style={styles.detailContent}>
                    <View style={{width: '80%'}}>
                      <View style={{marginVertical: 5}}>
                        <Text style={styles.cardtitle}>
                          {currentClickedPlaceDetail.name}
                        </Text>
                      </View>
                      <View style={{marginVertical: 5}}>
                        <Text style={styles.cardtitle}>
                          {currentClickedPlaceDetail.detail}
                        </Text>
                      </View>
                      <View style={{marginVertical: 5}}>
                        <Text style={styles.cardtitle}>
                          {currentClickedPlaceDetail.address}
                        </Text>
                      </View>

                      <StarRating ratings={currentClickedPlaceDetail.rating} />
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginHorizontal: 15,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <FastImage
                        //@ts-ignore
                        source={placeImageReq[currentClickedPlaceDetail.detail]}
                        // source={require('../../src/image/map_marker.png')}
                        style={styles.markerModalPic}
                        resizeMode="cover"
                      />
                    </View>
                  </View>
                  <View style={{marginVertical: 10}}>
                    <Button
                      title="確認"
                      // style={[styles.button, styles.buttonClose, {marginTop: 10}]}
                      onPress={() => setModalVisible(false)}
                    />
                    {/* <Text style={styles.textStyle}>確認</Text> */}
                  </View>
                </View>
              )}
            </TouchableWithoutFeedback>
          </Modal>
          {loadingStatus !== 'loadedSuggestPlaces' && (
            <View style={styles.return}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <MaterialIcons
                  name={isAndroid ? 'arrow-back' : 'arrow-back-ios'}
                  size={30}
                  color="#333333"
                  style={{transform: [{translateX: isAndroid ? 0 : 5}]}}
                />
              </TouchableOpacity>
            </View>
          )}
        </>
      )}
    </View>
  );
};

export default SuggestionResultScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
  },
  chipsScrollView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 90 : 80,
    paddingHorizontal: 10,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 5,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width: 2, height: -2},
    marginVertical: 20,
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: 'hidden',
  },
  cardImage: {
    flex: 4,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    borderRadius: 8,
  },
  textContent: {
    flex: 1,
    padding: 10,
    // flexDirection: 'row',
    alignSelf: 'center',
  },
  cardtitle: {
    fontSize: 14,
    // marginTop: 5,
    fontWeight: 'bold',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,
    borderRadius: 1000,
    overflow: 'hidden',
  },
  marker: {
    width: 42,
    height: 42,
    borderRadius: 1000,
    overflow: 'hidden',
  },
  markerModalPic: {
    width: 100,
    height: 100,
    borderRadius: 1000,
    overflow: 'hidden',
    marginRight: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
  },
  signIn: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  return: {
    position: 'absolute',
    top: isAndroid ? 10 : 50,
    justifyContent: 'center',
    alignItems: 'center',
    left: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    borderRadius: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  modalView: {
    width: '100%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 5,
  },
  detailContent: {
    // flex: 1,
    padding: 10,
    flexDirection: 'row',
    // alignSelf: 'center',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
