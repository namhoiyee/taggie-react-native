/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useCallback, useState} from 'react';
import {
  StyleSheet,
  Text,
  // TextInput,
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  Dimensions,
  Platform,
  // SafeAreaView,
  Modal,
  Button,
  FlatList,
  TouchableWithoutFeedback,
  // Image,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';

// import Ionicons from 'react-native-vector-icons/Ionicons';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import Fontisto from 'react-native-vector-icons/Fontisto';
import {scrollTo, useAnimatedRef} from 'react-native-reanimated';

// import { markers } from '../../model/mapData';
import {mapDarkStyle, mapStandardStyle} from '../../../model/googleMapStyle';
// import StarRating from '../../model/StarRating';
import {useTheme} from '@react-navigation/native';
// import FastImage from 'react-native-fast-image';
import {IRootState} from '../../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
// import {S3Link} from '../../helpers/api';
// import Modal from 'react-native-modal';
import LoadingComponent from '../Loading/LoadingComponent';
import {isAndroid} from '../../../utils/platform';
import {placeImageReq, Region} from '../../../helpers/types';
import {googleResult} from '../../googleMap/mapData';
import StarRating from '../../../model/StarRating';
import FastImage from 'react-native-fast-image';
import {GoogleSuggestMarker} from '../../../redux/suggestion/state';
import {
  loadMapSuggestion,
  loadSuggestPlaces,
} from '../../../redux/suggestion/thunk';
import {Google} from '../../../helpers/api';
import {updatedCurrentRoomId} from '../../../redux/suggestion/action';
// import {markers} from '../../model/mapData';

const {width} = Dimensions.get('window');
const CARD_HEIGHT = 240;
const CARD_WIDTH = 150;
const SPACING = 10;

const SuggestionResultScreen = ({navigation, route}: any) => {
  const theme = useTheme();
  const loadingStatus = useSelector(
    (state: IRootState) => state.suggestion.status,
  );
  // const room_id = useSelector(
  //   (state: IRootState) => state.chatroom.currentRoomId,
  // );
  const {room_id} = route?.params;
  const roomIdSaved = useSelector(
    (state: IRootState) => state.suggestion.roomId,
  );
  console.log('in room roomIdSaved', roomIdSaved);
  const thumbRef = useRef<FlatList<any> | null>(null);
  console.log('room_id', room_id);
  const markersByCategories = useSelector(
    (state: IRootState) => state.suggestion.markers,
  );
  const suggestion = useSelector(
    (state: IRootState) => state.suggestion.suggestion,
  );
  const location = useSelector(
    (state: IRootState) => state.suggestion.location,
  );
  // const [state] = useState();
  // const region = useSelector((state: IRootState) => state.map.region);
  // const markers = useSelector((state: IRootState) => state.map.whoLikeU);
  const mapIndex = useRef(0);
  // const selectedUserIndex = useRef<number | undefined>();
  const _map = useAnimatedRef<MapView>();
  const [modalVisible, setModalVisible] = useState(false);
  const [currentClickedIndex, setCurrentClickedIndex] = useState(0);
  const currentRegion = useRef<Region | undefined>();
  const [currentClickedPlaceDetail, setCurrentClickedPlaceDetail] =
    useState<GoogleSuggestMarker | undefined>();
  // const selectedMarker = markers[selectedUserIndex.current!];
  const markers = useRef<GoogleSuggestMarker[]>();
  const [isReady, setIsReady] = useState(false);
  // .slice(0, 5),

  const dispatch = useDispatch();

  useEffect(() => {
    setIsReady(false);
    if (room_id && room_id !== '' /* && room_id !== roomIdSaved */) {
      console.log('loadSuggestPlaces first', room_id);
      dispatch(loadSuggestPlaces(room_id));
    }
  }, [dispatch, roomIdSaved, room_id]);

  console.log('loadingStatus', loadingStatus, location, suggestion);
  useEffect(() => {
    console.log(
      'checking status',
      loadingStatus === 'loadedSuggestPlaces',
      !!location,
      !!suggestion,
      room_id,
      roomIdSaved,
      // !!places.suggestion,
      // !!places.location,
    );
    if (
      // room_id !== roomIdSaved &&
      loadingStatus === 'loadedSuggestPlaces' &&
      location &&
      suggestion
    ) {
      console.log('loadMapSuggestion first');
      dispatch(loadMapSuggestion(suggestion, location));
      // dispatch(updatedCurrentRoomId(room_id));
    }
  }, [dispatch, loadingStatus, location, roomIdSaved, room_id, suggestion]);

  useEffect(() => {
    if (markersByCategories) {
      const tempMarkerArray: GoogleSuggestMarker[] = [];
      markersByCategories.forEach(cat =>
        cat.forEach(marker => tempMarkerArray.push(marker)),
      );
      markers.current = tempMarkerArray;
      console.log(
        'markers.current to map',
        markers.current.map(m => m.name),
      );
      setIsReady(true);
    }
  }, [markersByCategories]);

  const scrollToActiveIndex = useCallback(
    (index: number) => {
      console.log('scroll', index);
      // setActiveIndex(index);
      console.log(
        'width',
        CARD_WIDTH,
        SPACING,
        width,
        CARD_WIDTH + SPACING - width / 2 + CARD_WIDTH / 2,
      );
      // thumbRef?.current?.scrollToIndex({
      //   index: index,
      //   animated: true,
      // });

      thumbRef?.current?.scrollToOffset({
        offset:
          index * (CARD_WIDTH + SPACING * 2) -
          width / 2 +
          CARD_WIDTH / 2 +
          SPACING * 2,
        animated: true,
      });

      const coordinate = markers.current
        ? markers.current[index].coordinate
        : {latitude: 22.2876, longitude: 114.14816};
      setCurrentClickedIndex(index);

      if (currentRegion.current) {
        _map?.current?.animateToRegion(
          {
            ...coordinate,
            latitudeDelta: currentRegion.current.latitudeDelta,
            longitudeDelta: currentRegion.current.longitudeDelta,
          },
          200,
        );
      }
    },
    [_map],
  );

  const onMarkerPress = useCallback(
    (index: number) => {
      console.log('google map index', index);
      mapIndex.current = index;

      scrollToActiveIndex(index);
    },
    [scrollToActiveIndex],
  );

  const onRegionChangeComplete = useCallback((mapRegion: Region) => {
    currentRegion.current = mapRegion;
  }, []);

  // if (loadingStatus === 'loading') {
  //   return <LoadingComponent navigation={navigation} />;
  // }

  console.log(
    'before render view',
    markersByCategories?.length,
    markers.current?.map(m => m.name),
  );

  return (
    <View style={styles.container}>
      {markers.current && isReady && (
        <>
          <MapView
            ref={_map}
            initialRegion={
              markers.current
                ? {
                    latitude: markers.current[0].coordinate.latitude,
                    longitude: markers.current[0].coordinate.longitude,
                    latitudeDelta: 0.004864195044303443,
                    longitudeDelta: 0.0040142817690068,
                  }
                : {
                    latitude: 22.2860375,
                    longitude: 114.1486955,
                    latitudeDelta: 0.004864195044303443,
                    longitudeDelta: 0.0040142817690068,
                  }
            }
            style={styles.container}
            provider={PROVIDER_GOOGLE}
            onRegionChangeComplete={onRegionChangeComplete}
            customMapStyle={theme.dark ? mapDarkStyle : mapStandardStyle}>
            {markers.current &&
              markers.current.map((marker, index) => {
                const scaleStyle = {
                  transform: [
                    {
                      scale: index === currentClickedIndex ? 2 : 1,
                    },
                  ],
                };

                return (
                  <Marker
                    key={index}
                    coordinate={marker.coordinate}
                    onPress={() => {
                      console.log('marker pressed', index);
                      onMarkerPress(index);
                    }}>
                    <Animated.View style={[styles.markerWrap]}>
                      <Animated.Image
                        // @ts-ignore
                        source={placeImageReq[marker.detail]}
                        // source={require('../../src/image/map_marker.png')}
                        style={[
                          styles.marker,
                          scaleStyle,
                          index === currentClickedIndex
                            ? {borderWidth: 5, borderColor: 'red'}
                            : {},
                        ]}
                        resizeMode="cover"
                      />
                    </Animated.View>
                  </Marker>
                );
              })}
          </MapView>
          <FlatList
            ref={thumbRef}
            horizontal
            data={markers.current}
            keyExtractor={item => item.place_id}
            contentContainerStyle={{paddingHorizontal: SPACING}}
            showsHorizontalScrollIndicator={false}
            style={styles.scrollView}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  style={[
                    styles.card,
                    index === currentClickedIndex
                      ? {
                          borderWidth: 1,
                          borderColor: 'red',
                          transform: [{translateY: -20}],
                        }
                      : {},
                  ]}
                  onPress={() => {
                    scrollToActiveIndex(index);
                  }}>
                  <View style={styles.textContent}>
                    <View style={{marginVertical: 5}}>
                      <Text numberOfLines={2} style={styles.cardtitle}>
                        {item.name}
                      </Text>
                    </View>
                    <View style={{marginVertical: 5}}>
                      <Text numberOfLines={1} style={styles.cardtitle}>
                        {item.detail}
                      </Text>
                    </View>
                    <View style={{marginVertical: 5}}>
                      <Text numberOfLines={3} style={styles.cardtitle}>
                        {item.address}
                      </Text>
                    </View>

                    <StarRating ratings={item.rating} />
                    <View style={{flex: 1, justifyContent: 'flex-end'}}>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {
                            setCurrentClickedPlaceDetail({
                              coordinate: item.coordinate,
                              name: item.name,
                              rating: item.rating,
                              detail: item.detail,
                              address: item.address,
                              place_id: item.place_id,
                              photo: item.photo,
                            });
                            setModalVisible(true);
                          }}
                          style={[
                            styles.signIn,
                            {
                              borderColor: '#FF6347',
                              borderWidth: 1,
                            },
                          ]}>
                          <Text
                            style={[
                              styles.textSign,
                              {
                                color: '#FF6347',
                              },
                            ]}>
                            了解更多
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {
                            navigation.navigate('Scheduling', {
                              venue: item.name,
                              detail: item.detail,
                              address: item.address,
                              location: item.coordinate,
                              rating: item.rating,
                              place_id: item.place_id,
                              room_id: room_id,
                            });
                          }}
                          style={[
                            styles.signIn,
                            {
                              borderColor: '#FF6347',
                              borderWidth: 1,
                            },
                          ]}>
                          <Text
                            style={[
                              styles.textSign,
                              {
                                color: '#FF6347',
                              },
                            ]}>
                            在此見面
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(false);
            }}>
            <TouchableWithoutFeedback
              style={styles.centeredView}
              onPress={() => setModalVisible(false)}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 20,
                }}>
                {currentClickedPlaceDetail && (
                  <View style={styles.modalView}>
                    <View style={styles.detailContent}>
                      <View style={{width: '80%'}}>
                        <View style={{marginVertical: 5}}>
                          <Text style={styles.cardtitle}>
                            {currentClickedPlaceDetail.name}
                          </Text>
                        </View>
                        <View style={{marginVertical: 5}}>
                          <Text style={styles.cardtitle}>
                            {currentClickedPlaceDetail.detail}
                          </Text>
                        </View>
                        <View style={{marginVertical: 5}}>
                          <Text style={styles.cardtitle}>
                            {currentClickedPlaceDetail.address}
                          </Text>
                        </View>

                        <StarRating
                          ratings={currentClickedPlaceDetail.rating}
                        />
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          marginHorizontal: 15,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <FastImage
                          source={
                            currentClickedPlaceDetail.photo
                              ? {
                                  uri: `https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&maxheight=1000&photoreference=${currentClickedPlaceDetail.photo}&key=${Google}`,
                                }
                              : //@ts-ignore
                                placeImageReq[currentClickedPlaceDetail.detail]
                          }
                          // source={require('../../src/image/map_marker.png')}
                          style={styles.markerModalPic}
                          resizeMode="cover"
                        />
                      </View>
                    </View>
                    <View style={{marginVertical: 10}}>
                      <Button
                        title="確認"
                        // style={[styles.button, styles.buttonClose, {marginTop: 10}]}
                        onPress={() => setModalVisible(false)}
                      />
                      {/* <Text style={styles.textStyle}>確認</Text> */}
                    </View>
                  </View>
                )}
              </View>
            </TouchableWithoutFeedback>
          </Modal>
          {loadingStatus !== 'loadedSuggestPlaces' && (
            <View style={styles.return}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <MaterialIcons
                  name={isAndroid ? 'arrow-back' : 'arrow-back-ios'}
                  size={30}
                  color="#333333"
                  style={{transform: [{translateX: isAndroid ? 0 : 5}]}}
                />
              </TouchableOpacity>
            </View>
          )}
        </>
      )}
    </View>
  );
};

export default SuggestionResultScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
  },
  chipsScrollView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 90 : 80,
    paddingHorizontal: 10,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 5,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width: 2, height: -2},
    marginVertical: 20,
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    marginRight: SPACING,
    overflow: 'hidden',
  },
  cardImage: {
    flex: 4,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    borderRadius: 8,
  },
  textContent: {
    flex: 1,
    padding: 10,
    // flexDirection: 'row',
    alignSelf: 'center',
  },
  cardtitle: {
    fontSize: 14,
    // marginTop: 5,
    fontWeight: 'bold',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,
    borderRadius: 1000,
    overflow: 'hidden',
  },
  marker: {
    width: 42,
    height: 42,
    borderRadius: 1000,
    overflow: 'hidden',
  },
  markerModalPic: {
    width: 100,
    height: 100,
    borderRadius: 1000,
    overflow: 'hidden',
    marginRight: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
  },
  signIn: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  return: {
    position: 'absolute',
    top: isAndroid ? 10 : 50,
    justifyContent: 'center',
    alignItems: 'center',
    left: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    borderRadius: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  modalView: {
    width: '100%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 5,
  },
  detailContent: {
    // flex: 1,
    padding: 10,
    flexDirection: 'row',
    // alignSelf: 'center',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
