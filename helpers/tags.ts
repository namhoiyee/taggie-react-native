export const exclusiveClassesForWishTags = [/* '習慣2', '習慣1', '尋求' */ ''];
export const exclusiveClassesForUserTags = [
  '性別',
  '年齡',
  '星座',
  '習慣1',
  '習慣2',
  '尋求',
];
export const highPriorityForUserTags = ['習慣2', '習慣1', '尋求'];
