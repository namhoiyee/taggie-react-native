import React from 'react';
import {useTheme} from 'react-native-paper';
import {createStackNavigator} from '@react-navigation/stack';
import UserWishTagScreen from '../screen/UserWishTagScreen';

const UserWishTagStack = createStackNavigator();

export const UserWishTagStackNavigator = ({_navigation}: any) => {
  const {colors} = useTheme();

  return (
    <UserWishTagStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#efefef',
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
      }}>
      <UserWishTagStack.Screen
        name="UserWishTag"
        component={UserWishTagScreen}
      />
    </UserWishTagStack.Navigator>
  );
};
