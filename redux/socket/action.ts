import {Socket} from 'socket.io-client';

export function loadedSocket(socket: Socket) {
  return {
    type: '@@Socket/loadedSocket' as const,
    socket,
  };
}

export function loading() {
  return {
    type: '@@Socket/loading' as const,
  };
}

export function failed(msg: string) {
  console.log('socket', msg);
  return {
    type: '@@Socket/failed' as const,
    msg,
  };
}

export type SocketAction =
  | ReturnType<typeof loadedSocket>
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>;
