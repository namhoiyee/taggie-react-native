import {Socket} from 'socket.io-client';

export type SocketState =
  | {
      status: 'loading';
      msg?: string;
      socket?: Socket;
    }
  | {
      status: 'error';
      msg: string;
      socket?: Socket;
    }
  | {
      status: 'loadedSocket';
      msg?: string;
      socket: Socket;
    };

export const initialState: SocketState = {
  status: 'loading',
};
