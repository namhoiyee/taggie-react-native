/* eslint-disable react-native/no-inline-styles */
import React, {useLayoutEffect, useState, useEffect, useCallback} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  FlatList,
  TextInput,
  SafeAreaView,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Linking,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Octicons from 'react-native-vector-icons/Octicons';
import {useDispatch, useSelector} from 'react-redux';
import FastImage from 'react-native-fast-image';
import {Colors} from '../../utils/Colors';
import {isAndroid} from '../../utils/platform';
import {IRootState} from '../../redux/store';
import {formatTime} from '../utils/calculateTime';
import {
  loadChatroomMessages,
  postChatroomMessage,
  readMessages,
} from '../../redux/chatroom/thunk';
import {ChatroomMessage} from '../../redux/chatroom/state';
import LoadingComponent from './Loading/LoadingComponent';
import StarRating from '../../model/StarRating';
import {pointLocation} from '../../helpers/types';
import {replySchedule} from '../../redux/schedule/thunk';
import {SocketScheduleData} from '../../redux/schedule/state';
import {wentOutFromChatroom} from '../../redux/chatroom/action';

const SPACING = 20;

const ChatScreen = ({navigation, route}: any) => {
  const loadingStatus = useSelector(
    (state: IRootState) => state.chatroom.status,
  );
  const user = useSelector((state: IRootState) => state.auth?.user);
  const userToken = useSelector((state: IRootState) => state.auth?.token);
  const socket = useSelector((state: IRootState) => state.socket.socket);
  const [input, setInput] = useState('');
  const [messageBarHeight, setMessageBarHeight] = useState(40);
  const {name, profilePic, room_id, friend_id} = route?.params?.detail;
  const chatroomMessages = useSelector(
    (state: IRootState) => state.chatroom?.messages,
  );
  const [allMessages, setAllMessages] = useState<ChatroomMessage[]>([]);
  const [readChatroom, setReadChatroom] = useState(false);
  const [initLoadMessage, setInitLoadMessage] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener('beforeRemove', () => {
      console.log('going out');
      dispatch(wentOutFromChatroom());
    });

    return unsubscribe;
  }, [dispatch, navigation]);

  useEffect(() => {
    if (initLoadMessage) {
      dispatch(loadChatroomMessages(room_id));
      setInitLoadMessage(false);
    }
  }, [dispatch, room_id, initLoadMessage]);

  useEffect(() => {
    if (chatroomMessages && !readChatroom) {
      setAllMessages(chatroomMessages);
      if (!readChatroom) {
        console.log('read chatroom');
        dispatch(readMessages(room_id));
        setReadChatroom(true);
      }
    }
  }, [chatroomMessages, dispatch, readChatroom, room_id]);

  useEffect(() => {
    if (userToken && socket) {
      const messageUpdate = (message: ChatroomMessage) => {
        console.log('message socket', message, 'userId', user?.id);
        console.log('message socket roomId', room_id, message.roomId);
        if (room_id === message.roomId) {
          setAllMessages(prev => [message, ...prev]);
          dispatch(readMessages(room_id));
        }
      };

      const scheduleUpdate = (scheduleStatus: SocketScheduleData) => {
        console.log(
          'scheduleUpdate socket',
          scheduleStatus,
          'userId',
          user?.id,
        );
        console.log(
          'scheduleUpdate socket roomId',
          room_id,
          scheduleStatus.roomId,
        );
        if (room_id === scheduleStatus.roomId) {
          setAllMessages(prev =>
            prev.map(record =>
              record.meet_up_schedule_id === scheduleStatus.id
                ? {...record, is_accepted: scheduleStatus.is_accepted}
                : record,
            ),
          );
          console.log('socket dispatch read');
        }
      };

      socket.on('message-update', messageUpdate);
      socket.on('schedule-update', scheduleUpdate);

      return () => {
        socket.off('message-update', messageUpdate);
        socket.off('schedule-update', scheduleUpdate);
      };
    }
  }, [userToken, socket, dispatch, room_id, user?.id]);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Chat',
      headerBackTitleVisible: false,
      headerTitleAlign: 'left',
      headerTitle: () => (
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('FriendProfile', {
              friend_id: friend_id,
              name: name,
              profilePic: profilePic,
            })
          }>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <FastImage
              source={{uri: profilePic}}
              style={{
                width: 30,
                height: 30,
                borderRadius: 30,
              }}
            />
            <Text
              style={{
                color: '#333333',
                marginLeft: 10,
                fontWeight: '700',
              }}>
              {name}
            </Text>
          </View>
        </TouchableOpacity>
      ),
      headerRight: () => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: 80,
            marginRight: 20,
          }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Report', {
                friend_id: friend_id,
                name: name,
              })
            }>
            <Octicons name="report" size={24} color="#333333" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('SuggestionResult', {room_id})}>
            <MaterialCommunityIcons
              name="calendar-account"
              size={24}
              color="#FF6347"
            />
          </TouchableOpacity>
        </View>
      ),
    });
  }, [navigation, name, profilePic, friend_id, room_id]);

  const sendMessage = useCallback(() => {
    Keyboard.dismiss();
    setInput('');
    if (input !== '') {
      dispatch(postChatroomMessage(room_id, input));
    }
  }, [dispatch, input, room_id]);

  const readMap = useCallback((location: pointLocation, placeName: string) => {
    const url = Platform.select({
      ios: 'maps:' + location.y + ',' + location.x + '?q=' + placeName,
      android: 'geo:' + location.y + ',' + location.x + '?q=' + placeName,
    });
    if (url) {
      Linking.openURL(url);
    }
  }, []);

  const replyScheduleClient = useCallback(
    (
      ans: boolean | null,
      scheduleID: number,
      roomId: string,
      place_name: string,
    ) => {
      console.log('reply');
      dispatch(replySchedule(ans, scheduleID, roomId, place_name));
    },
    [dispatch],
  );

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.backgroundGray}}>
      <KeyboardAvoidingView
        behavior={isAndroid ? 'height' : 'padding'}
        style={styles.container}
        keyboardVerticalOffset={isAndroid ? 120 : 90}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <>
            {allMessages && allMessages.length > 0 ? (
              <FlatList
                showsVerticalScrollIndicator={false}
                inverted={true}
                data={allMessages}
                keyExtractor={item =>
                  item.id.toString() + Math.random().toString()
                }
                contentContainerStyle={{
                  padding: SPACING,
                }}
                renderItem={({item}) => {
                  return (
                    <View>
                      <View
                        style={[
                          item.sender_id === user?.id
                            ? item.meet_up_schedule_id
                              ? styles.senderWithSchedule
                              : styles.sender
                            : item.meet_up_schedule_id
                            ? styles.receiverWithSchedule
                            : styles.receiver,
                        ]}>
                        <View
                          style={
                            item.sender_id === user?.id
                              ? styles.insideSender
                              : styles.insideReceiver
                          }>
                          {item.meet_up_schedule_id ? (
                            <View style={styles.scheduleBackground}>
                              <Text style={styles.cardtitle}>
                                誠邀你於
                                <Text style={styles.placeText}>
                                  {item.place_name}
                                </Text>
                                見面!
                              </Text>
                              <Text style={styles.regularText}>
                                {item.scheduled_at &&
                                  new Date(item.scheduled_at).toLocaleString()}
                              </Text>
                              <Text style={styles.cardDescription}>
                                {item.vicinity}
                              </Text>
                              <StarRating ratings={item.rating} />
                              <View style={styles.button}>
                                <TouchableOpacity
                                  onPress={() => {
                                    if (item.location && item.place_name) {
                                      readMap(item.location, item.place_name);
                                    }
                                  }}
                                  style={[
                                    styles.readMap,
                                    {
                                      borderColor: '#FF6347',
                                      borderWidth: 0.5,
                                    },
                                  ]}>
                                  <Text
                                    style={[
                                      styles.readMapText,
                                      {
                                        color: '#FF6347',
                                      },
                                    ]}>
                                    地圖🌍
                                  </Text>
                                </TouchableOpacity>
                              </View>
                              {item.is_accepted === null ? (
                                item.sender_id === user?.id ? null : (
                                  <View style={{flexDirection: 'row'}}>
                                    <View style={styles.button}>
                                      <TouchableOpacity
                                        onPress={() => {
                                          if (
                                            item.meet_up_schedule_id &&
                                            room_id &&
                                            item.place_name
                                          ) {
                                            replyScheduleClient(
                                              false,
                                              item.meet_up_schedule_id,
                                              room_id,
                                              item.place_name,
                                            );
                                          }
                                        }}
                                        style={[
                                          styles.readMap,
                                          {
                                            backgroundColor: 'transparent',
                                            borderColor: '#2Ba8E6',
                                            borderWidth: 0.5,
                                          },
                                        ]}>
                                        <Text
                                          style={[
                                            styles.readMapText,
                                            {
                                              color: 'crimson',
                                            },
                                          ]}>
                                          拒絕🥺
                                        </Text>
                                      </TouchableOpacity>
                                    </View>
                                    <View style={styles.button}>
                                      <TouchableOpacity
                                        onPress={() => {
                                          if (
                                            item.meet_up_schedule_id &&
                                            room_id &&
                                            item.place_name
                                          ) {
                                            replyScheduleClient(
                                              true,
                                              item.meet_up_schedule_id,
                                              room_id,
                                              item.place_name,
                                            );
                                          }
                                        }}
                                        style={[
                                          styles.readMap,
                                          {
                                            backgroundColor: 'transparent',
                                            borderColor: '#2Ba8E6',
                                            borderWidth: 0.5,
                                          },
                                        ]}>
                                        <Text
                                          style={[
                                            styles.readMapText,
                                            {
                                              color: 'blue',
                                            },
                                          ]}>
                                          應約😎
                                        </Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                )
                              ) : (
                                <View>
                                  <Text
                                    style={[
                                      styles.regularText,
                                      {textAlign: 'center'},
                                    ]}>
                                    狀態:{' '}
                                    {item.is_accepted ? '成行😎' : '不成行🥺'}
                                  </Text>
                                  {item.is_accepted &&
                                  item.sender_id !== user?.id ? (
                                    <View style={styles.button}>
                                      <TouchableOpacity
                                        onPress={() => {
                                          if (
                                            item.meet_up_schedule_id &&
                                            room_id &&
                                            item.place_name
                                          ) {
                                            // replyScheduleClient(
                                            //   null,
                                            //   item.meet_up_schedule_id,
                                            //   room_id,
                                            //   item.place_name,
                                            // );
                                            replyScheduleClient(
                                              false,
                                              item.meet_up_schedule_id,
                                              room_id,
                                              item.place_name,
                                            );
                                          }
                                        }}
                                        style={[
                                          styles.readMap,
                                          {
                                            backgroundColor: 'transparent',
                                            borderColor: '#FF6347',
                                            borderWidth: 0.5,
                                          },
                                        ]}>
                                        <Text>更改為拒絕</Text>
                                      </TouchableOpacity>
                                    </View>
                                  ) : null}
                                </View>
                              )}
                            </View>
                          ) : (
                            <Text
                              style={
                                item.sender_id === user?.id
                                  ? styles.senderText
                                  : styles.receiverText
                              }>
                              {item.message}
                            </Text>
                          )}
                          <Text
                            style={
                              item.sender_id === user?.id
                                ? styles.senderTime
                                : styles.receiverTime
                            }>
                            {formatTime(
                              Date.now(),
                              new Date(item.created_at).getTime(),
                            )}
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                }}
              />
            ) : (
              <View style={{flex: 1}} />
            )}
            <View style={styles.footer}>
              <TextInput
                returnKeyType="next"
                autoCorrect={false}
                multiline
                spellCheck={false}
                autoCapitalize="none"
                value={input}
                onChangeText={text => setInput(text)}
                onContentSizeChange={e =>
                  setMessageBarHeight(
                    Math.min(
                      Math.max(
                        input === '' ? 40 : messageBarHeight,
                        e.nativeEvent.contentSize.height,
                      ),
                      120,
                    ),
                  )
                }
                style={[
                  styles.textInput,
                  {
                    height: messageBarHeight,
                    borderWidth: isAndroid ? 0 : 5,
                  },
                ]}
              />
              <TouchableOpacity onPress={sendMessage} activeOpacity={0.5}>
                <Feather name="send" size={24} color="#2B68E6" />
              </TouchableOpacity>
            </View>
          </>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sender: {
    padding: 15,
    backgroundColor: 'rgba(200, 230, 240, 1)',
    alignSelf: 'flex-end',
    borderRadius: 20,
    marginHorizontal: 15,
    marginVertical: 5,
    maxWidth: '80%',
    position: 'relative',
  },
  insideSender: {
    backgroundColor: 'rgba(200, 230, 240, 1)',
    alignSelf: 'flex-end',
    borderRadius: 15,
    padding: 5,
  },
  receiver: {
    padding: 15,
    backgroundColor: 'antiquewhite',
    alignSelf: 'flex-start',
    borderRadius: 20,
    marginHorizontal: 15,
    marginVertical: 5,
    maxWidth: '80%',
    position: 'relative',
  },
  insideReceiver: {
    backgroundColor: 'antiquewhite',
    alignSelf: 'flex-start',
    borderRadius: 15,
    padding: 5,
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
  },
  textInput: {
    bottom: 0,
    flex: 1,
    marginRight: 15,
    backgroundColor: '#DCDCDC',
    paddingHorizontal: 20,
    color: '#4f4f4f',
    borderRadius: 20,
    paddingTop: 7,
    borderColor: 'transparent',
  },
  receiverText: {
    color: '#000',
    fontWeight: '500',
    marginBottom: 10,
  },
  receiverTime: {
    fontSize: 10,
    color: '#000',
  },
  senderText: {
    color: '#000',
    fontWeight: '500',
    marginBottom: 10,
  },
  senderTime: {
    right: 0,
    fontSize: 10,
    color: '#000',
    textAlign: 'right',
  },
  cardtitle: {
    fontSize: 18,
    color: '#333333',
    // marginTop: 5,
  },
  regularText: {
    fontSize: 14,
    marginTop: 5,
    textShadowColor: 'white',
    textShadowOffset: {width: 0.5, height: 0.5},
    textShadowRadius: 2,
  },
  cardDescription: {
    fontSize: 12,
    color: '#444',
    marginVertical: 5,
  },
  button: {
    alignItems: 'center',
    marginTop: 10,
  },
  readMap: {
    minWidth: '50%',
    paddingVertical: 5,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginBottom: 10,
  },
  readMapText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  placeText: {
    color: Colors.starOrange,
    // textShadowColor: '#e3e3e3',
    // textShadowOffset: {width: 0.5, height: 0.5},
    // textShadowRadius: 2,
    fontWeight: '900',
  },
  scheduleBackground: {
    borderRadius: 10,
    padding: 5,
  },
  senderWithSchedule: {
    padding: 7,
    backgroundColor: 'white',
    alignSelf: 'flex-end',
    borderRadius: 20,
    maxWidth: '80%',
    position: 'relative',
    marginHorizontal: 15,
    marginVertical: 5,
  },
  receiverWithSchedule: {
    padding: 7,
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    borderRadius: 20,
    maxWidth: '80%',
    position: 'relative',
    borderColor: 'red',
    marginHorizontal: 15,
    marginVertical: 5,
  },
});
