import {Alert} from 'react-native';
import {api} from '../../helpers/api';
import {matched} from '../match/action';
import {IRootState, IRootThunkDispatch} from '../store';
import {loadedUserProfile} from '../userProfile/action';
import {failed, loadCoinRecord, loaded, loading} from './action';

export function loadCoinRecordThunk() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      let token = getState().auth.token;
      let res = await api.get('/getCoinRecord', token);
      if (res.status !== 200) {
        throw await res.json();
      }
      let result = await res.json();
      console.log('coin record: ', result.data);
      dispatch(loaded());
      dispatch(loadCoinRecord(result.data));
    } catch (e) {
      dispatch(failed('get coin record failed' + e.msg + e.message));
      dispatch(loaded());
      Alert.alert('網絡連接失敗', '請重新檢查網絡', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
  };
}

export function rematch() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    // dispatch(loading());
    try {
      let userProfile = getState().userProfile.userProfile;
      let token = getState().auth.token;
      let res = await api.get('/pay-to-rematch', token);
      let result = await res.json();
      console.log('rematch result: ', result);
      if (
        result.msg &&
        result.msg === 'unexpected errorError: no matchUserDetail'
      ) {
        Alert.alert('抱歉', '附近沒有足夠用戶', [
          {text: '確認', style: 'default'},
        ]);
        return;
      }
      if (result.isSuccess === true && userProfile) {
        userProfile.info.coins = result.data.coinBalanceAfterUsed;
        console.log('result data: ', result.data.finalResult);
        console.log(
          'result data length: ',
          result.data.finalResult.otherUserDetail.length,
        );
        if (result.data.finalResult.otherUserDetail.length < 10) {
          dispatch(refund());
          dispatch(matched(result.data.finalResult));
          // dispatch(loadedUserProfile(userProfile!))
          return;
        }
        dispatch(matched(result.data.finalResult));
        dispatch(loadedUserProfile(userProfile!));
        return;
      }
      if (res.status !== 200) {
        throw await res.json();
      }
    } catch (e) {
      // dispatch(failed('rematch failed' + e.msg + e.message));
      // dispatch(loaded());
      Alert.alert('網絡連接失敗', '請重新檢查網絡', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
  };
}

export function refund() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    try {
      let token = getState().auth.token;
      let res = await api.get('/refund', token);
      if (res.status !== 200) {
        throw await res.json();
      }
      let result = await res.json();
      console.log('coin record: ', result.data);
      // dispatch(loaded())
    } catch (e) {
      // dispatch(failed('rematch failed' + e.msg + e.message));
      // dispatch(loaded());
      Alert.alert('退款失敗', '請聯絡客戶服務員', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
  };
}
