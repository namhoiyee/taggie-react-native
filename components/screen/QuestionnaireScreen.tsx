/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Alert, Button, TextInput, TouchableOpacity} from 'react-native';
import {submitQuestionnaireThunk} from '../../redux/questionnaire/thunk';
import {View, Text, FlatList, StyleSheet, Animated} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {ScrollView} from 'react-native-gesture-handler';
import {RadioButton} from 'react-native-paper';
import {DateTimePickerComponent} from '../utils/DateTimePicker';
import {isAndroid} from '../../utils/platform';
import {isDate, reverseTimeDifference} from '../../helpers/date';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {IRootState} from '../../redux/store';
import {Tag} from '../../helpers/types';
import {
  deleteUserTag,
  deleteWishTag,
  loadUserTags,
  loadUserWishTags,
} from '../../redux/tags/thunk';
import LoadingComponent from './Loading/LoadingComponent';
import {loaded} from '../../redux/questionnaire/action';

const circleWidth = 60;
const circleMargin = 10;

export default function QuestionnaireScreen({navigation}: any) {
  const loadingStatus = useSelector(
    (state: IRootState) => state.questionnaire.status,
  );
  const user = useSelector((state: IRootState) => state.auth.user);
  const topRef = useRef<FlatList | null>(null);
  const [userBirthday, setUserBirthday] = useState(new Date('2000-01-01'));

  console.log('user questionnaire', user);

  useEffect(() => {
    if (user && user.registration_status >= 2) {
      console.log('navigation push');
      navigation.push('EditProfile');
    }
  }, [navigation, user]);

  const [data, setData] = useState<{
    gender: '' | 'male' | 'female' | '其他';
    birthday: Date | null;
    smoke: '' | '常吸煙' | '少吸煙' | '唔吸煙';
    drink: '' | '常飲酒' | '少飲酒' | '唔飲酒';
    textDescription: string;
  }>({
    gender: '',
    birthday: null,
    smoke: '',
    drink: '',
    textDescription: '',
  });
  const [dataCheck, setDataCheck] = useState(new Array(7).fill(false));
  // const [currentProfilePic, setCurrentProfilePic] = useState<ImageUpload[]>();
  const [questionOpen, setQuestionOpen] = useState(0);
  // const [loading, setLoading] = useState(true);
  const questionIndex = [
    {
      id: 1,
      name: 'Q1',
    },
    {
      id: 2,
      name: 'Q2',
    },
    {
      id: 3,
      name: 'Q3',
    },
    {
      id: 4,
      name: 'Q4',
    },
    {
      id: 5,
      name: 'Q5',
    },
    {
      id: 6,
      name: 'Q6',
    },
    {
      id: 7,
      name: 'Q7',
    },
  ];
  const userTagsDetails = useSelector(
    (state: IRootState) => state.tags?.userTags,
  );
  const userWishTagsDetails = useSelector(
    (state: IRootState) => state.tags?.userWishTags,
  );
  const userTags = userTagsDetails?.data?.userTags;
  const userWishTags = userWishTagsDetails?.data?.wishTags;
  // const pan = useRef(new Animated.ValueXY()).current;
  // const list = useRef(new Animated.ValueXY()).current;
  const [show, setShow] = useState(isAndroid ? false : true);
  // const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadUserTags());
    dispatch(loadUserWishTags());
    dispatch(loaded());
  }, [dispatch]);

  useEffect(() => {
    if (userTags && userTags.length >= 4) {
      let newArray = dataCheck;
      newArray.splice(4, 1, true);
      setDataCheck(newArray);
    } else {
      let newArray = dataCheck;
      newArray.splice(4, 1, false);
      setDataCheck(newArray);
    }
  }, [dataCheck, userTags]);

  useEffect(() => {
    if (userWishTags && userWishTags.length >= 4) {
      let newArray = dataCheck;
      newArray.splice(5, 1, true);
      setDataCheck(newArray);
    } else {
      let newArray = dataCheck;
      newArray.splice(5, 1, false);
      setDataCheck(newArray);
    }
  }, [dataCheck, userWishTags]);

  const scrollToOffset = useCallback((index: number) => {
    // setCurrentQuestionIndex(index);
    topRef?.current?.scrollToOffset({
      offset: (index + 1) * (circleWidth + circleMargin * 2),
      animated: true,
    });
  }, []);

  const clickQ = useCallback((index: number) => {
    setQuestionOpen(index);
  }, []);

  const onChange = useCallback(
    (event: any, selectedDate: any) => {
      console.log('currentDate', isDate(selectedDate));
      const currentDate = isDate(selectedDate)
        ? selectedDate
        : new Date('2000-01-01');
      setShow(!isAndroid);
      setUserBirthday(selectedDate);
      setData(prev => ({
        ...prev,
        birthday: reverseTimeDifference(currentDate),
      }));
      let newArray = dataCheck;
      newArray.splice(1, 1, true);
      setDataCheck(newArray);
      setTimeout(() => {
        scrollToOffset(1);
        clickQ(2);
      }, 700);
    },
    [clickQ, dataCheck, scrollToOffset],
  );

  // console.log('userTags: ', userTags);

  const showMode = useCallback(
    (/* currentMode: any */) => {
      setShow(true);
      // setMode(currentMode);
    },
    [],
  );

  // useEffect(
  //   function () {
  //     const getData = async () => {
  //       // const resp = await fetch(URL);
  //       // const data = await resp.json();
  //       // setData(data);
  //       setLoading(false);
  //     };
  //     getData();

  //     Animated.timing(pan, {
  //       toValue: {x: -400, y: 0},
  //       delay: 700,
  //       useNativeDriver: false,
  //     }).start();
  //   },
  //   [pan],
  // );

  const genderOnChange = useCallback(
    (gender: 'male' | 'female' | '其他') => {
      setData({...data, gender: gender});
      let newArray = dataCheck;
      newArray.splice(0, 1, true);
      setDataCheck(newArray);
      setTimeout(() => {
        clickQ(1);
      }, 500);
    },
    [clickQ, data, dataCheck],
  );

  const smokeOnChange = useCallback(
    (smoke: '常吸煙' | '少吸煙' | '唔吸煙') => {
      setData({...data, smoke: smoke});
      let newArray = dataCheck;
      newArray.splice(2, 1, true);
      setDataCheck(newArray);
      setTimeout(() => {
        clickQ(3);
      }, 500);
    },
    [clickQ, data, dataCheck],
  );

  const drinkOnChange = useCallback(
    (drink: '常飲酒' | '少飲酒' | '唔飲酒') => {
      setData({...data, drink: drink});
      let newArray = dataCheck;
      newArray.splice(3, 1, true);
      setDataCheck(newArray);
      setTimeout(() => {
        clickQ(4);
      }, 500);
    },
    [clickQ, data, dataCheck],
  );

  const textDescriptionOnChange = useCallback(
    (textDescription: string) => {
      setData({...data, textDescription: textDescription});
      let newArray = dataCheck;
      newArray.splice(6, 1, true);
      setDataCheck(newArray);
    },
    [data, dataCheck],
  );

  const submitQuestionnaire = useCallback(() => {
    if (data.gender === '') {
      Alert.alert('提交錯誤', '請選擇性別', [{text: '確認', style: 'default'}]);
      return;
    }
    if (data.birthday === null) {
      Alert.alert('提交錯誤', '請選擇出生日期', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
    if (data.smoke === null) {
      Alert.alert('提交錯誤', '請選擇你的吸煙習慣', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
    if (data.drink === null) {
      Alert.alert('提交錯誤', '請選擇你的飲酒習慣', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
    if (userTags && userTags.length < 4) {
      Alert.alert('提交錯誤', '請選擇最少四個可以代表你的TAG', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
    if (userWishTags && userWishTags.length < 4) {
      Alert.alert('提交錯誤', '請選擇最少四個TAG黎代表你想遇到的人', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
    if (data.textDescription === '') {
      Alert.alert('提交錯誤', '請填寫你的自我介紹', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
    dispatch(submitQuestionnaireThunk(data));
  }, [data, dispatch, userTags, userWishTags]);

  // const submitFirstPic = useCallback(() => {
  //   // dispatch(submitFirstPicThunk())
  // }, []);

  // after got registration_status
  // let registration_status // from token
  // if (registration_status === 1){
  //   return <Questionnaire />
  // } else if (registration_status === 2){
  //   return (Edit Profile Pic Component)
  // } else if (registration_status === 3){
  //   return (normal page)
  // } else {
  //   return (loading)
  // }

  const editUserTag = useCallback(
    (item: Tag) => {
      console.log('element clicked', item);
      navigation.navigate('EditUserTag', {item});
    },
    [navigation],
  );

  const editWishTag = useCallback(
    (item: Tag) => {
      console.log('element clicked', item);
      navigation.navigate('EditWishTag', {item});
    },
    [navigation],
  );

  const deleteUserRow = useCallback(
    (rowKey: any) => {
      console.log('delete', rowKey);
      Alert.alert('確認刪除?', '將刪除Tag,刪除後可重新增加', [
        {text: '取消', style: 'default'},
        {
          text: '確認',
          style: 'destructive',
          onPress: () => dispatch(deleteUserTag(rowKey)),
        },
      ]);
    },
    [dispatch],
  );

  const deleteWishRow = useCallback(
    (rowKey: any) => {
      console.log('delete', rowKey);
      Alert.alert('確認刪除?', '將刪除Tag,刪除後可重新增加', [
        {text: '取消', style: 'default'},
        {
          text: '確認',
          style: 'destructive',
          onPress: () => dispatch(deleteWishTag(rowKey)),
        },
      ]);
    },
    [dispatch],
  );

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <LinearGradient
      colors={['#009387', '#00939F', '#009387']}
      style={styles.gradient}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>介紹下自己</Text>
      </View>
      <FlatList
        ref={topRef}
        horizontal
        style={styles.proContainer}
        showsHorizontalScrollIndicator={false}
        data={[...questionIndex, {id: -1, name: ''}]}
        renderItem={({item, index}) => {
          return (
            <Animated.View style={styles.card}>
              {index !== questionIndex.length ? (
                <TouchableOpacity
                  onPress={() => {
                    clickQ(index);
                  }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <LinearGradient
                      colors={
                        dataCheck[index] === false
                          ? ['#ffba00', '#ffdd80', '#ffba00']
                          : ['#88CC00', '#80d280', '#88CC00']
                      }
                      style={styles.circle}>
                      <Text
                        style={{
                          color: questionOpen === index ? 'red' : 'black',
                          fontSize: 20,
                        }}>
                        {item.name}
                      </Text>
                    </LinearGradient>
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={submitQuestionnaire}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <LinearGradient
                      colors={['#ffba00', '#ffdd80', '#ffba00']}
                      style={styles.circle}>
                      <Text style={{color: 'black', fontSize: 20}}>提交</Text>
                    </LinearGradient>
                  </View>
                </TouchableOpacity>
              )}
            </Animated.View>
          );
        }}
      />

      <View style={styles.ops}>
        <ScrollView>
          <View>
            {questionOpen === 0 ? (
              <View style={styles.container}>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.question}>性別</Text>
                  <RadioButton.Group
                    onValueChange={value => {
                      scrollToOffset(0);
                      genderOnChange(value as any);
                    }}
                    value={data.gender}>
                    {/* <View style={{display: "flex", flexDirection: "row", alignItems: "center", marginTop: 20}}>
                            <RadioButton value="female" />
                            <Text style={{fontSize: 20}}>女</Text>
                          </View>
                          <View style={{display: "flex", flexDirection: "row", alignItems: "center", marginTop: 10}}>
                            <RadioButton value="male" />
                            <Text style={{fontSize: 20}}>男</Text>
                          </View>
                          <View style={{display: "flex", flexDirection: "row", alignItems: "center", marginTop: 10}}>
                            <RadioButton value="其他" />
                            <Text style={{fontSize: 20}}>其他</Text>
                          </View> */}
                    <RadioButton.Item label="女" value="female" />
                    <RadioButton.Item label="男" value="male" />
                    <RadioButton.Item label="其他" value="其他" />
                  </RadioButton.Group>
                </View>
              </View>
            ) : null}
            {questionOpen === 1 ? (
              <View style={styles.container}>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.question}>出生日期</Text>
                  <View
                    style={{
                      marginTop: 30,
                      marginLeft: 10,
                      transform: [{translateY: -5}],
                    }}>
                    <DateTimePickerComponent
                      date={userBirthday}
                      show={show}
                      onChange={onChange}
                      showMode={showMode}
                    />
                  </View>
                </View>
              </View>
            ) : null}
            {questionOpen === 2 ? (
              <View style={styles.container}>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.question}>吸煙習慣</Text>
                  <RadioButton.Group
                    onValueChange={value => {
                      scrollToOffset(2);
                      smokeOnChange(value as any);
                    }}
                    value={data.smoke}>
                    <RadioButton.Item label="常吸煙" value="常吸煙" />
                    <RadioButton.Item label="少吸煙" value="少吸煙" />
                    <RadioButton.Item label="唔吸煙" value="唔吸煙" />
                  </RadioButton.Group>
                </View>
              </View>
            ) : null}
            {questionOpen === 3 ? (
              <View style={styles.container}>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.question}>飲酒習慣</Text>
                  <RadioButton.Group
                    onValueChange={value => {
                      scrollToOffset(3);
                      drinkOnChange(value as any);
                    }}
                    value={data.drink}>
                    <RadioButton.Item label="常飲酒" value="常飲酒" />
                    <RadioButton.Item label="少飲酒" value="少飲酒" />
                    <RadioButton.Item label="唔飲酒" value="唔飲酒" />
                  </RadioButton.Group>
                </View>
              </View>
            ) : null}
            {questionOpen === 4 ? (
              <View style={styles.container}>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.question}>
                    揀4個或以上可以代表自己既TAG
                  </Text>
                  {userTags?.length !== 0 && (
                    <View style={styles.tagsContainer}>
                      {userTags?.map((item, index) => (
                        <View key={'userTags' + index}>
                          {item.tag.includes('吸煙') ||
                          item.tag.includes('飲酒') ? null : (
                            <TouchableOpacity
                              onPress={() =>
                                editUserTag({
                                  id: item.id!,
                                  class: item.class!,
                                  tag: item.tag!,
                                  classId: item.classId!,
                                  dbPrimaryId: item.dbPrimaryId!,
                                })
                              }
                              onLongPress={() => deleteUserRow(item)}>
                              <View style={styles.tag}>
                                <Text style={styles.text}>{item.tag}</Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        </View>
                      ))}
                    </View>
                  )}
                  {userTags && userTags.length >= 14 ? null : (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('EditUserTag', {
                          item: {
                            id: -1,
                            class: '',
                            tag: '',
                            classId: -1,
                            dbPrimaryId: -1,
                          },
                        });
                      }}>
                      <AntDesign
                        style={{marginTop: 20}}
                        name="pluscircle"
                        size={30}
                      />
                    </TouchableOpacity>
                  )}
                  <View
                    style={{
                      display: 'flex',
                      alignItems: 'flex-end',
                      marginTop: 20,
                      marginBottom: 40,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        if (userTags && userTags.length < 4) {
                          Alert.alert(
                            '提交錯誤',
                            '請選擇最少四個可以代表你的TAG',
                            [{text: '確認', style: 'default'}],
                          );
                        } else {
                          scrollToOffset(4);
                          clickQ(5);
                        }
                      }}>
                      {userTags && userTags.length >= 4 ? (
                        <View
                          style={{
                            backgroundColor: 'grey',
                            paddingHorizontal: 10,
                            paddingVertical: 5,
                            borderRadius: 20,
                            alignItems: 'center',
                          }}>
                          <Text style={{fontSize: 18, color: 'white'}}>
                            {'繼續  '}
                            <FontAwesome5
                              name="arrow-alt-circle-right"
                              size={20}
                            />
                          </Text>
                        </View>
                      ) : null}
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : null}
            {questionOpen === 5 ? (
              <View style={styles.container}>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.question}>
                    揀4個或以上既TAG代表你想遇到既人
                  </Text>
                  {userTags?.length !== 0 ? (
                    <View style={styles.tagsContainer}>
                      {userWishTags?.map((item, index) => (
                        <View key={'userWishTags' + index}>
                          <TouchableOpacity
                            onPress={() =>
                              editWishTag({
                                id: item.id!,
                                class: item.class!,
                                tag: item.tag!,
                                classId: item.classId!,
                                dbPrimaryId: item.dbPrimaryId!,
                              })
                            }
                            onLongPress={() => deleteWishRow(item)}>
                            <View style={styles.tag}>
                              <Text style={styles.text}>{item.tag}</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      ))}
                    </View>
                  ) : null}
                  {userWishTags && userWishTags.length >= 16 ? null : (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('EditWishTag', {
                          item: {
                            id: -1,
                            class: '',
                            tag: '',
                            classId: -1,
                            dbPrimaryId: -1,
                          },
                        });
                      }}>
                      <AntDesign
                        style={{marginTop: 20}}
                        name="pluscircle"
                        size={30}
                      />
                    </TouchableOpacity>
                  )}
                  <View
                    style={{
                      display: 'flex',
                      alignItems: 'flex-end',
                      marginTop: 20,
                      marginBottom: 40,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        if (userWishTags && userWishTags.length < 4) {
                          Alert.alert(
                            '提交錯誤',
                            '請選擇最少四個TAG黎代表你想遇到的人',
                            [{text: '確認', style: 'default'}],
                          );
                        } else {
                          scrollToOffset(5);
                          clickQ(6);
                        }
                      }}>
                      {userWishTags && userWishTags.length >= 4 ? (
                        <View
                          style={{
                            backgroundColor: 'grey',
                            paddingHorizontal: 10,
                            paddingVertical: 5,
                            borderRadius: 20,
                            alignItems: 'center',
                          }}>
                          <Text style={{fontSize: 18, color: 'white'}}>
                            {'繼續  '}
                            <FontAwesome5
                              name="arrow-alt-circle-right"
                              size={20}
                            />
                          </Text>
                        </View>
                      ) : null}
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : null}
            {questionOpen === 6 ? (
              <View>
                <View style={styles.container}>
                  <View style={{marginLeft: 10}}>
                    <Text style={styles.question}>用文字介紹下自己</Text>
                    <View style={styles.textAreaContainer}>
                      <TextInput
                        style={styles.textArea}
                        placeholder="自我介紹"
                        placeholderTextColor="grey"
                        onChangeText={value => textDescriptionOnChange(value)}
                        multiline={true}
                        autoCorrect={false}
                        autoCapitalize="none"
                        value={data.textDescription}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.btnContainer}>
                  <Button
                    title="提交"
                    onPress={() => {
                      scrollToOffset(5);
                      submitQuestionnaire();
                    }}
                  />
                </View>
              </View>
            ) : null}
          </View>
        </ScrollView>
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  card: {
    // marginLeft: 400,
    flexDirection: 'row',
  },
  gradient: {
    height: '100%',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    paddingHorizontal: 20,
    paddingTop: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    color: '#FFF',
    flex: 1,
    fontSize: 24,
  },
  proContainer: {
    marginRight: -20,
    alignSelf: 'center',
  },
  ops: {
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    height: 500,
    backgroundColor: '#FFF',
    marginHorizontal: -20,
  },
  col: {
    flexDirection: 'row',
    marginTop: 25,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  day: {
    color: '#000119',
    flex: 1,
    fontSize: 20,
  },
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginTop: 30,
  },
  circle: {
    height: 60,
    width: circleWidth,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    // marginRight:20,
    marginHorizontal: circleMargin,
    padding: 5,
  },
  gradientStyle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
  },
  count: {
    color: '#fff',
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  text: {
    color: 'rgb(255,255,255)',
    fontSize: 20,
    textAlign: 'center',
  },
  duration: {
    color: '#000119',
    fontSize: 12,
    flex: 1,
    marginLeft: 280,
    position: 'absolute',
  },
  question: {
    flex: 1,
    minWidth: 150,
    color: '#000119',
    fontSize: 30,
    textShadowColor: 'grey',
    textShadowOffset: {height: 1, width: 1},
    textShadowRadius: 2,
  },
  avatarStyle: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  textAreaContainer: {
    borderWidth: 1,
    padding: 5,
    borderRadius: 10,
    margin: 12,
    width: 300,
    height: 100,
  },
  textArea: {
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
    color: 'black',
  },
  btnContainer: {
    backgroundColor: 'white',
    marginTop: 30,
    marginLeft: 150,
    marginRight: 150,
  },
  tag: {
    backgroundColor: '#FF5740',
    borderRadius: 20,
    padding: 10,
    alignSelf: 'stretch',
    marginRight: 10,
    marginVertical: 10,
    justifyContent: 'center',
    alignContent: 'center',
  },
  tagsContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
