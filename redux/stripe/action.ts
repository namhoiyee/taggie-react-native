export function paymentSucceed(totalCoins: number) {
  return {
    type: '@@Stripe/SuccessPayment' as const,
    totalCoins,
  };
}

export function failed(msg: string) {
  console.log('failed to pay: ', msg);
  return {
    type: '@@Stripe/failed' as const,
    msg,
  };
}
export function cancel() {
  return {
    type: '@@Stripe/cancel' as const,
  };
}

export function resetStripe() {
  return {
    type: '@@Stripe/reset' as const,
  };
}

export function loading() {
  return {
    type: '@@Stripe/loading' as const,
  };
}

export type StripeAction =
  | ReturnType<typeof paymentSucceed>
  | ReturnType<typeof failed>
  | ReturnType<typeof loading>
  | ReturnType<typeof cancel>
  | ReturnType<typeof resetStripe>;
