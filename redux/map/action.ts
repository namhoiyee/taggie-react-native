import {MarkerInfo, Region} from '../../helpers/types';
import {WhoLikeUInfo} from './thunk';

export function putMarker(userMarker: MarkerInfo[], region: Region) {
  return {
    type: '@@Map/putMarker' as const,
    userMarker,
    region,
  };
}

export function whoLikeYouSocket(userInfo: WhoLikeUInfo) {
  return {
    type: '@@Map/whoLikeYouSocket' as const,
    userInfo,
  };
}
export function failed(msg: string) {
  console.log('map failed', msg);
  return {
    type: '@@Map/failed' as const,
    msg,
  };
}

export function loading() {
  return {
    type: '@@Map/loading' as const,
  };
}
// export function insertedGeolocation(time: Date) {
//   return {
//     type: '@@Match/inserted' as const,
//     time,
//   };
// }

export type MapAction =
  | ReturnType<typeof putMarker>
  | ReturnType<typeof failed>
  | ReturnType<typeof loading>
  | ReturnType<typeof whoLikeYouSocket>;
