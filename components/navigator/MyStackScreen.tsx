import React, {useEffect} from 'react';
import {useTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {loadSocket} from '../../redux/socket/thunk';
import {getUnreadCount, updateUnreadCount} from '../../redux/chatroom/thunk';
import {crushHappen} from '../../redux/crush/thunk';
import {CrushHappenedFriendAndUserInfo} from '../../redux/crush/state';
import {logout} from '../../redux/auth/thunk';
import MyTabsScreen from '../navigator/MyTabsScreen';
import ChatScreen from '../screen/ChatScreen';
import EditWishTagsScreen from '../screen/EditWishTagsScreen';
import EditProfileScreen from '../screen/EditProfileScreen';
import EditGalleryScreen from '../screen/EditGalleryScreen';
import EditUserTagsScreen from '../screen/EditUserTagsScreen';
import ReportScreen from '../screen/ReportScreen';
import ScheduleDatingScreen from '../screen/Schedule&Suggestion/ScheduleDatingScreen';
import QuestionnaireScreen from '../screen/QuestionnaireScreen';
import {
  loadUserProfile,
  loadUserProfilePic,
} from '../../redux/userProfile/thunk';
import SuggestionResultScreen from '../screen/Schedule&Suggestion/SuggestionResultScreen';
import CoinHistoryScreen from '../screen/CoinHistoryScreen';
import SchedulingScreen from '../screen/Schedule&Suggestion/SchedulingScreen';
import StripeGatewayScreen from '../screen/StripeGatewayScreen';
import FriendProfileScreen from '../screen/FriendProfileScreen';
import {WhoLikeUInfo} from '../../redux/map/thunk';
import {whoLikeYouSocket} from '../../redux/map/action';
import {Alert} from 'react-native';

const Stack = createStackNavigator();

export default () => {
  const user = useSelector((state: IRootState) => state.auth.user);
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const userId = useSelector((state: IRootState) => state.auth.user?.id);
  const userToken = useSelector((state: IRootState) => state.auth.token);
  const socket = useSelector((state: IRootState) => state.socket.socket);
  
  useEffect(() => {
    if (user?.registration_status && user?.registration_status >= 2) {
      dispatch(loadUserProfile());
      dispatch(loadUserProfilePic());
    }
  }, [dispatch, user?.registration_status]);

  useEffect(() => {
    if (userToken && user?.registration_status === 3) {
      dispatch(loadSocket());
    }
  }, [dispatch, userToken, user?.registration_status]);

  useEffect(() => {
    if (userToken && socket && user?.registration_status === 3) {
      const connect = () => {
        console.log('connect socket io', socket.id);
        dispatch(getUnreadCount());
      };
      socket.on('connect', connect);

      const disconnect = () => {
        console.log('connect socket io', socket.id);
        // dispatch(disconnect());
        dispatch(logout());
      };
      socket.on('disconnect', disconnect);

      const updateCount = (data: {count: number; sender_id: number}) => {
        if (data.sender_id !== userId) {
          dispatch(updateUnreadCount(data.count));
        }
        console.log('unread count socket', data.count, 'userId', userId);
      };

      const crushHappenHandle = (data: CrushHappenedFriendAndUserInfo) => {
        const userPresent =
          data.firstMatchUser.userId === user.id
            ? data.lateMatchUser
            : data.firstMatchUser;
        console.log('received crush');
        dispatch(crushHappen(userPresent));
      };

      const whoLikeYou = (res: WhoLikeUInfo) => {
        console.log('whoLikeYou');
        dispatch(whoLikeYouSocket(res));
      };
      const blockAndLogout = () => {
        console.log('blocked');
        Alert.alert('你的帳戶有異常', '請聯絡客服: taggie1taggie@gmail.com', [
          {text: '確定', onPress: () => dispatch(logout())},
        ]);
      };

      const onEvent = (rooms: string[]) => {
        console.log(rooms);
        rooms?.forEach(room => {
          console.log('joined room', room);
          socket.emit('join', room);
        });
      };
      socket.on('room-to-join', onEvent);
      socket.on('match', crushHappenHandle);
      socket.on('unread-update', updateCount);
      socket.on('whoLikeYou', whoLikeYou);
      socket.on('Blocked by admin', blockAndLogout);
      return () => {
        socket.off('connect', connect);
        socket.off('unread-update', updateCount);
        socket.off('match', crushHappenHandle);
        socket.off('disconnect', disconnect);
        socket.off('whoLikeYou', whoLikeYou);
        socket.off('Blocked by admin', blockAndLogout);
        socket.off('room-to-join', onEvent);
      };
    }
  }, [
    userToken,
    socket,
    dispatch,
    userId,
    user?.registration_status,
    user?.id,
  ]);

  console.log('user', user?.registration_status);
  return (
    <Stack.Navigator
      initialRouteName={
        user?.registration_status === 1
          ? 'Questionnaire'
          : user?.registration_status === 2
          ? 'EditProfile'
          : 'MyTabs'
      }
      screenOptions={{
        headerBackTitleVisible: false,
        headerStyle: {
          backgroundColor: '#efefef',
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
        headerTitleStyle: {
          fontWeight: 'bold',
          color: '#333333',
        },
      }}>
      <Stack.Screen
        name="EditProfile"
        options={{
          title: '編輯個人資料',
        }}
        component={EditProfileScreen}
      />
      <Stack.Screen
        name="EditGallery"
        options={{
          title: '編輯相簿',
          headerShown: false,
        }}
        component={EditGalleryScreen}
      />
      <Stack.Screen
        name="SuggestionResult"
        options={{
          headerTitle: '',
          headerShown: false,
        }}
        component={SuggestionResultScreen}
      />
      <Stack.Screen name="Scheduling" component={SchedulingScreen} />
      <Stack.Screen
        name="StripeGatewayScreen"
        options={{
          headerBackTitleVisible: false,
          title: '付款',
        }}
        component={StripeGatewayScreen}
      />
      <Stack.Screen
        name="EditWishTag"
        options={{
          headerTitle: '',
        }}
        component={EditWishTagsScreen}
      />
      <Stack.Screen
        name="EditUserTag"
        options={{
          headerTitle: '',
        }}
        component={EditUserTagsScreen}
      />
      <Stack.Screen
        name="ChatroomDetail"
        options={{
          title: 'Chatroom',
        }}
        component={ChatScreen}
      />
      <Stack.Screen
        name="ScheduleDating"
        options={{
          headerBackTitleVisible: false,
          title: '你的日程',
        }}
        component={ScheduleDatingScreen}
      />
      <Stack.Screen
        name="Report"
        options={{
          title: '報告',
        }}
        component={ReportScreen}
      />
      <Stack.Screen
        name="Questionnaire"
        options={{
          title: '問卷',
        }}
        component={QuestionnaireScreen}
      />
      <Stack.Screen
        name="CoinHistory"
        options={{
          title: '交易記錄',
        }}
        component={CoinHistoryScreen}
      />
      <Stack.Screen
        name="MyTabs"
        component={MyTabsScreen}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <Stack.Screen
        name="FriendProfile"
        component={FriendProfileScreen}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
    </Stack.Navigator>
  );
};
