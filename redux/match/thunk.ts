import {api} from '../../helpers/api';
import {
  located,
  failed,
  matched,
  gotUserPermission,
  insertedGeolocation,
  likedPeople,
  unlikedPeople,
  updatedMatchResult,
  checkedIfAfter15MinsFromLastMatch,
} from './action';
import {IRootState, IRootThunkDispatch} from '../store';
import Geolocation from 'react-native-geolocation-service';
import {PermissionsAndroid, Alert} from 'react-native';
import {isAndroid} from '../../utils/platform';
import {logout} from '../auth/thunk';
import {ILocation} from '../../helpers/types';
import {loaded, loading} from '../loading/action';

export function locateUser() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    let token = getState().auth.token;
    dispatch(loading());
    Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        console.log('location', latitude, longitude);
        dispatch(
          located({
            latitude: latitude,
            //  - (isAndroid ? 37.4219812 - 22.2864231 : 15.4997965),
            longitude: longitude,
            // - (isAndroid ? -122.0839266 - 114.1481361 : -236.5551125),
          }),
        );
        if (token) {
          dispatch(
            insertGeolocation(token, {
              latitude: latitude,
              longitude: longitude,
            }),
          );
        }
      },
      error => {
        console.log('failed get location', error.code, error.message);
        dispatch(failed('讀取地理位置失敗'));
        dispatch(loaded());
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  };
}

export function getUserPermission() {
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    if (!isAndroid) {
      Geolocation.requestAuthorization('always').then(async res => {
        if (res === 'granted') {
          dispatch(gotUserPermission(true));
        } else {
          await dispatch(logout());
          Alert.alert(
            '抱歉',
            '此App需要使用地理位置為用戶配對。請於設定中為本App設定地理位置權限，謝謝使用。正在為你登出...',
            [{text: '確認', style: 'default'}],
          );
        }
      });
    } else {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ]).then(async granted => {
        console.log(granted);
        if (
          granted['android.permission.ACCESS_COARSE_LOCATION'] ===
            PermissionsAndroid.RESULTS.GRANTED &&
          granted['android.permission.ACCESS_FINE_LOCATION'] ===
            PermissionsAndroid.RESULTS.GRANTED
        ) {
          dispatch(gotUserPermission(true));
        } else {
          await dispatch(logout());
          Alert.alert(
            '抱歉',
            '此App需要使用地理位置為用戶配對。請於設定中為本App設定地理位置權限，謝謝使用。正在為你登出...',
            [{text: '確認', style: 'default'}],
          );
        }
      });
    }
  };
}

export function insertGeolocation(userToken: string, userLocation: ILocation) {
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try {
      const res = await api.post(
        '/new-location',
        {
          latitude: userLocation.latitude,
          longitude: userLocation.longitude,
        },
        userToken,
      );

      const message = await res.json();
      // console.log(message)
      if (res.status === 201) {
        dispatch(insertedGeolocation(message.data.created_at));
        dispatch(checkIfAfter15MinsFromLastMatch(userToken));
        // console.log('insert location', res.status, message.data.created_at);
      } else {
        dispatch(failed('插入地理位置失敗'));
        console.log('insert location failed', message);
        dispatch(loaded());
      }
    } catch (e) {
      dispatch(failed('插入地理位置失敗'));
      console.log('insert location failed at first', e);
      dispatch(loaded());
    }
  };
}

export function checkIfAfter15MinsFromLastMatch(userToken: string) {
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try {
      console.log('check need match!');
      const res = await api.get('/check-last-match-time', userToken);

      const result = await res.json();
      // console.log(
      //   'matching result',
      //   result.data.finalResultWithLocation.otherUserDetail,
      // );
      if (res.status === 200) {
        console.log('checkedIfAfter15MinsFromLastMatch', result?.data?.rematch);
        dispatch(checkedIfAfter15MinsFromLastMatch(result?.data?.rematch));
        if (result?.data?.rematch) {
          dispatch(initMatching(userToken));
          return;
        }
        if (!result?.data?.rematch && result?.data?.rematch !== undefined) {
          dispatch(updateCurrentMatching(userToken));
          return;
        }
      } else {
        dispatch(failed('checkedIfAfter15MinsFromLastMatch'));
        console.log('checkedIfAfter15MinsFromLastMatch failed', result);
        dispatch(loaded());
      }
    } catch (e) {
      dispatch(failed('checkedIfAfter15MinsFromLastMatch'));
      console.log('checkedIfAfter15MinsFromLastMatch failed at first', e);
      dispatch(loaded());
    }
  };
}

export function updateCurrentMatching(userToken: string) {
  console.log('update current matching');
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try {
      const res = await api.get('/buddies-status', userToken);

      const result = await res.json();
      // console.log(
      //   'matching result',
      //   result.data.finalResultWithLocation.otherUserDetail,
      // );
      if (res.status === 200) {
        // console.log(
        //   'updateCurrentMatching',
        //   result?.data?.userLastTimeMatchResult,
        // );
        dispatch(updatedMatchResult(result?.data?.userLastTimeMatchResult));
        // console.log(
        //   'matched',
        //   result/* .otherUserDetail.map(result => result.user_tags) */,
        // );
        dispatch(loaded());
      } else {
        dispatch(failed('重新讀取配對時失敗'));
        dispatch(loaded());
        console.log('read old match failed', result);
      }
    } catch (e) {
      dispatch(failed('重新讀取配對時失敗'));
      dispatch(loaded());
      console.log('read old match failed', e);
    }
  };
}

export function initMatching(userToken: string) {
  console.log('update initMatching');
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try {
      const res = await api.get('/buddies', userToken);

      const result = await res.json();
      // console.log(
      //   'matching result',
      //   result.data.finalResultWithLocation.otherUserDetail,
      // );
      if (res.status === 200) {
        // console.log('initMatching', result?.data?.finalResultWithLocation);
        dispatch(matched(result?.data?.finalResultWithLocation));
        // console.log(
        //   'matched',
        //   result/* .otherUserDetail.map(result => result.user_tags) */,
        // );
        dispatch(loaded());
      } else {
        dispatch(failed('配對失敗'));
        dispatch(loaded());
        console.log('matching failed', result);
      }
    } catch (e) {
      dispatch(failed('配對失敗'));
      dispatch(loaded());
      console.log('matching failed at first', e);
    }
  };
}

export function likeOrUnlikePeople(
  userToken: string,
  likedUserId: number,
  likeRes: 'like' | 'unlike',
) {
  return async (dispatch: IRootThunkDispatch) => {
    // console.log('initMatching');
    dispatch(loading());
    try {
      const res = await api.post(
        '/like-match',
        {
          likedUserId,
          likeRes,
        },
        userToken,
      );

      if (res.status !== 201) {
        throw await res.json();
      }
      const result = await res.json();
      console.log(result);
      if (likeRes === 'like') {
        dispatch(likedPeople(result.data));
        dispatch(loaded());
      } else {
        dispatch(unlikedPeople(result.data));
        dispatch(loaded());
      }
      // console.log('insert location', res.status, message.data.created_at);
    } catch (e) {
      dispatch(failed('failed to like or unlike ' + e.msg + e.message));
      dispatch(loaded());
      // console.log('insert location failed at first', e);
    }
  };
}
