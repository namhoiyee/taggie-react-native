import {JWTPayload} from '../../helpers/types';

export type AuthState =
  | {
      status: 'loading';
      msg?: string;
      user?: JWTPayload;
      token?: string;
    }
  | {
      status: 'error';
      msg: string;
      user?: JWTPayload;
      token?: string;
    }
  | {
      status: 'logout';
      msg?: string;
      user: undefined;
      token: undefined;
    }
  | {
      status: 'updatedToken';
      msg?: string;
      user: JWTPayload;
      token: string;
    }
  | {
      status: 'ready';
      msg?: string;
      user: JWTPayload;
      token: string;
    }
  | {
      status: 'confirmedToBeRegStatThree';
      msg?: string;
      user?: JWTPayload;
      token?: string;
    }
  | {
      status: 'signUp';
      msg?: string;
      user: JWTPayload;
      token: string;
    };

export const initialState: AuthState = {
  status: 'loading',
};
