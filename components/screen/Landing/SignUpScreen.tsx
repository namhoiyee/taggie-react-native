/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet,
  ScrollView,
  StatusBar,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import {useDispatch, useSelector} from 'react-redux';
import {signUp} from '../../../redux/auth/thunk';
import {IRootState} from '../../../redux/store';
import LoadingComponent from '../Loading/LoadingComponent';

const SignInScreen = ({navigation}: any) => {
  const [data, setData] = React.useState<{
    username: string;
    email: string;
    password: string;
    confirm_password: string;
    check_textInputChange: boolean;
    check_emailEntry: null | boolean;
    secureTextEntry: null | boolean;
    confirm_secureTextEntry: boolean;
    isValidUsername: boolean;
    isValidEmail: boolean;
    isValidPassword: boolean;
    isValidPasswordConfirmation: boolean;
  }>({
    username: '',
    email: '',
    password: '',
    confirm_password: '',
    check_textInputChange: false,
    check_emailEntry: null,
    secureTextEntry: null,
    confirm_secureTextEntry: true,
    isValidUsername: true,
    isValidEmail: true,
    isValidPassword: true,
    isValidPasswordConfirmation: true,
  });
  const authLoadingStatus = useSelector(
    (state: IRootState) => state.auth.status,
  );

  const dispatch = useDispatch();

  const textInputChange = (val: any) => {
    if (val.length !== 0) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true,
        isValidUsername: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
        isValidUsername: false,
      });
    }
  };

  const handleEmailChange = (val: any) => {
    if (val.length === 0) {
      setData({
        ...data,
        email: val,
        check_emailEntry: null,
        isValidEmail: false,
      });
      return;
    }
    if (val.includes('@') && val[val.length - 1] !== '@') {
      console.log('Email is Correct');
      setData({
        ...data,
        email: val,
        check_emailEntry: true,
        isValidEmail: true,
      });
      return;
    } else {
      console.log('Email is Not Correct');
      setData({
        ...data,
        email: val,
        check_emailEntry: false,
        isValidEmail: true,
      });
    }
  };

  const handlePasswordChange = (val: any) => {
    if (val.length === 0) {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    }
  };

  const handleConfirmPasswordChange = (val: any) => {
    if (val === data.password) {
      setData({
        ...data,
        confirm_password: val,
        isValidPasswordConfirmation: true,
      });
    } else {
      setData({
        ...data,
        confirm_password: val,
        isValidPasswordConfirmation: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const updateConfirmSecureTextEntry = () => {
    setData({
      ...data,
      confirm_secureTextEntry: !data.confirm_secureTextEntry,
    });
  };

  function clickSignUp() {
    if (data.username === '') {
      setData({
        ...data,
        isValidUsername: false,
      });
      if (data.email === '') {
        setData({
          ...data,
          isValidUsername: false,
          isValidEmail: false,
        });
        if (data.password === '') {
          setData({
            ...data,
            isValidUsername: false,
            isValidEmail: false,
            isValidPassword: false,
          });
          if (data.password !== data.confirm_password) {
            setData({
              ...data,
              isValidUsername: false,
              isValidEmail: false,
              isValidPassword: false,
              isValidPasswordConfirmation: false,
            });
          }
        }
      }
      return;
    }
    if (data.email === '') {
      setData({
        ...data,
        isValidEmail: false,
      });
      if (data.password === '') {
        setData({
          ...data,
          isValidPassword: false,
          isValidEmail: false,
        });
        if (data.password !== data.confirm_password) {
          setData({
            ...data,
            isValidPassword: false,
            isValidEmail: false,
            isValidPasswordConfirmation: false,
          });
        }
      }
      return;
    }
    if (data.password === '') {
      setData({
        ...data,
        isValidPassword: false,
      });
      if (data.password !== data.confirm_password) {
        setData({
          ...data,
          isValidPassword: false,
          isValidPasswordConfirmation: false,
        });
      }
      return;
    }
    if (data.password !== data.confirm_password) {
      setData({
        ...data,
        isValidPasswordConfirmation: false,
      });
      return;
    }
    if (data.isValidUsername === false) {
      return;
    }
    if (data.isValidEmail === false) {
      return;
    }
    if (data.isValidPassword === false) {
      return;
    }
    if (data.isValidPasswordConfirmation === false) {
      return;
    }
    if (data.check_textInputChange !== true) {
      return;
    }
    if (data.check_emailEntry !== true) {
      return;
    }
    dispatch(
      signUp({
        username: data.username,
        email: data.email,
        password: data.password,
        lang: 'zh',
      }),
    );
  }

  if (authLoadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>立即註冊💪!</Text>
      </View>
      <View /* animation="fadeInUpBig" */ style={styles.footer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.text_footer}>用戶名稱</Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#05375a" size={20} />
            <TextInput
              returnKeyType="done"
              autoCorrect={false}
              placeholder="你的用戶名稱"
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={val => textInputChange(val)}
            />
            {data.check_textInputChange ? (
              <View /* animation="bounceIn" */>
                <Feather name="check-circle" color="green" size={20} />
              </View>
            ) : (
              !data.check_textInputChange &&
              data.username !== '' && (
                <View /* animation="bounceIn" */>
                  <Entypo name="circle-with-cross" color="red" size={20} />
                </View>
              )
            )}
          </View>
          {data.isValidUsername ? null : (
            <View /* animation="fadeInLeft"  duration={500} */>
              <Text style={styles.errorMsg}>請輸入用戶名稱。</Text>
            </View>
          )}
          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 35,
              },
            ]}>
            電郵
          </Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#05375a" size={20} />
            <TextInput
              returnKeyType="done"
              autoCorrect={false}
              placeholder="你的電郵"
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={val => handleEmailChange(val)}
            />
            {data.check_emailEntry === null ? null : data.check_emailEntry ===
              true ? (
              <View /* animation="bounceIn" */>
                <Feather name="check-circle" color="green" size={20} />
              </View>
            ) : (
              <View /* animation="bounceIn" */>
                <Entypo name="circle-with-cross" color="red" size={20} />
              </View>
            )}
          </View>
          {data.isValidEmail ? null : (
            <View /* animation="fadeInLeft"  duration={500} */>
              <Text style={styles.errorMsg}>請輸入電郵。</Text>
            </View>
          )}
          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 35,
              },
            ]}>
            密碼
          </Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375a" size={20} />
            <TextInput
              returnKeyType="done"
              placeholder="你的密碼"
              secureTextEntry={data.secureTextEntry ? true : false}
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={val => handlePasswordChange(val)}
            />
            <TouchableOpacity
              onPress={updateSecureTextEntry}
              style={{height: 40, width: 40}}>
              {data.secureTextEntry ? (
                <Feather
                  name="eye-off"
                  color="grey"
                  size={20}
                  style={{alignSelf: 'flex-end'}}
                />
              ) : (
                <Feather
                  name="eye"
                  color="grey"
                  size={20}
                  style={{alignSelf: 'flex-end'}}
                />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidPassword ? null : (
            <View /* animation="fadeInLeft"  duration={500} */>
              <Text style={styles.errorMsg}>請輸入密碼。</Text>
            </View>
          )}

          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 35,
              },
            ]}>
            確認密碼
          </Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375a" size={20} />
            <TextInput
              returnKeyType="done"
              placeholder="確認你的密碼"
              secureTextEntry={data.confirm_secureTextEntry ? true : false}
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={val => handleConfirmPasswordChange(val)}
            />
            <TouchableOpacity
              onPress={updateConfirmSecureTextEntry}
              style={{height: 40, width: 40}}>
              {data.confirm_secureTextEntry ? (
                <Feather
                  name="eye-off"
                  color="grey"
                  size={20}
                  style={{alignSelf: 'flex-end'}}
                />
              ) : (
                <Feather
                  name="eye"
                  color="grey"
                  size={20}
                  style={{alignSelf: 'flex-end'}}
                />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidPasswordConfirmation ? null : (
            <View /* animation="fadeInLeft"  duration={500} */>
              <Text style={styles.errorMsg}>請確認密碼一致。</Text>
            </View>
          )}
          <View style={styles.textPrivate}>
            <Text style={styles.color_textPrivate}>
              註冊Taggie代表你同意我們的
            </Text>
            <Text style={[styles.color_textPrivate, {fontWeight: 'bold'}]}>
              {' '}
              服務條款
            </Text>
            <Text style={styles.color_textPrivate}> 和</Text>
            <Text style={[styles.color_textPrivate, {fontWeight: 'bold'}]}>
              {' '}
              隱私政策
            </Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity style={styles.signIn} onPress={clickSignUp}>
              <LinearGradient
                colors={['#08d4c4', '#01ab9d']}
                style={styles.signIn}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#fff',
                    },
                  ]}>
                  註冊
                </Text>
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={[
                styles.signIn,
                {
                  borderColor: '#009387',
                  borderWidth: 1,
                  marginTop: 15,
                },
              ]}>
              <Text
                style={[
                  styles.textSign,
                  {
                    color: '#009387',
                  },
                ]}>
                登入
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#009387',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: Platform.OS === 'ios' ? 3 : 5,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingTop: 30,
    paddingBottom: 10,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  color_textPrivate: {
    color: 'grey',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
});
