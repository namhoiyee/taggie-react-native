import {ScheduleData} from './state';

// export function loaded() {
//   console.log('loaded schedules');
//   return {
//     type: '@@Schedule/loaded' as const,
//   };
// }

export function loading() {
  console.log('loading schedules');
  return {
    type: '@@Schedule/loading' as const,
  };
}

export function failed(msg: string) {
  console.log('failed to load schedules', msg);
  return {
    type: '@@Schedule/failed' as const,
    msg,
  };
}

export function loadedSchedule(scheduleData: ScheduleData[]) {
  return {
    type: '@@Schedule/loadedSchedule' as const,
    scheduleData,
  };
}

export function wentOutFromSchedule() {
  return {
    type: '@@Schedule/wentOutFromSchedule' as const,
  };
}

export type ScheduleAction =
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>
  | ReturnType<typeof wentOutFromSchedule>
  | ReturnType<typeof loadedSchedule>;
