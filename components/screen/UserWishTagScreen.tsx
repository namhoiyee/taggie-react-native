/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect, useLayoutEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Animated,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  // ImageBackground,
} from 'react-native';

import {SwipeListView} from 'react-native-swipe-list-view';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';

// import Tags, {Tag} from '../../model/tags';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {Tag} from '../../helpers/types';
import {deleteWishTag, loadUserWishTags} from '../../redux/tags/thunk';
import {exclusiveClassesForWishTags} from '../../helpers/tags';
import LoadingComponent from './Loading/LoadingComponent';
import LinearGradient from 'react-native-linear-gradient';
import {imageReq} from '../../helpers/types';
import FastImage from 'react-native-fast-image';
import {isAndroid} from '../../utils/platform';
import CrushScreen from './Crush/CrushScreen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

type ListData = {
  key: string;
  title: string;
  details: string;
  id: number;
  classId: number;
  wishId: number;
};

const UserWishTagScreen = ({navigation}: any) => {
  const loadingStatus = useSelector((state: IRootState) => state.tags.status);
  const userWishTagsDetails = useSelector(
    (state: IRootState) => state.tags?.userWishTags,
  );
  const status = useSelector((state: IRootState) => state.tags?.status);
  const userWishTags = userWishTagsDetails?.data?.wishTags;
  // console.log('userWishTags', userWishTags);

  // console.log('userWishTagsDetails', userWishTagsDetails?.data?.tagCount);
  const [listData, setListData] = useState<ListData[] | undefined>(
    userWishTags && userWishTags.length > 0
      ? userWishTags
          .sort(a => (exclusiveClassesForWishTags.includes(a.class) ? -1 : 1))
          .map(userWishTag => ({
            key: `${userWishTag.dbPrimaryId}`,
            title: userWishTag.class,
            details: userWishTag.tag,
            id: userWishTag.id,
            classId: userWishTag.classId,
            wishId: userWishTag.dbPrimaryId,
          }))
      : undefined,
  );
  // console.log('listData', listData);
  const [itemToDeleteKey, setItemToDeleteKey] = useState<string | undefined>();
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );
  const dispatch = useDispatch();
  const [tagCount, setTagCount] = useState(16);

  console.log('tagCount', tagCount);

  useEffect(() => {
    dispatch(loadUserWishTags());
  }, [dispatch]);

  useEffect(() => {
    if (userWishTags) {
      setTagCount(userWishTags?.length);
    }
  }, [userWishTags]);

  console.log('tagCount2', tagCount);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerBackTitleVisible: false,
      title: 'Tag你所喜',
      headerRight: () => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            // width: 80,
            marginRight: 20,
          }}>
          {tagCount === 16 ? null : (
            <TouchableOpacity>
              <MaterialIcons
                name="add"
                size={25}
                color="#333333"
                onPress={() => {
                  navigation.navigate('EditWishTag', {
                    item: {
                      id: -1,
                      class: '',
                      tag: '',
                      classId: -1,
                      dbPrimaryId: -1,
                    },
                  });
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      ),
    });
  }, [navigation, tagCount]);

  useEffect(() => {
    setListData(
      userWishTags
        ?.sort(a => (exclusiveClassesForWishTags.includes(a.class) ? -1 : 1))
        .map(userWishTag => ({
          key: `${userWishTag.dbPrimaryId}`,
          title: userWishTag.class,
          details: userWishTag.tag,
          id: userWishTag.id,
          classId: userWishTag.classId,
          wishId: userWishTag.dbPrimaryId,
        })),
    );
  }, [userWishTags]);

  useEffect(() => {
    if (itemToDeleteKey && listData) {
      const tagToDelete = listData.find(item => item.key === itemToDeleteKey);
      if (tagToDelete) {
        const tagToDeleteRestructured = {
          id: tagToDelete.id,
          class: tagToDelete.title,
          tag: tagToDelete.details,
          classId: tagToDelete.classId,
          dbPrimaryId: tagToDelete.wishId,
        };
        dispatch(deleteWishTag(tagToDeleteRestructured));
      }
    }
  }, [dispatch, itemToDeleteKey, listData]);

  useEffect(() => {
    if (itemToDeleteKey && listData) {
      setListData(
        prevData =>
          prevData && prevData.filter(item => item.key !== itemToDeleteKey),
      );
      setItemToDeleteKey(undefined);
    }
  }, [itemToDeleteKey, listData]);

  const closeRow = useCallback((rowMap: any, rowKey: any) => {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  }, []);

  const editTag = useCallback(
    (item: Tag) => {
      console.log('element clicked', item);
      navigation.navigate('EditWishTag', {item});
    },
    [navigation],
  );

  const deleteRow = useCallback((rowKey: any) => {
    console.log('delete', rowKey);
    Alert.alert('確認刪除?', '將刪除Tag,刪除後可重新增加', [
      {text: '取消', style: 'default'},
      {
        text: '確認',
        style: 'destructive',
        onPress: () => {
          setItemToDeleteKey(rowKey);
          setTagCount(prev => prev - 1);
        },
      },
    ]);
  }, []);

  useEffect(() => {
    if (status === 'error') {
      dispatch(loadUserWishTags());
    }
  }, [dispatch, status]);

  const VisibleItem = (props: {data: {item: ListData}}) => {
    const {data} = props;

    return (
      <View style={[styles.rowFront, {height: 60}]}>
        <TouchableHighlight
          style={styles.rowFrontVisible}
          onPress={() =>
            editTag({
              id: data.item.id,
              class: data.item.title,
              tag: data.item.details,
              classId: data.item.classId,
              dbPrimaryId: data.item.wishId,
            })
          }
          underlayColor={'#aaa'}>
          <View style={{height: 60, overflow: 'hidden', borderRadius: 5}}>
            <FastImage
              //@ts-ignore
              source={imageReq[data.item.title]}
              style={{
                position: 'absolute',
                width: '100%',
                height: 70,
                top: -10,
              }}
              // style={{position: 'absolute', height: "100%", top: 12}}
              resizeMode="cover"
              // blurRadius={2}
            />
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={[
                '#333333',
                '#333333',
                '#333333',
                '#333333',
                'rgba(0, 0, 0, 0.5)',
                'rgba(0, 0, 0, 0.4)',
                'rgba(0, 0, 0, 0.3)',
                'rgba(0, 0, 0, 0.2)',
                'rgba(0, 0, 0, 0.1)',
                'rgba(0, 0, 0, 0)',
                'rgba(0, 0, 0, 0)',
              ]}
              style={{
                position: 'absolute',
                width: '100%',
                height: 70,
                top: -10,
              }}
            />
            <View style={{padding: isAndroid ? 5 : 10}}>
              <Text style={styles.title} numberOfLines={1}>
                {data.item.title === '習慣1'
                  ? '吸煙習慣'
                  : data.item.title === '習慣2'
                  ? '飲酒習慣'
                  : data.item.title}
              </Text>
              <Text style={styles.details} numberOfLines={1}>
                {data.item.details}
              </Text>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    );
  };

  const renderItem = (data: {item: ListData}) => {
    // const rowHeightAnimatedValue = new Animated.Value(60);

    return (
      <VisibleItem
        data={data}
        // rowHeightAnimatedValue={60}
        // removeRow={() => deleteRow(data.item.key)}
      />
    );
  };

  const HiddenItemWithActions = (props: any) => {
    const {
      swipeAnimatedValue,
      leftActionActivated,
      // rightActionActivated,
      // rowActionAnimatedValue,
      // rowHeightAnimatedValue,
      onClose,
      onDelete,
    } = props;

    // console.log('props', props);

    return (
      <View style={[styles.rowBack, {height: 60}]}>
        {/* {!leftActionActivated && (
          <PlatformTouchable
            style={[styles.backRightBtn, styles.backRightBtnLeft]}
            onPress={onClose}>
            <MaterialCommunityIcons
              name="close-circle-outline"
              size={25}
              style={styles.trash}
              color="#fff"
            />
          </PlatformTouchable>
        )} */}
        {!leftActionActivated && (
          <TouchableOpacity
            style={[styles.backRightBtn, styles.backRightBtnLeft]}
            onPress={onDelete}>
            <MaterialCommunityIcons
              name="trash-can-outline"
              size={25}
              color="#fff"
            />
          </TouchableOpacity>
        )}
        {!leftActionActivated && (
          <Animated.View
            style={[
              styles.backRightBtn,
              styles.backRightBtnRight,
              {
                flex: 1,
                width: 75,
              },
            ]}>
            <TouchableOpacity
              style={[styles.backRightBtn, styles.backRightBtnRight]}
              onPress={onClose}>
              <Animated.View
                style={[
                  styles.trash,
                  {
                    transform: [
                      {
                        scale: swipeAnimatedValue.interpolate({
                          inputRange: [-90, -45],
                          outputRange: [1, 0],
                          extrapolate: 'clamp',
                        }),
                      },
                    ],
                  },
                ]}>
                <Ionicons name="ios-close-circle" size={25} color="#fff" />
              </Animated.View>
            </TouchableOpacity>
          </Animated.View>
        )}
      </View>
    );
  };

  const renderHiddenItem = (data: {item: ListData}, rowMap: any) => {
    // const rowHeightAnimatedValue = new Animated.Value(60);

    return (
      <HiddenItemWithActions
        data={data}
        rowMap={rowMap}
        // rowActionAnimatedValue={rowActionAnimatedValue}
        // rowHeightAnimatedValue={rowHeightAnimatedValue}
        onClose={() => closeRow(rowMap, data.item.key)}
        onDelete={() => deleteRow(data.item.key)}
      />
    );
  };

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View style={[styles.container, {marginBottom: tabBarHeight}]}>
          <StatusBar barStyle="dark-content" />
          {/* <StatusBar backgroundColor="#FF6347" barStyle="light-content"/> */}
          <SwipeListView
            showsVerticalScrollIndicator={false}
            data={listData}
            renderItem={renderItem}
            renderHiddenItem={renderHiddenItem}
            // leftOpenValue={75}
            rightOpenValue={-150}
            disableRightSwipe
            // onRowDidOpen={onRowDidOpen}
            // leftActivationValue={100}
            // rightActivationValue={
            //   -Math.min(Dimensions.get('window').width * 0.7, 500)
            // }
            // leftActionValue={0}
            useAnimatedList={true}
            tension={20}
            closeOnRowBeginSwipe={true}
            closeOnRowOpen={true}
            closeOnRowPress={true}
            // rightActionValue={-150}
            // onLeftAction={onLeftAction}
            // onRightAction={onRightAction}
            // onLeftActionStatusChange={onLeftActionStatusChange}
            // onRightActionStatusChange={onRightActionStatusChange}
            // onSwipeValueChange={onSwipeValueChange}
          />
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
};

export default UserWishTagScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f4f4f4',
    flex: 1,
  },
  backTextWhite: {
    color: '#FFF',
  },
  rowFront: {
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: 60,
    marginTop: 5,
    marginHorizontal: 10,
    marginBottom: 5,
    shadowColor: '#999',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
  },
  rowFrontVisible: {
    backgroundColor: '#333333',
    borderRadius: 5,
    height: 60,
    // padding: isAndroid ? 5 : 10,
    marginBottom: 15,
  },
  rowBack: {
    backgroundColor: '#f4f4f4',
    flex: 1,
    margin: 5,
    marginRight: 15,
    marginLeft: 11,
    marginBottom: 6,
    borderRadius: 5,
    // borderBottomRightRadius: 5,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    right: 0,
    // paddingRight: 17,
  },
  backRightBtnLeft: {
    backgroundColor: 'red',
    // width: 0,
    right: 75,
    // elevation: 100,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  backRightBtnRight: {
    backgroundColor: '#a8a8a8',
    right: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  trash: {
    height: 25,
    width: 25,
    // marginRight: 7,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#fff',
  },
  details: {
    fontSize: 14,
    color: '#fff',
  },
});
