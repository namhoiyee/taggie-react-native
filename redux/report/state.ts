export type ReportState = {
  status: 'reported' | 'normal';
};

export const initialState: ReportState = {
  status: 'normal',
};
