/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {Agenda} from 'react-native-calendars';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {S3Link} from '../../../helpers/api';
import {wentOutFromSchedule} from '../../../redux/schedule/action';
import {loadScheduleThunk} from '../../../redux/schedule/thunk';
import {IRootState} from '../../../redux/store';
import {translateTime} from '../../utils/calculateTime';

type SchedulerDetail = {name: string; profilePic: string};

const ScheduleDatingScreen = ({navigation}: any) => {
  const schedules = useSelector(
    (state: IRootState) => state.schedule.scheduleData,
  );
  const [loadedSchedule, setLoadedSchedule] = useState(false);
  const [updateSchedules, setUpdateSchedules] =
    useState<{[x: string]: SchedulerDetail[]} | undefined>();
  console.log('updateSchedules', updateSchedules);
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener('beforeRemove', () => {
      console.log('going out from schedule');
      dispatch(wentOutFromSchedule());
    });

    return unsubscribe;
  }, [dispatch, navigation]);

  useEffect(() => {
    if (!loadedSchedule) {
      dispatch(loadScheduleThunk());
      setLoadedSchedule(true);
    }
  }, [dispatch, loadedSchedule]);

  useEffect(() => {
    const schedulesToShow =
      schedules &&
      schedules.reduce((acc, schedule) => {
        const {scheduled_at, friend, name, profilePic} = schedule;
        const date = new Date(scheduled_at);
        const month = date.getMonth() + 1;
        const dateDate = date.getDate();
        const currentScheduleDate = `${date.getFullYear()}-${(month < 10
          ? '0' + month.toString()
          : month.toString()
        ).slice(0, 2)}-${
          dateDate < 10 ? '0' + dateDate.toString() : dateDate.toString()
        }`;
        return {
          ...acc,
          [currentScheduleDate]: [
            // @ts-ignore
            ...(acc[currentScheduleDate] || []),
            {
              name: `與${friend}於${translateTime(scheduled_at)}到${name}`,
              profilePic: profilePic,
            },
          ],
        };
      }, {});

    setUpdateSchedules(schedulesToShow);
    console.log('schedulesToShow', schedulesToShow);
  }, [schedules]);

  const renderItem = (item: SchedulerDetail) => {
    // console.log(item);
    return (
      <View style={styles.itemContainer}>
        <View style={{width: '50%', marginRight: 15}}>
          <Text>{item.name}</Text>
        </View>
        <View style={{borderRadius: 100, overflow: 'hidden'}}>
          <FastImage
            source={{uri: S3Link + item.profilePic}}
            style={{height: 50, width: 50}}
          />
        </View>
      </View>
    );
  };

  const renderEmptyData = () => {
    return (
      <View style={styles.itemContainer}>
        <Text>此日沒有活動</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.safe}>
      {updateSchedules && (
        // @ts-ignore
        <Agenda
          items={updateSchedules as any}
          renderItem={renderItem}
          renderEmptyData={renderEmptyData}
          pastScrollRange={2}
          futureScrollRange={2}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safe: {
    flex: 1,
  },
  itemContainer: {
    backgroundColor: 'white',
    margin: 5,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    minHeight: 40,
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

export default ScheduleDatingScreen;
