import {api} from '../../helpers/api';
import {
  failed,
  loadedAllClassesAndTags,
  loadedUserWishTags,
  loadedUserTags,
  editedWishTags,
  addedWishTags,
  deletedWishTags,
  deletedUserTags,
  editedUserTags,
  addedUserTags,
  loading,
} from './action';
import {IRootThunkDispatch, IRootState} from '../store';
import {Alert} from 'react-native';
import {Tag, TagClass} from '../../helpers/types';
// import {loading, loaded} from '../loading/action';

export function loadUserTags() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/getUserTags', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result = await res.json();
      // console.log('loadUserWishTags', result.data.wishTags);
      dispatch(loadedUserTags(result));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取你的Tag時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to get Tags: ' + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function editWishTags(data: {oldTag: Tag; newTag: Tag}) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userWishTags = getState().tags.userWishTags;
      if (!userWishTags) {
        throw new Error('no userWishTags');
      }
      // console.log('tag.id', data.oldTag.dbPrimaryId);
      const res = await api.patch(
        '/editWishTag',
        {
          oldTag: data.oldTag,
          newTag: data.newTag,
        },
        userToken,
      );
      if (res.status !== 202) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          msg: string;
        };
      } = await res.json();
      // console.log('result', result);
      // console.log('tagssss', data.oldTag.tag, data.newTag.tag);
      const updatedUserWishTags = userWishTags.data.wishTags.map(tag => {
        return tag.id === data.oldTag.id ? data.newTag : tag;
        // return tag;
      });
      // const updatedUserWishTags2 = userWishTags.data.wishTags.map(tag => {
      //   console.log('updatingUserWishTags', tag.tag, data.oldTag.tag);
      //   return tag.id === data.oldTag.id ? data.newTag.tag : tag.tag;
      // });
      // console.log('updatedUserWishTags', updatedUserWishTags2);
      dispatch(
        editedWishTags(result.isSuccess, {
          data: {
            tagCount: updatedUserWishTags.length,
            wishTags: updatedUserWishTags,
          },
        }),
      );
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('編輯Tags時失敗', '請稍後重試' + error.msg, [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to edit Tags: ' + error.msg));
      // dispatch(loaded());
      return;
    }
  };
}

export function editUserTags(data: {oldTag: Tag; newTag: Tag}) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userTags = getState().tags.userTags;
      if (!userTags) {
        throw new Error('no userTags');
      }
      // console.log('tag.id', data.oldTag.dbPrimaryId, data.newTag.dbPrimaryId);
      const res = await api.patch(
        '/editUserTag',
        {
          oldTag: data.oldTag,
          newTag: data.newTag,
        },
        userToken,
      );
      if (res.status !== 202) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          msg: string;
        };
      } = await res.json();
      // console.log('result', result);
      // console.log('tagssss', data.oldTag.tag, data.newTag.tag);
      const updatedUserTags = userTags.data.userTags.map(tag => {
        return tag.id === data.oldTag.id ? data.newTag : tag;
        // return tag;
      });
      // const updatedUserWishTags2 = userWishTags.data.wishTags.map(tag => {
      //   console.log('updatingUserWishTags', tag.tag, data.oldTag.tag);
      //   return tag.id === data.oldTag.id ? data.newTag.tag : tag.tag;
      // });
      // console.log('updatedUserWishTags', updatedUserWishTags2);
      dispatch(
        editedUserTags(result.isSuccess, {
          data: {
            tagCount: updatedUserTags.length,
            userTags: updatedUserTags,
          },
        }),
      );
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('編輯Tags時失敗', '請稍後重試' + error.msg, [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to edit Tags: ' + error.msg));
      // dispatch(loaded());
      return;
    }
  };
}

export function loadUserWishTags() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/getWishTags', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result = await res.json();
      // console.log('loadUserWishTags', result.data.wishTags);
      dispatch(loadedUserWishTags(result));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取你尋找的人的Tag時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to get Tags: ' + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function loadAllClassesAndTags() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    // dispatch(loading());
    try {
      const allClassesAndTags = getState().tags.tagClassesAndTags;
      if (allClassesAndTags) {
        // console.log('loaded already.');
        dispatch(loadedAllClassesAndTags(allClassesAndTags));
        return;
      }
      dispatch(loading());
      const userToken = getState().auth.token;
      const res = await api.get('/getClassAndTags', userToken);
      if (res.status !== 200) {
        throw new Error(await res.json());
      }
      const result: {data: TagClass[]} = await res.json();
      dispatch(loadedAllClassesAndTags(result.data));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取Tags時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to get all tags: ' + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function addWishTag(newTag: Tag) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userWishTags = getState().tags.userWishTags;
      if (!userWishTags) {
        throw new Error('no userWishTags');
      }
      // console.log('tag.id', newTag);
      const res = await api.post(
        '/addWishTag',
        {
          newTag: newTag,
        },
        userToken,
      );
      if (res.status === 401) {
        Alert.alert('添加Tags時失敗', '請稍後重試', [
          {text: '確認', style: 'default'},
        ]);
        return;
      }
      if (res.status !== 201) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          tag: Tag;
        };
      } = await res.json();
      // console.log('result', result);
      // console.log('tagAdded', data.oldTag.tag, data.newTag.tag);
      const updatedUserWishTags = [
        ...userWishTags.data.wishTags,
        result.data.tag,
      ];
      // console.log('updatedUserWishTags', updatedUserWishTags);

      dispatch(
        addedWishTags(result.isSuccess, {
          data: {
            tagCount: updatedUserWishTags.length,
            wishTags: updatedUserWishTags,
          },
        }),
      );
      // dispatch(loaded());
    } catch (error) {
      Alert.alert('添加Tags時失敗', '請稍後重試' + error.msg + error.message, [
        {text: '確認', style: 'default'},
      ]);
      dispatch(failed('failed to add Tags: ' + error.msg + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function deleteWishTag(originalTag: Tag) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userWishTags = getState().tags.userWishTags;
      if (!userWishTags) {
        throw new Error('no userWishTags');
      }
      // console.log('tag.id', originalTag);
      const res = await api.delete(
        '/deleteWishTag',
        {
          originalTag,
        },
        userToken,
      );
      if (res.status !== 202) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          msg: string;
        };
      } = await res.json();
      // console.log('result', result);
      // console.log('tagAdded', data.oldTag.tag, data.newTag.tag);
      const updatedUserWishTags = userWishTags.data.wishTags.filter(
        tag => tag.id !== originalTag.id,
      );
      dispatch(
        deletedWishTags(result.isSuccess, {
          data: {
            tagCount: updatedUserWishTags.length,
            wishTags: updatedUserWishTags,
          },
        }),
      );
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert(
      //   '刪除Tags時失敗',
      //   '請稍後重試。原因: ' + originalTag.tag + '不能刪除',
      //   [{text: '確認', style: 'default'}],
      // );
      dispatch(
        failed('failed to delete wish Tags: ' + error.msg + error.message),
      );
      // dispatch(loaded());
      return;
    }
  };
}

export function deleteUserTag(originalTag: Tag) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userTags = getState().tags.userTags;
      if (!userTags) {
        throw new Error('no userTags');
      }
      // console.log('tag.id', originalTag);
      const res = await api.delete(
        '/deleteUserTag',
        {
          originalTag,
        },
        userToken,
      );
      if (res.status !== 202) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          msg: string;
        };
      } = await res.json();
      // console.log('result', result);
      // console.log('tagAdded', data.oldTag.tag, data.newTag.tag);
      const updatedUserTags = userTags.data.userTags.filter(
        tag => tag.id !== originalTag.id,
      );
      dispatch(
        deletedUserTags(result.isSuccess, {
          data: {
            tagCount: updatedUserTags.length,
            userTags: updatedUserTags,
          },
        }),
      );
      // dispatch(loaded());
    } catch (error) {
      Alert.alert(
        '刪除Tags時失敗',
        '請稍後重試。原因: ' + originalTag.tag + '不能刪除',
        [{text: '確認', style: 'default'}],
      );
      dispatch(failed('failed to delete Tags: ' + error.msg + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function addUserTag(newTag: Tag) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userTags = getState().tags.userTags;
      if (!userTags) {
        throw new Error('no userTags');
      }
      // console.log('tag.id', newTag);
      const res = await api.post(
        '/addUserTag',
        {
          newTag: newTag,
        },
        userToken,
      );
      if (res.status === 401) {
        // Alert.alert('添加Tags時失敗', '請稍後重試', [
        //   {text: '確認', style: 'default'},
        // ]);
        return;
      }
      if (res.status !== 201) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          tag: Tag;
        };
      } = await res.json();
      // console.log('result', result);
      // console.log('tagAdded', data.oldTag.tag, data.newTag.tag);
      const updatedUserTags = [...userTags.data.userTags, result.data.tag];
      // console.log('updatedUserTags', updatedUserTags);

      dispatch(
        addedUserTags(result.isSuccess, {
          data: {
            tagCount: updatedUserTags.length,
            userTags: updatedUserTags,
          },
        }),
      );
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('添加Tags時失敗', '請稍後重試' + error.msg + error.message, [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to add Tags: ' + error.msg + error.message));
      // dispatch(loaded());
      return;
    }
  };
}
