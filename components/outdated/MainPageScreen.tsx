/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState, useCallback} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  // Alert,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  Modal,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';
import {useDispatch, useSelector} from 'react-redux';
import FastImage from 'react-native-fast-image';

import {IRootState} from '../../redux/store';
import {isAndroid} from '../../utils/platform';
import {Colors} from '../../utils/Colors';
import GoogleMapComponent from '../googleMap/GoogleMapComponent';
// import {ILocation} from '../../redux/match/state';
import {
  getUserPermission,
  locateUser,
  insertGeolocation,
  initMatching,
  likeOrUnlikePeople,
  updateCurrentMatching,
  checkIfAfter15MinsFromLastMatch,
} from '../../redux/match/thunk';
// import {loadSocket} from '../../redux/socket/thunk';
import {MatchResult, MatchedUserDetail} from '../../helpers/types';
import {S3Link} from '../../helpers/api';
import LoadingComponent from './Loading/LoadingComponent';
import {rematch} from '../../redux/coin/thunk';
import CrushScreen from './Crush/CrushScreen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const {width, height} = Dimensions.get('window');

export default function MainPageScreen({navigation}: any) {
  const userToken = useSelector((state: IRootState) => state.auth.token);
  const location = useSelector((state: IRootState) => state.match.location);
  const userAllowedGeolocation = useSelector(
    (state: IRootState) => state.match.userPermitted,
  );
  const insertedGeoTime = useSelector(
    (state: IRootState) => state.match.insertedGeoTime,
  );
  const matchResult: MatchResult | undefined = useSelector(
    (state: IRootState) => state.match.matchResult,
  );
  const loadingStatus = useSelector(
    (state: IRootState) => state.loading.status,
  );
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const needToDoMatching = useSelector(
    (state: IRootState) => state.match.isAfter15Mins,
  );
  const [matchResultDetail, setMatchResultDetail] =
    useState<MatchedUserDetail | undefined>();
  const [userMatchedLocation, setUserMatchedLocation] =
    useState<{x: number; y: number} | undefined>();
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );
  const socket = useSelector((state: IRootState) => state.socket.socket);
  const [currentSwipe, setCurrentSwipe] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [count, setCount] = useState(0);

  // if (matchResultDetail && matchResultDetail.length > 0) {
  //   console.log(
  //     'matchResultDetail',
  //     matchResultDetail,
  //     matchResultDetail[1]?.profilePic?.url,
  //   );
  // }

  const dispatch = useDispatch();

  useEffect(() => {
    if (userToken && socket) {
      const onEvent = (rooms: string[]) => {
        console.log(rooms);
        rooms?.forEach(room => {
          console.log('joined room', room);
          socket.emit('join', room);
        });
      };
      socket.on('room-to-join', onEvent);

      return () => {
        socket.off('room-to-join', onEvent);
      };
    }
  }, [userToken, socket]);

  useEffect(() => {
    dispatch(getUserPermission());
  }, [dispatch]);

  useEffect(() => {
    if (userAllowedGeolocation) {
      dispatch(locateUser());
    }
  }, [dispatch, userAllowedGeolocation]);

  useEffect(() => {
    if (location && userToken) {
      console.log('location', location);
      dispatch(insertGeolocation(userToken, location));
    }
  }, [dispatch, location, userToken]);

  useEffect(() => {
    if (insertedGeoTime && userToken) {
      dispatch(checkIfAfter15MinsFromLastMatch(userToken));
    }
  }, [dispatch, insertedGeoTime, userToken]);

  useEffect(() => {
    if (needToDoMatching && insertedGeoTime && userToken) {
      dispatch(initMatching(userToken));
    }
  }, [dispatch, insertedGeoTime, needToDoMatching, userToken]);

  useEffect(() => {
    if (!needToDoMatching && needToDoMatching !== undefined && userToken) {
      dispatch(updateCurrentMatching(userToken));
    }
  }, [dispatch, needToDoMatching, userToken]);

  useEffect(() => {
    if (matchResult && matchResult?.otherUserDetail) {
      setUserMatchedLocation(matchResult.userLocation);
      setMatchResultDetail(
        matchResult?.otherUserDetail?.map(userDetail => {
          const {
            id,
            matchedTags,
            userAge,
            user_tags,
            username,
            gallery,
            profilePic,
            text_description,
          } = userDetail;
          return {
            id,
            matchedTags,
            userAge,
            user_tags,
            username,
            gallery,
            profilePic,
            text_description,
          };
        }),
      );
      setCount(matchResult.otherUserDetail.length);
      setCurrentSwipe(0);
    }
  }, [matchResult]);

  const likeOrUnlikePeopleNow = useCallback(
    (likedUserId: number, likeRes: 'like' | 'unlike') => {
      if (userToken) {
        dispatch(likeOrUnlikePeople(userToken, likedUserId, likeRes));
      }
    },
    [dispatch, userToken],
  );

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  function submitRematchRequest() {
    dispatch(rematch());
    setModalVisible(false);
  }

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View style={styles.imageContainer}>
          {matchResultDetail && matchResultDetail.length && count > 0 ? (
            matchResultDetail.map(
              (match, index) =>
                index === currentSwipe && (
                  <ScrollView showsVerticalScrollIndicator={false} key={index}>
                    <View>
                      <View>
                        <FastImage
                          source={{
                            uri: S3Link + match.profilePic?.url,
                            priority: FastImage.priority.high,
                          }}
                          style={styles.image}
                          // resizeMode='contain'
                        />
                        <TouchableOpacity
                          style={{
                            ...styles.iconContainerLeft,
                            bottom: tabBarHeight ? tabBarHeight + 80 : 120,
                          }}
                          onPress={() => {
                            // Alert.alert('unlike');
                            likeOrUnlikePeopleNow(match.id, 'unlike');
                            setCurrentSwipe(prev => prev + 1);
                            setCount(prev => prev - 1);
                          }}>
                          <FontAwesome5
                            name="heart-broken"
                            color="black"
                            size={25}
                            style={styles.iconLeft}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={{
                            ...styles.iconContainerRight,
                            bottom: tabBarHeight ? tabBarHeight + 80 : 120,
                          }}
                          onPress={() => {
                            // Alert.alert('like');
                            likeOrUnlikePeopleNow(match.id, 'like');
                            setCurrentSwipe(prev => prev + 1);
                            setCount(prev => prev - 1);
                          }}>
                          <MaterialIcons
                            name="favorite"
                            color="rgb(253,127,156)"
                            size={25}
                            style={styles.iconRight}
                          />
                        </TouchableOpacity>
                        <View
                          style={{
                            ...styles.matchedUserDetail,
                            bottom: tabBarHeight ? tabBarHeight + 140 : 220,
                          }}>
                          <Text style={styles.matchedUserDetailText}>
                            {match.username}&nbsp;
                            {match.userAge}歲
                          </Text>
                        </View>
                      </View>
                      <View style={styles.tagsContainer}>
                        {location ? (
                          <View style={styles.tagContainer}>
                            <Text>Latitude: {location.latitude}</Text>
                            <Text>Longitude: {location.longitude}</Text>
                          </View>
                        ) : (
                          <View style={styles.tagContainer}>
                            <Text>Loading...</Text>
                          </View>
                        )}
                        <Text style={styles.matchedUserDetailText}>
                          {match.text_description}
                        </Text>
                        {match.user_tags.map((tag, idx) => (
                          <View
                            style={[
                              styles.tagContainer,
                              {
                                backgroundColor: match.matchedTags.includes(tag)
                                  ? '#FF5740'
                                  : 'rgba(238, 164, 127, 1)',
                              },
                            ]}
                            key={idx}>
                            <Text style={styles.text}>{tag}</Text>
                          </View>
                        ))}
                      </View>
                      {match.gallery.length > 0 &&
                        match.gallery.map((photo, photoIndex) => (
                          <View key={photoIndex}>
                            <FastImage
                              source={{
                                uri: S3Link + photo.url,
                                priority:
                                  photoIndex <= 1
                                    ? FastImage.priority.normal
                                    : FastImage.priority.low,
                              }}
                              style={styles.image}
                              // resizeMode='contain'
                            />
                          </View>
                        ))}
                      <View
                        style={{
                          alignItems: 'center',
                          height: 20,
                          backgroundColor: Colors.backgroundGray,
                          borderRadius: 20,
                          justifyContent: 'center',
                        }}>
                        <Text>你們相遇的地點:</Text>
                      </View>
                      <View style={styles.container}>
                        <GoogleMapComponent
                          location={
                            userMatchedLocation
                              ? userMatchedLocation
                              : {x: 114.1483671, y: 22.2875585}
                          }
                        />
                      </View>
                      <View style={{height: tabBarHeight}} />
                    </View>
                  </ScrollView>
                ),
            )
          ) : (
            <View>
              <View style={styles.refreshBtn}>
                {/* <Button
                  title="Rematch"
                  onPress={() => {console.log("press rematch")}}
                /> */}
                <TouchableOpacity
                  style={styles.refresh}
                  onPress={() => setModalVisible(true)}>
                  <MaterialIcons
                    name="group-add"
                    color="rgb(253,127,156)"
                    size={70}
                  />
                </TouchableOpacity>
              </View>
              <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                  setModalVisible(false);
                }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {/* <TouchableWithoutFeedback
                    onPress={() => setModalVisible(false)}
                    style={styles.modal}> */}
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View style={styles.detailContent}>
                      <ScrollView>
                        <View style={styles.row}>
                          <Text style={styles.cardTitle}>
                            重新配對需要消費1枚Taggie幣
                          </Text>
                        </View>
                        <View style={styles.btn}>
                          <Button
                            title={isAndroid ? '     取消     ' : '取消'}
                            onPress={() => setModalVisible(false)}
                          />
                          <Button
                            title={isAndroid ? '     確認     ' : '確認'}
                            onPress={submitRematchRequest}
                          />
                        </View>
                      </ScrollView>
                    </View>
                  </View>
                  {/* </TouchableWithoutFeedback> */}
                </View>
              </Modal>
            </View>
          )}
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
    // marginTop: statusBarHeight,
  },
  // flatList: {
  //   position: 'absolute',
  // },
  image: {
    width,
    height,
    borderRadius: isAndroid ? 0 : 20,
  },
  // listContainer: {
  //   backgroundColor: '#333333',
  // },
  tagsContainer: {
    // minHeight: 200,
    backgroundColor: Colors.mainBlue,
    borderRadius: isAndroid ? 0 : 20,
    padding: 20,
    flexDirection: 'row',
    width: width,
    flexWrap: 'wrap',
    // height: 200,
  },
  tagContainer: {
    borderRadius: 20,
    // backgroundColor: /* 'rgba(238, 164, 127, 1)'  */ '#FF5740',
    // minWidth: '30%',
    // maxHeight: 40,
    padding: 10,
    alignSelf: 'stretch',
    marginRight: 10,
    marginVertical: 10,
    justifyContent: 'center',
    alignContent: 'center',
    minWidth: 50,
  },
  matchedUserDetail: {
    position: 'absolute',
    left: 20,
    // height: 30,
    // width: '100%',
    backgroundColor: 'rgba(0,0,0, 0.02)',
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  matchedUserDetailText: {
    color: '#fff',
    fontSize: 24,
    textShadowColor: 'black',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
  },
  iconContainerLeft: {
    position: 'absolute',
    left: 40,
    height: 40,
    width: 40,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    borderRadius: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainerRight: {
    position: 'absolute',
    right: 40,
    height: 40,
    width: 40,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    borderRadius: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconLeft: {
    position: 'absolute',
  },
  iconRight: {
    position: 'absolute',
    top: 7.5,
  },
  text: {
    color: 'rgb(255,255,255)',
    textAlign: 'center',
  },
  container: {
    height: 300,
    width: '100%',
    borderRadius: 20,
  },
  refreshBtn: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  refresh: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  detailContent: {
    padding: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    width: '80%',
    borderRadius: 20,
    alignItems: 'center',
  },
  cardTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 10,
  },
  row: {
    marginVertical: 10,
  },
  btn: {
    marginVertical: 10,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
