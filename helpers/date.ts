export const isDate = function (input: any) {
  if (Object.prototype.toString.call(input) === '[object Date]') {
    return true;
  }
  return false;
};

export const reverseTimeDifference = (timestamp: Date) => {
  const newDate = new Date(timestamp);
  const year = newDate.getFullYear();
  const month = newDate.getMonth();
  const date = newDate.getDate();

  // console.log('date', year, month, date);

  return new Date(Date.UTC(year, month, date, 0, 0, 0, 0));
};
