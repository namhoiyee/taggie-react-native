import React, { Component, useCallback, useEffect, useRef, useState } from "react"
import { useDispatch } from "react-redux"
import { ImageUpload } from "../../helpers/types";
import { Alert, Button, TextInput, TouchableOpacity } from 'react-native';
import { submitQuestionnaireThunk } from "../../redux/questionnaire/thunk";
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  Animated,
  Image
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';
import Entypo from "react-native-vector-icons/Entypo";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { RadioButton } from 'react-native-paper';
import {DateTimePickerComponent} from '../utils/DateTimePicker';
import { isAndroid } from "../../utils/platform";
import { isDate, reverseTimeDifference } from "../../utils/date";

export default function Questionnaire(){

  const [data, setData] = useState<{
    gender: "" | "male" | "female" | "其他",
    birthday: Date | null,
    smoke: "" | "常吸煙" | "少吸煙" | "唔吸煙",
    drink: "" | "常飲酒" | "少飲酒" | "唔飲酒",
    userTags: number[],
    userWishTags: number[],
    textDescription: string
  }>({
    gender: "",
    birthday: null,
    smoke: "",
    drink: "",
    userTags: [],
    userWishTags: [],
    textDescription: ""
  })
  const [dataCheck, setDataCheck] = useState(new Array(7).fill(false))
  // const [currentProfilePic, setCurrentProfilePic] = useState<ImageUpload[]>();
  const [questionOpen, setQuestionOpen] = useState([true, false, false, false, false, false, false])
  const [loading, setLoading] = useState(true)
  const [fakeData, setFakeData] = useState([
    {
      "id":1,
      "name": "Q1",
    },
    {
      "id":2,
      "name": "Q2",
    },
    {
      "id":3,
      "name": "Q3",
    },
    {
      "id":4,
      "name": "Q4",
    },
    {
      "id":5,
      "name": "Q5",
    },
    {
      "id":6,
      "name": "Q6",
    },
    {
      "id":6,
      "name": "Q7",
    }
  ])
  const pan = useRef(new Animated.ValueXY()).current;
  const list = useRef(new Animated.ValueXY()).current;
  const [show, setShow] = useState(isAndroid ? false : true);
  const onChange = useCallback(
    (event: any, selectedDate: any) => {
      const currentDate = isDate(selectedDate)
        ? selectedDate
        : new Date('2000-01-01');
      // console.log('currentDateBBD', reverseTimeDifference(currentDate));
      setShow(!isAndroid);
      setData(prev => ({
        ...prev,
        birthday: reverseTimeDifference(currentDate),
      }));
      let newArray = dataCheck
      newArray.splice(1, 1, true)
      setDataCheck(newArray)
      setTimeout(()=>{
        clickQ(2)
      }, 700)
    },
    [],
  );

  const showMode = useCallback(
    (/* currentMode: any */) => {
      setShow(true);
      // setMode(currentMode);
    },
    [],
  );
  
  useEffect(function() {
    const getData = async () => {
      // const resp = await fetch(URL);
      // const data = await resp.json();
      // setData(data);
      setLoading(false);
    };
    getData();

    Animated.timing(pan, {
      toValue:{x:-400,y:0},
      delay:1000,
      useNativeDriver:false
    }).start();

    Animated.timing(list, {
      toValue:{x:0,y:-300},
      delay:2000,
      useNativeDriver:false
    }).start();
  }, [])

  const dispatch = useDispatch()
  
  function genderOnChange(gender: "male" | "female" | "其他"){
    setData({...data, gender: gender})
    let newArray = dataCheck
    newArray.splice(0, 1, true)
    setDataCheck(newArray)
    setTimeout(()=>{
      clickQ(1)
    }, 500)
  }
  function smokeOnChange(smoke: "常吸煙" | "少吸煙" | "唔吸煙"){
    setData({...data, smoke: smoke})
    let newArray = dataCheck
    newArray.splice(2, 1, true)
    setDataCheck(newArray)
    setTimeout(()=>{
      clickQ(3)
    }, 500)
  }
  function drinkOnChange(drink: "常飲酒" | "少飲酒" | "唔飲酒"){
    setData({...data, drink: drink})
    let newArray = dataCheck
    newArray.splice(3, 1, true)
    setDataCheck(newArray)
    setTimeout(()=>{
      clickQ(4)
    }, 500)
  }
  function userTagsOnChange(userTags: number){
    // let newArray = data.userTags
    // newArray.push(userTags)
    // setData({...data, userTags: newArray})
    let newArray = dataCheck
    newArray.splice(4, 1, true)
    setDataCheck(newArray)
    setTimeout(()=>{
      clickQ(5)
    }, 500)
  }
  function userWishTagsOnChange(userWishTags: number){
    // let newArray = data.userWishTags
    // newArray.push(userWishTags)
    // setData({...data, userWishTags: newArray})
    let newArray = dataCheck
    newArray.splice(5, 1, true)
    setDataCheck(newArray)
    setTimeout(()=>{
      clickQ(6)
    }, 500)
  }
  function textDescriptionOnChange(textDescription: string){
    setData({...data, textDescription: textDescription})
    let newArray = dataCheck
    newArray.splice(6, 1, true)
    setDataCheck(newArray)
  }

  function submitQuestionnaire(){
    if (data.gender === ""){
      Alert.alert('提交錯誤', '請選擇性別', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    if (data.birthday === null){
      Alert.alert('提交錯誤', '請選擇出生日期', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    if (data.smoke === null){
      Alert.alert('提交錯誤', '請選擇你的吸煙習慣', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    if (data.drink === null){
      Alert.alert('提交錯誤', '請選擇你的飲酒習慣', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    if (data.userTags.length < 4){
      Alert.alert('提交錯誤', '請選擇最少四個可以代表你的TAG', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    if (data.userWishTags.length < 4){
      Alert.alert('提交錯誤', '請選擇最少四個TAG黎代表你想遇到的人', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    if (data.textDescription === ""){
      Alert.alert('提交錯誤', '請填寫你的自我介紹', [
        {text: '確認', style: 'default'},
      ]);
      return
    }
    dispatch(submitQuestionnaireThunk(data))
  }

  function submitFirstPic(){
  
    // dispatch(submitFirstPicThunk())
  }
  
  // after got registration_status
  // let registration_status // from token
  // if (registration_status === 1){
  //   return <Questionnaire />
  // } else if (registration_status === 2){
  //   return (Edit Profile Pic Component)
  // } else if (registration_status === 3){
  //   return (normal page)
  // } else {
  //   return (loading)
  // }

  function clickQ(index: number){
    if (index === 0){
      questionOpen[0] === false 
      ? setQuestionOpen([true, false, false, false, false, false, false]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
    if (index === 1){
      questionOpen[1] === false 
      ? setQuestionOpen([false, true, false, false, false, false, false]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
    if (index === 2){
      questionOpen[2] === false 
      ? setQuestionOpen([false, false, true, false, false, false, false]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
    if (index === 3){
      questionOpen[3] === false 
      ? setQuestionOpen([false, false, false, true, false, false, false]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
    if (index === 4){
      questionOpen[4] === false 
      ? setQuestionOpen([false, false, false, false, true, false, false]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
    if (index === 5){
      questionOpen[5] === false 
      ? setQuestionOpen([false, false, false, false, false, true, false]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
    if (index === 6){
      questionOpen[6] === false 
      ? setQuestionOpen([false, false, false, false, false, false, true]) 
      : ""
      // setQuestionOpen(new Array(7).fill(false))
    }
  }

  return (
    <LinearGradient
    colors={['#f26a50', '#f20042', '#f20045']}
    style={styles.gradient}
   >
      <View style={styles.headerContainer}>
        <Text style={styles.header}>介紹下自己</Text>
      </View>
      <ScrollView
          horizontal
          style={styles.proContainer}
          showsHorizontalScrollIndicator={false}
      >
        {loading ? 
          (
            <ActivityIndicator size='small' color='#FFF'/>
          ):(
            <Animated.View style={[pan.getLayout(),styles.card]}>
              {
                fakeData.map((item, index) => (
                  <TouchableOpacity 
                    onPress={()=>{clickQ(index)}}
                  >
                    <View style={{
                      flex:1,
                      alignItems:'center',
                      justifyContent:'center',
                    }}>
                      <LinearGradient
                        colors={dataCheck[index] === false ? ['#ffba00', '#ffdd80', '#ffba00'] : ['#88CC00', '#80d280', '#88CC00']}
                        style={styles.circle}
                      >
                        <Text style={{color: "black", fontSize: 20}}>{item.name}</Text>
                      </LinearGradient>
                    </View>
                  </TouchableOpacity>
                ))
              }
              <TouchableOpacity 
                onPress={submitQuestionnaire}
              >
                <View 
                  style={{
                    flex:1,
                    alignItems:'center',
                    justifyContent:'center',
                    marginHorizontal: 10,
                  }}
                >
                  <LinearGradient
                    colors={['#ffba00', '#ffdd80', '#ffba00']}
                    style={styles.circle}
                  >
                    <Text style={{color: "black"}}>Submit</Text>
                  </LinearGradient>
                </View>
              </TouchableOpacity>
            </Animated.View>
          )
        }
      </ScrollView>
      <View style={styles.ops}>
        <ScrollView>
          {
            loading ? 
              (
                <ActivityIndicator size='large' color='#f20042'/>
              ):(
                <Animated.View style={[list.getLayout(), styles.list]}>
                  <TouchableOpacity 
                    onPress={()=>clickQ(0)}
                    style={styles.container}
                  >
                    {questionOpen[0] === true
                      ?<View style={{marginLeft:10}}>
                        <Text style={styles.question}>性別</Text>
                        <RadioButton.Group onValueChange={(value) => genderOnChange(value as any)} value={data.gender}>
                          <RadioButton.Item label="女" value="male" />
                          <RadioButton.Item label="男" value="female" />
                          <RadioButton.Item label="其他" value="其他" />
                        </RadioButton.Group>
                      </View>
                      :<></>
                    }
                  </TouchableOpacity>

                  <TouchableOpacity 
                    onPress={()=>clickQ(1)}
                    style={styles.container}
                  >
                    {questionOpen[1] === true
                    ?<View style={{marginLeft:10}}>
                      <Text style={styles.question}>出生日期</Text>
                      <View style={{marginTop: 30, marginLeft: 10, transform: [{translateY: -5}]}}>
                        <DateTimePickerComponent
                          date={new Date('2000-01-01')}
                          show={show}
                          onChange={onChange}
                          showMode={showMode}
                        />
                      </View>
                    </View>
                    :<></>
                    }
                  </TouchableOpacity>
                  
                  <TouchableOpacity 
                    onPress={()=>clickQ(2)}
                    style={styles.container}
                  >
                    {questionOpen[2] === true
                    ?<View style={{marginLeft:10}}>
                      <Text style={styles.question}>吸煙習慣</Text>
                      <RadioButton.Group onValueChange={(value) => smokeOnChange(value as any)} value={data.smoke}>
                        <RadioButton.Item label="常吸煙" value="常吸煙" />
                        <RadioButton.Item label="少吸煙" value="少吸煙" />
                        <RadioButton.Item label="唔吸煙" value="唔吸煙" />
                      </RadioButton.Group>
                    </View>
                    :<></>
                    }
                  </TouchableOpacity>
                  
                  <TouchableOpacity 
                    onPress={()=>clickQ(3)}
                    style={styles.container}
                  >
                    {questionOpen[3] === true
                    ?<View style={{marginLeft:10}}>
                      <Text style={styles.question}>飲酒習慣</Text>
                      <RadioButton.Group onValueChange={(value) => drinkOnChange(value as any)} value={data.drink}>
                        <RadioButton.Item label="常飲酒" value="常飲酒" />
                        <RadioButton.Item label="少飲酒" value="少飲酒" />
                        <RadioButton.Item label="唔飲酒" value="唔飲酒" />
                      </RadioButton.Group>
                    </View>
                    :<></>
                    }
                  </TouchableOpacity>
                  
                  <TouchableOpacity 
                    onPress={()=>clickQ(4)}
                    style={styles.container}
                  >
                    {questionOpen[4] === true
                    ?<View style={{marginLeft:10}}>
                      <Text style={styles.question}>揀4個或以上可以代表自己既TAG</Text>
                      <Text style={styles.text}>Hello, How are you</Text>
                    </View>
                    :<></>
                    }
                  </TouchableOpacity>
                  
                  <TouchableOpacity 
                    onPress={()=>clickQ(5)}
                    style={styles.container}
                  >
                    {questionOpen[5] === true
                    ?<View style={{marginLeft:10}}>
                      <Text style={styles.question}>揀4個或以上既TAG代表你想遇到既人</Text>
                      <Text style={styles.text}>Hello, How are you</Text>
                    </View>
                    :<></>
                    }
                  </TouchableOpacity>

                  <TouchableOpacity 
                    onPress={()=>clickQ(6)}
                    style={styles.container}
                  >
                    {questionOpen[6] === true
                    ?<View style={{marginLeft:10}}>
                      <Text style={styles.question}>用文字介紹下自己</Text>
                      <View style={styles.textAreaContainer}>
                        <TextInput
                          style={styles.textArea}
                          placeholder="自我介紹"
                          placeholderTextColor="grey"
                          onChangeText={(value)=>textDescriptionOnChange(value)}
                          multiline={true}
                          value={data.textDescription}
                        />
                      </View>
                    </View>
                    :<></>
                    }
                  </TouchableOpacity>

                  <View style={styles.btnContainer}>
                    <Button title="提交" onPress={() => submitQuestionnaire()} />
                  </View>
                </Animated.View>
          )}
        </ScrollView>
      </View>
    </LinearGradient>
   )
}

const styles = StyleSheet.create({
  list:{
      marginTop:300,
  },
  card:{
      marginLeft:400,
      width:400,
      flexDirection:'row'
  },
  gradient:{
      height:'100%',
      position:"absolute",
      left:0,
      right:0,
      top:0,
      paddingHorizontal:20,
      paddingTop:30
  },
  headerContainer:{
      flexDirection:'row',
      alignItems:'center'
  },
  header:{
      color:'#FFF',
      flex:1,
      fontSize:24
  },
  proContainer:{
      marginRight:-20,
      alignSelf:'center'
  },
  ops:{
      borderTopLeftRadius:40,
      borderTopRightRadius:40,
      height:500,
      backgroundColor:'#FFF',
      marginHorizontal:-20
  },
  col:{
      flexDirection:'row',
      marginTop:25,
      marginHorizontal:20,
      alignItems:'center'
  },
  day:{
      color:'#000119',
      flex:1,
      fontSize:20
  },
  container:{
    flexDirection:'row',
    paddingHorizontal:20,
    alignItems:'center',
    marginTop:30
  },
  circle:{
    height:60,
    width:60,
    borderRadius:30,
    alignItems:'center',
    justifyContent:'center',
    // marginRight:20,
    marginHorizontal: 10,
    padding: 5,
  },
  gradientStyle:{
    height:20,
    width:20,
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center',
    marginRight:20 
  },
  count:{
    color:'#fff',
  },
  image:{
    width:60,
    height:60,
    borderRadius:30
  },
  text:{
    color:'#b6b6b6',
    fontSize:11
  },
  duration:{
    color:'#000119',
    fontSize:12,
    flex:1,
    marginLeft:280,
    position:'absolute',
  },
  question:{
    color:'#000119',
    fontSize: 20,
  },
  avatarStyle:{
    width:60,
    height:60,
    borderRadius:30
  },
  textAreaContainer: {
    borderWidth: 1,
    padding: 5,
    borderRadius: 10,
    margin: 12,
    width: 300,
    height: 100,
  },
  textArea: {
    justifyContent: "flex-start",
    textAlignVertical: "top"
  },
  btnContainer: {
    backgroundColor: 'white',
    marginTop: 30,
    marginLeft: 150,
    marginRight: 150,
  },
})