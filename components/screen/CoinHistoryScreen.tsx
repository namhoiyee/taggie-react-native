/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  Animated,
  Button,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';
import {PlatformTouchable} from '../utils/AndroidComponent';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {Colors} from '../../utils/Colors';
import LoadingComponent from './Loading/LoadingComponent';
import {loadCoinRecordThunk} from '../../redux/coin/thunk';
import {useState} from 'react';
import {isAndroid} from '../../utils/platform';
import CrushScreen from './Crush/CrushScreen';

const SPACING = 10;
const AVATAR_SIZE = 48;
const ITEM_SIZE = AVATAR_SIZE + SPACING * 3;

export default function CoinHistoryScreen({navigation}: any) {
  const scrollY = React.useRef(new Animated.Value(0)).current;
  const loadingStatus = useSelector(
    (state: IRootState) => state.coinRecord.status,
  );
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedForModal, setSelectedForModal] = useState<number>(0);
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadCoinRecordThunk());
  }, [dispatch]);
  const data = useSelector((state: IRootState) => state.coinRecord?.coinRecord);
  console.log('data: ', data);

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.backgroundGray,
            marginBottom: tabBarHeight,
          }}>
          {
            <Animated.FlatList
              showsVerticalScrollIndicator={false}
              data={data}
              onScroll={Animated.event(
                [{nativeEvent: {contentOffset: {y: scrollY}}}],
                {useNativeDriver: true},
              )}
              keyExtractor={item => item.id + Math.random().toString()}
              contentContainerStyle={{
                padding: SPACING,
              }}
              renderItem={({item, index}) => {
                const inputRange = [
                  -1,
                  0,
                  ITEM_SIZE * index,
                  ITEM_SIZE * (index + 2),
                ];

                const opacityInputRange = [
                  -1,
                  0,
                  ITEM_SIZE * index,
                  ITEM_SIZE * (index + 1),
                ];

                const scale = scrollY.interpolate({
                  inputRange,
                  outputRange: [1, 1, 1, 0],
                });

                const opacity = scrollY.interpolate({
                  inputRange: opacityInputRange,
                  outputRange: [1, 1, 1, 0],
                });
                return (
                  <PlatformTouchable
                    onPress={() => {
                      setModalVisible(true);
                      setSelectedForModal(index);
                    }}
                    // onLongPress={()=>{
                    //   setModalVisible(true)
                    //   setSelectedForModal(index)
                    // }}
                  >
                    <Animated.View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        padding: SPACING,
                        marginBottom: SPACING,
                        backgroundColor: 'rgba(255,255,255, 0.8)',
                        borderRadius: 12,
                        shadowColor: '#000000',
                        shadowOffset: {
                          width: 0,
                          height: 10,
                        },
                        shadowOpacity: 0.3,
                        shadowRadius: 20,
                        elevation: 50,
                        opacity,
                        transform: [{scale}],
                        width: '100%',
                      }}>
                      <View style={{flex: 1}}>
                        <View
                          style={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            alignSelf: 'stretch',
                            width: '100%',
                          }}>
                          <View
                            style={{
                              height: 24,
                              flex: 5,
                              alignSelf: 'stretch',
                              marginHorizontal: 5,
                            }}>
                            <Text
                              style={{fontSize: 14, fontWeight: '700'}}
                              numberOfLines={1}>
                              項目:{' '}
                              {item.description === 'rematch'
                                ? '重新配對'
                                : item.description === 'daily'
                                ? '每日登入'
                                : item.description === 'initiation'
                                ? '初次登入'
                                : item.description === 'refund'
                                ? '退款 - 附近用戶不足十個'
                                : item.description}
                            </Text>
                          </View>
                          <View
                            style={{
                              height: 24,
                              flex: 3,
                              alignSelf: 'stretch',
                              marginHorizontal: 5,
                            }}>
                            <Text
                              style={{
                                fontSize: 14,
                                fontWeight: '700',
                                textAlign: 'right',
                              }}
                              numberOfLines={1}>
                              {item.transaction > 0
                                ? '+' + item.transaction
                                : item.transaction}{' '}
                              Taggie幣
                            </Text>
                          </View>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                          }}>
                          <View
                            style={{
                              height: 24,
                              flex: 5,
                              alignSelf: 'stretch',
                              marginHorizontal: 5,
                            }}>
                            <Text
                              style={{fontSize: 14, fontWeight: '700'}}
                              numberOfLines={1}>
                              來自:{' '}
                              {item.created_by === 'system' ? '系統' : '管理員'}
                            </Text>
                          </View>
                          <View
                            style={{
                              height: 24,
                              flex: 3,
                              alignSelf: 'stretch',
                              marginHorizontal: 5,
                            }}>
                            <Text
                              numberOfLines={1}
                              style={{
                                fontSize: 14,
                                opacity: 0.8,
                                color: '#0099cc',
                                textAlign: 'right',
                              }}>
                              {new Date(item.created_at)
                                .toISOString()
                                .slice(0, 10)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </Animated.View>
                  </PlatformTouchable>
                );
              }}
            />
          }
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(false);
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.7)',
              }}>
              <View style={styles.detailContent}>
                <ScrollView>
                  <View style={styles.row}>
                    <Text style={styles.cardTitle}>項目編號:</Text>
                    <Text style={styles.cardTitle}>
                      {data && data[selectedForModal].id}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.cardTitle}>項目:</Text>
                    <Text style={styles.cardTitle}>
                      {data && data[selectedForModal].description === 'rematch'
                        ? '重新配對'
                        : data && data[selectedForModal].description === 'daily'
                        ? '每日登入'
                        : data &&
                          data[selectedForModal].description === 'initiation'
                        ? '初次登入'
                        : data &&
                          data[selectedForModal].description === 'refund'
                        ? '退款 - 附近用戶不足十個'
                        : data && data[selectedForModal].description}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.cardTitle}>詳細:</Text>
                    <Text style={styles.cardTitle}>
                      {data && data[selectedForModal].transaction} Taggie幣
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.cardTitle}>來自:</Text>
                    <Text style={styles.cardTitle}>
                      {data && data[selectedForModal].created_by === 'system'
                        ? '系統'
                        : '管理員'}
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.cardTitle}>時間:</Text>
                    <Text style={styles.cardTitle}>
                      {data &&
                        data[selectedForModal].created_at &&
                        new Date(data[selectedForModal].created_at)
                          .toISOString()
                          .slice(0, 10) +
                          ' ' +
                          new Date(data[selectedForModal].created_at)
                            .toTimeString()
                            .slice(0, 8)}
                    </Text>
                  </View>
                  <View style={styles.btn}>
                    <Button
                      title={isAndroid ? '     確認     ' : '確認'}
                      // style={[styles.button, styles.buttonClose, {marginTop: 10}]}
                      onPress={() => setModalVisible(false)}
                    />
                  </View>
                </ScrollView>
              </View>
              {/* </View> */}
              {/* </TouchableWithoutFeedback> */}
            </View>
          </Modal>
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
}

const styles = StyleSheet.create({
  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  detailContent: {
    padding: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    width: '80%',
    borderRadius: 20,
  },
  cardTitle: {
    fontSize: 18,
    // fontWeight: 'bold',
    marginHorizontal: 10,
    maxWidth: 200,
  },
  row: {
    marginVertical: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btn: {
    marginVertical: 10,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
});
