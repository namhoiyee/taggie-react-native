export const /* { */ REACT_APP_API_SERVER /* } */ =
    // 'http://192.168.59.125:8200';
    'https://api.taggie.xyz';
// 'http://10.15.19.207:8100';
// 'http://192.168.59.114:8100';
// 'http://192.168.1.61:8100'; // kevin
// 'http://192.168.0.182:8100';
// 'http://192.168.59.139:8100';

export const S3Link = 'https://d16x2532cuenem.cloudfront.net/';
export const Google = 'AIzaSyAn1upjENBjxqyIS_Cwi_GU8tW5w0S3prI';

function patchUrl(url: string) {
  if (url.startsWith('/')) {
    url = REACT_APP_API_SERVER + url;
  }
  return url;
}

function DELETE(url: string, body?: object, token?: string) {
  url = patchUrl(url);
  return request('DELETE', url, token, body);
}

function get(url: string, token?: string) {
  url = patchUrl(url);
  return request('GET', url, token);
}

function request(method: string, url: string, token?: string, body?: object) {
  url = patchUrl(url);
  if (body === undefined) {
    return fetch(url, {
      method,
      headers: token
        ? {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          }
        : {
            'Content-Type': 'application/json',
          },
    });
  }
  if (body instanceof FormData) {
    console.log('multipart', body);

    return fetch(url, {
      method,
      headers: token
        ? {
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
          }
        : {
            'Content-Type': 'multipart/form-data',
          },
      body,
    });
  }
  return fetch(url, {
    method,
    headers: token
      ? {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        }
      : {
          'Content-Type': 'application/json',
        },
    body: JSON.stringify(body),
  });
}

function post(url: string, body?: object, token?: string) {
  return request('POST', url, token, body);
}

function patch(url: string, body?: object, token?: string) {
  return request('PATCH', url, token, body);
}

export const api = {
  delete: DELETE,
  get,
  post,
  patch,
};
