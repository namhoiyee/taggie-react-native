/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
// Example of Collapsible/Accordion/Expandable List View in React Native
// https://aboutreact.com/collapsible-accordion-expandable-view/

// import React in our code
import React, {useState} from 'react';

// import all the components we are going to use
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button,
  Alert,
} from 'react-native';
import {useAnimatedRef} from 'react-native-reanimated';

//import for the animation of Collapse and Expand
// import * as Animatable from 'react-native-animatable';

//import for the Accordion view
import Accordion from 'react-native-collapsible/Accordion';
import Tags, {Tag} from '../../model/tags';
import {Colors} from '../../utils/Colors';
import {isAndroid} from '../../utils/platform';

const headerWidthPerItem = 60;
const accordionHeaderHeightPerItem = 50;

const EditTagsScreen = () => {
  // Ddefault active selector
  const [activeSections, setActiveSections] = useState<number[]>([1]);
  // Collapsed condition for the single collapsible
  const scrollRef = useAnimatedRef<ScrollView>();
  const accordionScrollRef = useAnimatedRef<ScrollView>();

  const setSectionsByAccordion = (sections: Array<number>) => {
    //setting up a active section state
    setActiveSections(sections);
    // console.log(scrollRef.current);
    scrollRef?.current?.scrollTo({
      x: headerWidthPerItem * (sections[0] - 1),
      animated: true,
    });
  };

  const setSectionsByHeader = (section: number) => {
    //setting up a active section state
    setActiveSections([section]);
    // console.log(scrollRef.current);
    accordionScrollRef?.current?.scrollTo({
      y: accordionHeaderHeightPerItem * (section - 1),
      animated: true,
    });
  };

  const renderHeader = (section: Tag, _: any, isActive: boolean) => {
    //Accordion Header view
    return (
      <View
        // duration={400}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        /* transition="backgroundColor" */
      >
        <Text style={styles.headerText}>{section.category}</Text>
      </View>
    );
  };

  const renderContent = (section: Tag, _: any, isActive: boolean) => {
    //Accordion Content view
    return (
      <View
        // duration={400}
        style={[styles.content, isActive ? styles.active : styles.inactive]}
        /* transition="backgroundColor" */
      >
        <Button title="test" onPress={() => Alert.alert('test')} />
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
        <Text
          // animation={isActive ? 'bounceIn' : undefined}
          style={{textAlign: 'center'}}>
          {section.tag}
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.horizontalContainer}>
        <ScrollView
          ref={scrollRef}
          horizontal
          scrollEventThrottle={100}
          showsHorizontalScrollIndicator={false}
          // height={50}
          // style={styles.chipsScrollView}
          contentInset={{
            // iOS only
            top: 0,
            left: 0,
            bottom: 0,
            right: 20,
          }}
          contentContainerStyle={{
            // paddingLeft: isAndroid ? 0 : 20,
            paddingHorizontal: isAndroid ? 20 : 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {Tags.map(tag => {
            return (
              <View
                style={{
                  width: headerWidthPerItem,
                  paddingHorizontal: 10,
                }}
                key={tag.id}>
                <TouchableOpacity
                  onPress={() => setSectionsByHeader(tag.id)}
                  //on Press of any selector sending the selector value to
                  // setSections function which will expand the Accordion accordingly
                >
                  <Text
                    style={{
                      color: activeSections.includes(tag.id) ? 'red' : 'white',
                      textAlign: 'center',
                    }}>
                    {tag.category}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View style={styles.container}>
        <ScrollView ref={accordionScrollRef}>
          <Accordion
            keyExtractor={item => item.id}
            // renderAsFlatList
            activeSections={activeSections}
            //for any default active section
            sections={Tags}
            //title and content of accordion
            touchableComponent={TouchableOpacity}
            //which type of touchable component you want
            //It can be the following Touchables
            //TouchableHighlight, TouchableNativeFeedback
            //TouchableOpacity , TouchableWithoutFeedback
            // expandMultiple={multipleSelect}
            //Do you want to expand mutiple at a time or single at a time
            renderHeader={renderHeader}
            //Header Component(View) to render
            renderContent={renderContent}
            //Content Component(View) to render
            duration={400}
            //Duration for Collapse and expand
            onChange={setSectionsByAccordion}
            //setting the state of active sections
          />
          {/* <View style={{height: 1000}} /> */}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default EditTagsScreen;

const styles = StyleSheet.create({
  horizontalContainer: {
    height: 50,
    backgroundColor: Colors.mainBlue,
  },
  container: {
    flex: 1,
    // justifyContent: 'flex-start',
    backgroundColor: '#f4f4f4',
    // paddingTop: 30,
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#f4f4f4',
    padding: 10,
    height: 40,
    justifyContent: 'center',
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  content: {
    padding: 20,
    backgroundColor: '#f4f4f4',
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    // justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
    textAlign: 'center',
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});
