import React from 'react';
import {StyleSheet, View, Alert} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import TagComponent from './TagComponent';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';

export default function UserWishTagScreen() {
  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <ScrollView style={{marginBottom: tabBarHeight}}>
          <View style={styles.row}>
            <TagComponent
              onSelect={() => {
                Alert.alert('Edit');
              }}
              tagClass="運動"
              tags={['足球', '籃球']}
            />
            <TagComponent
              onSelect={() => {
                Alert.alert('Edit');
              }}
              tagClass="尋求"
              tags={['戀愛']}
            />
          </View>
          <View style={styles.row}>
            <TagComponent
              onSelect={() => {
                Alert.alert('Edit');
              }}
              tagClass="棋類"
              tags={['中國象棋']}
            />
            <TagComponent
              onSelect={() => {
                Alert.alert('Edit');
              }}
              tagClass="性別"
              tags={['男']}
            />
          </View>
          <View style={styles.row}>
            <TagComponent
              onSelect={() => {
                Alert.alert('Edit');
              }}
              tagClass="年齡"
              tags={['31-35']}
            />
            <TagComponent
              onSelect={() => {
                Alert.alert('Edit');
              }}
              tagClass="星座"
              tags={['雙子座']}
            />
          </View>
        </ScrollView>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
});
