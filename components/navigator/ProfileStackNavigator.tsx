/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {useTheme} from '@react-navigation/native';
import {View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createStackNavigator} from '@react-navigation/stack';
import ProfileScreen from '../screen/ProfileScreen';
import UserTagScreen from '../screen/UserTagScreen';
const ProfileStack = createStackNavigator();

export const ProfileStackNavigator = ({navigation}: any) => {
  const {colors} = useTheme();

  return (
    <ProfileStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#efefef',
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
      }}>
      <ProfileStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerRight: () => (
            <View
              style={{
                marginRight: 10,
              }}>
              <MaterialCommunityIcons.Button
                name="account-edit"
                size={25}
                backgroundColor={colors.background}
                color={colors.text}
                onPress={() => navigation.navigate('EditProfile')}
              />
            </View>
          ),
        }}
      />
      <ProfileStack.Screen
        name="UserTags"
        options={{
          title: '你的Tags',
          headerRight: () => (
            <View style={{marginRight: 10}}>
              <MaterialIcons.Button
                name="add"
                size={25}
                backgroundColor={colors.background}
                color={colors.text}
                onPress={() => {
                  navigation.navigate('EditUserTag', {
                    item: {
                      id: -1,
                      class: '',
                      tag: '',
                      classId: -1,
                      dbPrimaryId: -1,
                    },
                  });
                }}
              />
            </View>
          ),
        }}
        component={UserTagScreen}
      />
    </ProfileStack.Navigator>
  );
};
