/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MainPageScreen from '../screen/MainPageScreen';
import {isAndroid} from '../../utils/platform';
import {Colors} from '../../utils/Colors';
import {ProfileStackNavigator} from './ProfileStackNavigator';
import {UserWishTagStackNavigator} from './UserWishTagStackNavigator';
import {MatchHistoryStackNavigator} from './MatchHistoryStackNavigator';
import {ChatroomStackNavigator} from './ChatroomStackNavigatory';
import FastImage from 'react-native-fast-image';
import {useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';

const Tab = createBottomTabNavigator();

export default function MyTabsScreen() {
  const unreadCount = useSelector((state: IRootState) => state.chatroom.unread);
  console.log('unreadCount', unreadCount);
  const [chatroomNotification, setChatroomNotification] =
    useState<number | undefined>();

  useEffect(() => {
    if (unreadCount || unreadCount === 0) {
      console.log('unread count show', unreadCount);
      setChatroomNotification(unreadCount);
    }
  }, [unreadCount]);

  return (
    <Tab.Navigator
      initialRouteName="Main"
      tabBarOptions={{
        activeTintColor: Colors.mainBlue,
        inactiveTintColor: 'rgba(144, 144, 144, 1)',
        showLabel: false,
        style: styles.tabContainer,
      }}
      backBehavior="history">
      <Tab.Screen
        name="MatchHistoryStack"
        component={MatchHistoryStackNavigator}
        options={{
          tabBarIcon: ({focused, color}) => (
            <MaterialIcons
              name="favorite"
              color={color}
              size={focused ? 35 : 30}
              style={{marginBottom: isAndroid ? 0 : -15}}
            />
          ),
          tabBarButton: props => <TouchableOpacity {...props} />,
        }}
      />
      <Tab.Screen
        name="UserWishTagsStack"
        component={UserWishTagStackNavigator}
        options={{
          tabBarIcon: ({focused, color}) => (
            <MaterialIcons
              name="loyalty"
              color={color}
              size={focused ? 35 : 30}
              style={{marginBottom: isAndroid ? 0 : -15}}
            />
          ),
          tabBarButton: props => <TouchableOpacity {...props} />,
        }}
      />
      <Tab.Screen
        name="Main"
        component={MainPageScreen}
        options={{
          tabBarIcon: () => (
            <FastImage
              source={require('../../src/image/ColorHome.png')}
              style={{height: 50, width: 50, marginBottom: isAndroid ? 0 : -20}}
            />
          ),
          tabBarButton: props => <TouchableOpacity {...props} />,
        }}
      />
      <Tab.Screen
        name="ChatroomStack"
        component={ChatroomStackNavigator}
        options={{
          tabBarIcon: ({focused, color}) => (
            <Ionicons
              name="chatbubbles"
              color={color}
              size={focused ? 35 : 30}
              style={{marginBottom: isAndroid ? 0 : -15}}
            />
          ),
          tabBarBadge:
            chatroomNotification && chatroomNotification > 0
              ? chatroomNotification
              : undefined,
          tabBarButton: props => <TouchableOpacity {...props} />,
        }}
      />
      <Tab.Screen
        name="UserSettingStack"
        component={ProfileStackNavigator}
        options={{
          tabBarIcon: ({focused, color}) => (
            <Ionicons
              name="person"
              color={color}
              size={focused ? 35 : 30}
              style={{marginBottom: isAndroid ? 0 : -15}}
            />
          ),
          tabBarButton: props => <TouchableOpacity {...props} />,
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    position: 'absolute',
    minHeight: isAndroid ? 70 : 0,
    maxHeight: 100,
    elevation: isAndroid ? 100 : 0,
    backgroundColor: 'rgba(0,0,0, 0.2)',
    borderTopColor: isAndroid ? 'transparent' : 'rgba(62, 36, 101, 0.1)',
    borderTopWidth: isAndroid ? 0 : 1,
  },
});
