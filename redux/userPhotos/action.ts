import {UserGallery} from '../../helpers/types';

export function loadedUserPhotos(userGallery: UserGallery) {
  return {
    type: '@@User_photos/loadedUserPhotos' as const,
    userGallery,
  };
}

export function loading() {
  return {
    type: '@@User_photos/loading' as const,
  };
}

export function failed(msg: string) {
  console.log('failed insert gallery', msg);
  return {
    type: '@@User_photos/failed' as const,
    msg,
  };
}

export function insertedUserPhotos(userGallery: UserGallery) {
  return {
    type: '@@User_photos/insertedUserPhotos' as const,
    userGallery,
  };
}

export function deletedUserPhotos(userGallery: UserGallery) {
  return {
    type: '@@User_photos/deletedUserPhotos' as const,
    userGallery,
  };
}

export type UserPhotosAction =
  | ReturnType<typeof loadedUserPhotos>
  | ReturnType<typeof insertedUserPhotos>
  | ReturnType<typeof deletedUserPhotos>
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>;
