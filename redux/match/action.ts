import {ILocation, MatchResult} from '../../helpers/types';

export function located(userLocation: ILocation) {
  return {
    type: '@@Match/init' as const,
    userLocation,
  };
}

export function matched(matchResult: MatchResult) {
  return {
    type: '@@Match/matched' as const,
    matchResult,
  };
}

export function updatedMatchResult(matchResult: MatchResult) {
  return {
    type: '@@Match/updatedMatchResult' as const,
    matchResult,
  };
}

export function checkedIfAfter15MinsFromLastMatch(isAfter15Mins: boolean) {
  return {
    type: '@@Match/checkedIfAfter15MinsFromLastMatch' as const,
    isAfter15Mins,
  };
}

export function gotUserPermission(userPermitted: boolean) {
  return {
    type: '@@Match/user-permission' as const,
    userPermitted,
  };
}

export function loading() {
  return {
    type: '@@Match/loading' as const,
  };
}

export function failed(msg: string) {
  console.log('match', msg);
  return {
    type: '@@Match/failed' as const,
    msg,
  };
}

export function insertedGeolocation(time: Date) {
  return {
    type: '@@Match/inserted' as const,
    time,
  };
}

// likeStatus no use currently
export function likedPeople(likeStatus: any[]) {
  return {
    type: '@@Match/likedPeople' as const,
    likeStatus,
  };
}

// likeStatus no use currently
export function unlikedPeople(likeStatus: any[]) {
  return {
    type: '@@Match/unlikedPeople' as const,
    likeStatus,
  };
}

export type MatchAction =
  | ReturnType<typeof located>
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>
  | ReturnType<typeof likedPeople>
  | ReturnType<typeof unlikedPeople>
  | ReturnType<typeof checkedIfAfter15MinsFromLastMatch>
  | ReturnType<typeof gotUserPermission>
  | ReturnType<typeof insertedGeolocation>
  | ReturnType<typeof updatedMatchResult>
  | ReturnType<typeof matched>;
