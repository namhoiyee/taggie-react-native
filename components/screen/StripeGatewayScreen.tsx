/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Dimensions,
  ScrollView,
  SafeAreaView,
} from 'react-native';
//@ts-ignore
import {CreditCardInput} from 'react-native-credit-card-input';
import {Title} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {/* cancel, */ resetStripe} from '../../redux/stripe/action';
import {payment} from '../../redux/stripe/thunk';
import {Colors} from '../../utils/Colors';
import LoadingComponent from './Loading/LoadingComponent';

const {width} = Dimensions.get('window');

const StripeGatewayScreen = ({navigation}: any) => {
  const [CardInput, setCardInput] = React.useState({valid: false});
  const coinsOptions = [10, 20, 50];
  const [CoinsAmount, setCoinsAmount] = React.useState(coinsOptions[0]);
  const balance = useSelector(
    (state: IRootState) => state.userProfile.userProfile?.info.coins,
  );

  const dispatch = useDispatch();
  const status = useSelector((state: IRootState) => state.stripe.status);
  if (status === 'loading') {
    return <LoadingComponent />;
  } /* else if (status === 'cancel') {
    dispatch(resetStripe());
    navigation.goBack();
  }  */

  const onSubmit = async () => {
    Alert.alert(`確認購買${CoinsAmount}枚Taggie幣`, `總值: $${CoinsAmount}`, [
      {
        text: '取消',
        onPress: () => {
          dispatch(resetStripe());
        },
        style: 'cancel',
      },
      {
        text: '確認',
        onPress: () => {
          dispatch(payment(CardInput, CoinsAmount, navigation));
        },
      },
    ]);
  };

  const _onChange = (data: any) => {
    setCardInput(data);
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.backgroundGray}}>
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={styles.container}>
          <Title
            style={{
              marginTop: 30,
              marginHorizontal: 30,
              fontWeight: 'bold',
            }}>
            你現在有{balance} 枚Taggie幣
          </Title>
          <Title
            style={{
              marginTop: 10,
              marginHorizontal: 30,
              fontWeight: 'bold',
            }}>
            想加購多少枚?
          </Title>
          <View style={styles.buttonGroup}>
            {coinsOptions.map(amt => {
              return (
                <TouchableOpacity
                  onPress={() => setCoinsAmount(amt)}
                  style={
                    CoinsAmount === amt
                      ? styles.buttonItem
                      : styles.buttonNotChosen
                  }>
                  <Text style={styles.buttonText}> {amt}枚 </Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <Title
            style={{
              marginTop: 10,
              marginHorizontal: 30,
              fontWeight: 'bold',
              marginBottom: 30,
            }}>
            金額: $ {CoinsAmount}.00
          </Title>

          {/* <Image
        source={{
          uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/e/eb/Stripe_logo%2C_revised_2016.png/1200px-Stripe_logo%2C_revised_2016.png',
        }}
        style={styles.ImgStyle}
      /> */}
          <CreditCardInput
            inputContainerStyle={styles.inputContainerStyle}
            inputStyle={styles.inputStyle}
            labelStyle={styles.labelStyle}
            placeholderColor="#ccc"
            onChange={_onChange}
          />

          <TouchableOpacity
            disabled={!CardInput.valid}
            onPress={onSubmit}
            style={
              CardInput.valid
                ? styles.button
                : {...styles.button, backgroundColor: '#888888'}
            }>
            <Text style={styles.buttonText}>
              {' '}
              {CardInput.valid ? 'Pay Now' : 'Invalid card'}{' '}
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundGray,
  },
  button: {
    backgroundColor: '#2471A3',
    width: 150,
    height: 60,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    borderRadius: 5,
  },
  buttonItem: {
    backgroundColor: '#00C6A7',
    width: 0.25 * width,
    height: 45,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 5,
    margin: 10,
  },
  buttonNotChosen: {
    backgroundColor: '#888888',
    width: 0.25 * width,
    height: 45,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 5,
    margin: 10,
  },
  buttonText: {
    fontSize: 15,
    color: '#f4f4f4',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  inputContainerStyle: {
    backgroundColor: '#fff',
    borderRadius: 5,
  },
  inputStyle: {
    borderBottomColor: '#222242',
    borderBottomWidth: 1,
    paddingLeft: 15,
    borderRadius: 5,
  },
  labelStyle: {
    marginBottom: 5,
    fontSize: 12,
  },
  buttonGroup: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 30,
  },
});

//make this component available to the app
export default StripeGatewayScreen;
