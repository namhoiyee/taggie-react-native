import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import store from '../../redux/store';
import {Provider} from 'react-redux';
import MyStackNavigator from './MyStackScreen';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {loadToken, logout as logoutAction} from '../../redux/auth/action';
import LandingNavigator from './LandingNavigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoadingComponent from '../screen/Loading/LoadingComponent';

const AppNavigator = () => {
  const user = useSelector((state: IRootState) => state.auth.user);
  const authLoadingStatus = useSelector(
    (state: IRootState) => state.auth.status,
  );
  const dispatch = useDispatch();

  console.log('checking user', user);

  useEffect(() => {
    AsyncStorage.getItem('token')
      .then(token => {
        if (token) {
          dispatch(loadToken(token));
        } else {
          dispatch(logoutAction());
        }
      })
      .catch(e => {
        console.log(e);
        dispatch(logoutAction());
      });
  }, [dispatch]);

  if (authLoadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <>
      <Provider store={store}>
        <StatusBar backgroundColor="#efefef" barStyle="dark-content" />
        <NavigationContainer>
          {user ? <MyStackNavigator /> : <LandingNavigator />}
        </NavigationContainer>
      </Provider>
    </>
  );
};

export default AppNavigator;
