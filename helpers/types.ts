export type ImageUpload = {
  uri: string;
  name: string;
  type: string;
};

export type UserGallery = {
  data: {
    id: number;
    url: string;
  }[];
};

export type Photo = {
  id: number;
  url: string;
};

export type UserWishTags = {
  data: {
    tagCount: number;
    wishTags: Tag[];
  };
};

export type UserTags = {
  data: {
    tagCount: number;
    userTags: Tag[];
  };
};

export type TagClass = {
  id: number;
  class: string;
  tags: Tag[];
};

export type Tag = {
  id: number;
  class: string;
  tag: string;
  classId: number;
  dbPrimaryId: number;
};

export type JWTPayload = {
  id: number;
  registration_status: number;
  lang: 'zh' | 'en';
  username?: string;
  profilePic?: string;
};

export type LoginInput = {
  email: string;
  password: string;
  lang: 'zh';
};

export type RegistrationInput = {
  username: string;
  email: string;
  password: string;
  lang: 'zh';
};
export interface ILocation {
  latitude: number;
  longitude: number;
}

export type MatchedUserDetail = {
  id: number;
  matchedTags: string[];
  userAge: number;
  user_tags: string[];
  username: string;
  gallery: {url: string}[];
  profilePic: {url: string};
  text_description: string;
}[];

export type MatchResult = {
  matchedTime: string;
  otherUserDetail: {
    agePreferencePoint: number;
    gallery: {url: string}[];
    gender: number;
    horoscopeMatched: number;
    id: number;
    matchedPercent: number;
    matchedTags: string[];
    randomPart: number;
    userAge: number;
    user_tags: string[];
    username: string;
    profilePic: {url: string};
    text_description: string;
  }[];
  userLocation: {x: number; y: number};
};

export type pointLocation = {
  x: number;
  y: number;
};
export interface MarkerInfo {
  coordinate: ILocation;
  username: string;
  profilePic: string;
  originalPic: string;
  userId: number;
}

export interface Region {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}

export const genders = [
  {pic: 'gender-female', DB: 'female'},
  {pic: 'gender-male', DB: 'male'},
  {pic: 'gender-male-female', DB: '其他'},
];

export const imageReq = {
  運動: require('../src/image/tags/sports.jpg'),
  棋類: require('../src/image/tags/chess.jpg'),
  電影類別: require('../src/image/tags/film.jpg'),
  宗教: require('../src/image/tags/religion.jpg'),
  菜式: require('../src/image/tags/dishes.jpg'),
  玩樂: require('../src/image/tags/daily-entertainment.jpg'),
  文學: require('../src/image/tags/novel.jpg'),
  日常娛樂: require('../src/image/tags/entertainment.jpg'),
  學術: require('../src/image/tags/academic.jpg'),
  年齡: require('../src/image/tags/age.jpg'),
  性別: require('../src/image/tags/gender.jpg'),
  星座: require('../src/image/tags/constellations.jpg'),
  習慣1: require('../src/image/tags/smoke.jpg'),
  習慣2: require('../src/image/tags/drink.jpg'),
  尋求: require('../src/image/tags/relationship.jpg'),
};

export const placeImageReq = {
  游泳: require('../src/image/place/beach.jpg'),
  電子競技: require('../src/image/place/netcafe.jpg'),
  日本菜: require('../src/image/place/japanese.jpg'),
  中菜: require('../src/image/place/chinese.jpg'),
  西餐: require('../src/image/place/western.jpg'),
  韓國菜: require('../src/image/place/korean.jpg'),
  泰越菜: require('../src/image/place/thai.jpg'),
  台灣菜: require('../src/image/place/taiwanese.jpg'),
  印度菜: require('../src/image/place/indian.jpg'),
  意大利菜: require('../src/image/place/italian.jpg'),
  墨西哥菜: require('../src/image/place/mexican.jpg'),
  運動: require('../src/image/place/park.jpg'),
  棋類: require('../src/image/place/boardgame.jpg'),
  電影類別: require('../src/image/place/cinema.jpg'),
  菜式: require('../src/image/place/coffee.jpg'),
  文學: require('../src/image/place/bookstore.jpg'),
  學術: require('../src/image/place/museum.jpg'),
};

export type CoinRecord = {
  id: number;
  transaction: number;
  description: string;
  created_by: string;
  created_at: Date;
};

export type FriendProfile = {
  gallery: {url: string}[];
  gender: number;
  id: number;
  userAge: number;
  user_tags: {tag: string}[];
  matchedTags: {tag: string}[];
  username: string;
  profilePic: {url: string};
};

export const TagEmoji = {
  運動: '⚽',
  棋類: '♟️',
  電影類別: '🎬',
  宗教: '☮️',
  菜式: '🍽️',
  玩樂: '🤼‍♀️',
  文學: '📖',
  日常娛樂: '🎤',
  學術: '📝',
  年齡: '👨‍👩‍👧‍👦',
  性別: '👩🏻‍🤝‍👨🏼',
  星座: '☪️',
  習慣1: '🚬',
  習慣2: '🍻',
  尋求: '💟'
}

export const TagsEmoji = {
  足球: '⚽',
  籃球: '🏀',
  排球: '🏐',
  網球: '🎾',
  保齡球: '🎳',
  高爾夫球: '⛳',
  羽毛球: '🏸',
  桌球: '🎱',
  射箭: '🏹',
  衝浪: '🏄🏻‍♀️',
  游泳: '🏊🏻‍♀️',
  划艇: '🚣‍♀️',
  跑步: '🏃🏻‍♀️',
  電子競技: '🎮',
  攀岩: '🧗‍♀️',
  滑雪: '⛷️',
  滑翔: '🪂',
  滑浪風帆: '⛵',
  單車: '🚴‍♀️',
  馬術: '🏇',
  格鬥技: '🥊',
  健身: '💪',
  野外定向: '🏕️',
  中國象棋: '♟️',
  國際象棋: '♟️',
  圍棋: '♟️',
  麻將: '🀄',
  大富翁: '🎲',
  其他桌遊: '♟️',
  冒險片: '🎬',
  災難片: '🎬',
  科幻片: '🎬',
  恐怖片: '🎬',
  愛情片: '🎬',
  喜劇片: '🎬',
  劇情片: '🎬',
  奇幻片: '🎬',
  玄幻片: '🎬',
  超級英雄片: '🎬',
  '間諜/特工片': '🎬',
  '動作/武打片': '🎬',
  勵志片: '🎬',
  歌舞片: '🎬',
  "動畫/卡通片": '🎬',
  紀錄片: '🎬',
  傳記片: '🎬',
  戰爭片: '🎬',
  道教: '☮️',
  佛教: '☮️',
  伊斯蘭教: '☮️',
  基督教: '☮️',
  天主教: '☮️',
  日本菜: '🍽️',
  中菜: '🍽️',
  西餐: '🍽️',
  韓國菜: '🍽️',
  泰越菜: '🍽️',
  星馬菜: '🍽️',
  法國菜: '🍽️',
  台灣菜: '🍽️',
  印度菜: '🍽️',
  意大利菜: '🍽️',
  西班牙菜: '🍽️',
  美國菜: '🍽️',
  中東菜: '🍽️',
  墨西哥菜: '🍽️',
  港式: '🍽️',
  LARP: '🤼‍♀️',
  VR體驗: '🤼‍♀️',
  密室逃脫: '🤼‍♀️',
  狼人殺: '🤼‍♀️',
  愛情文學: '📖',
  冒險文學: '📖',
  科幻文學: '📖',
  恐怖文學: '📖',
  劇情文學: '📖',
  奇幻文學: '📖',
  玄幻文學: '📖',
  笑話: '📖',
  穿越文學: '📖',
  輕小說文學: '📖',
  武俠文學: '📖',
  推理文學: '📖',
  散文: '📖',
  唱K: '🎤',
  習字: '🎤',
  釣魚: '🎣',
  滑板: '🎤',
  跳舞: '💃',
  畫畫: '🎨',
  行山: '🎤',
  攝影: '🎤',
  煮飯仔: '🎤',
  行街: '🎤',
  語文: '📝',
  哲學: '📝',
  數學: '📝',
  地理: '📝',
  天文: '📝',
  "18-21": '👨‍👩‍👧‍👦',
  "22-25": '👨‍👩‍👧‍👦',
  "26-30": '👨‍👩‍👧‍👦',
  "31-35": '👨‍👩‍👧‍👦',
  "36-40": '👨‍👩‍👧‍👦',
  "41-50": '👨‍👩‍👧‍👦',
  "51-60": '👨‍👩‍👧‍👦',
  "60+": '👨‍👩‍👧‍👦',
  male: '👨',
  female: '👩',
  其他: '⚤',
  牡羊座: '♈',
  金牛座: '♉',
  巨蟹座: '♋',
  獅子座: '♌',
  處女座: '♍',
  雙子座: '♊',
  天秤座: '♎',
  天蠍座: '♏',
  射手座: '♐',
  魔羯座: '♑',
  水瓶座: '♒',
  雙魚座: '♓',
  常吸煙: '🚬',
  少吸煙: '🚬',
  唔吸煙: '🚭',
  常飲酒: '🍻',
  少飲酒: '🍺',
  唔飲酒: '🥛',
  尋覓戀愛: '💟',
  認識朋友: '🤝',
  尋覓婚姻: '💍',
}