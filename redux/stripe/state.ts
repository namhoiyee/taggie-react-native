export type StripeState =
  | {
      status: 'loading';
      msg?: string;
    }
  | {
      status: 'error';
      msg: string;
    }
  | {
      status: 'ready';
      msg?: string;
    }
  | {
      status: 'successful';
      msg?: string;
    }
  | {
      status: 'cancel';
    };

export const initialState: StripeState = {
  status: 'ready',
};
