/* eslint-disable react-native/no-inline-styles */
import React, {useState, useCallback, useLayoutEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {CalendarList} from 'react-native-calendars';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {Colors} from '../../../utils/Colors';
import {useDispatch} from 'react-redux';
import {recordSchedule} from '../../../redux/schedule/thunk';
import {translateTime} from '../../utils/calculateTime';

function SchedulingScreen({navigation, route}: any) {
  const [selectedDay, setSelectedDay] = useState({
    [new Date().toISOString().substring(0, 10)]: {
      selected: true,
      selectedColor: '#2E66E7',
    },
  });
  const [currentDay, setCurrentDay] = useState(
    new Date().toISOString().substring(0, 10),
  );
  const [notesText, setNotesText] = useState('');
  const [timeSelected, setTimeSelected] = useState('請選擇');
  const [timePicked, setTimePicked] = useState(false);
  const [scheduleTimestamp, setScheduleTimeStamp] = useState<Date | null>(null);

  const dispatch = useDispatch();

  useLayoutEffect(() => {
    navigation.setOptions({
      title: '見面吧',
      headerBackTitleVisible: false,
      headerRight: () => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: 80,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ScheduleDating');
            }}>
            <View>
              <Icon name="calendar-check" color="#FF6347" size={25} />
            </View>
          </TouchableOpacity>
        </View>
      ),
    });
  }, [navigation]);

  const showDatePicker = useCallback(() => {
    setTimePicked(true);
  }, []);

  const hideDatePicker = useCallback(() => {
    setTimePicked(false);
  }, []);

  const handleConfirm = useCallback(
    (date: any) => {
      setScheduleTimeStamp(date);
      const localeTime = new Date(date).toLocaleTimeString('en-US');
      const translatedTime = translateTime(date);

      console.warn(
        'A date has been picked: ',
        localeTime.substring(0, 5) + ' ' + localeTime.slice(-2),
      );
      setTimeSelected(translatedTime);
      hideDatePicker();
    },
    [hideDatePicker],
  );

  const submitSchedule = useCallback(() => {
    if (timeSelected === '請選擇') {
      Alert.alert('輸入錯誤', '請選擇時間', [{text: '確認', style: 'default'}]);
      return;
    }
    if (scheduleTimestamp === null) {
      return;
    }
    let timezone = (scheduleTimestamp?.getTimezoneOffset() as any) / -60;
    let schedule_date: string;
    if (timezone >= 0) {
      schedule_date = currentDay + ' ' + timeSelected + ' +' + timezone;
    } else {
      schedule_date = currentDay + ' ' + timeSelected + ' ' + timezone;
    }
    console.log(
      new Date(Date.now() + 1000 * 60 * 60 * 24).toISOString().substring(0, 10),
    );
    console.log(
      'check schedule info',
      !route.params?.room_id,
      !route.params?.place_id,
      !route.params?.location,
      !route.params?.address,
      !route.params?.rating,
      !route.params?.address,
      !route.params?.rating,
      !route.params?.venue,
      route.params?.searchKey,
      !notesText,
      !schedule_date,
    );
    dispatch(
      recordSchedule({
        room_id: route.params?.room_id,
        place_id: route.params?.place_id,
        location: route.params?.location,
        vicinity: route.params?.address,
        rating: route.params?.rating,
        name: route.params?.venue,
        description: notesText,
        schedule_date: schedule_date,
        searchKey: route.params?.searchKey,
      }),
    );
    navigation.pop(2);
  }, [
    timeSelected,
    scheduleTimestamp,
    route.params?.room_id,
    route.params?.place_id,
    route.params?.location,
    route.params?.address,
    route.params?.rating,
    route.params?.venue,
    route.params?.searchKey,
    notesText,
    dispatch,
    navigation,
    currentDay,
  ]);

  return (
    <SafeAreaView style={styles.container}>
      <>
        <View>
          <DateTimePicker
            isVisible={timePicked}
            mode="time"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />
        </View>
        <View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              paddingBottom: 100,
            }}>
            <View style={styles.calenderContainer}>
              <CalendarList
                style={{
                  width: 350,
                  height: 350,
                }}
                current={new Date(Date.now() + 1000 * 60 * 60 * 24)
                  .toISOString()
                  .substring(0, 10)}
                minDate={new Date(Date.now() + 1000 * 60 * 60 * 24)
                  .toISOString()
                  .substring(0, 10)}
                horizontal
                pastScrollRange={0}
                pagingEnabled
                calendarWidth={350}
                onDayPress={day => {
                  setSelectedDay({
                    [day.dateString]: {
                      selected: true,
                      selectedColor: '#2E66E7',
                    },
                  });
                  setCurrentDay(day.dateString);
                  console.log('faye', new Date(day.timestamp));
                }}
                monthFormat="yyyy MMMM"
                hideArrows={false}
                theme={{
                  arrowColor: '#333333',
                  selectedDayBackgroundColor: '#2E66E7',
                  selectedDayTextColor: '#ffffff',
                  todayTextColor: '#2E66E7',
                  backgroundColor: Colors.backgroundGray,
                  calendarBackground: Colors.backgroundGray,
                  textDisabledColor: '#d9dbe0',
                }}
                markedDates={selectedDay}
              />
            </View>
            <View style={styles.taskContainer}>
              <View>
                <Text style={styles.notes}>地點</Text>
                <Text
                  style={{
                    // height: 25,
                    fontSize: 19,
                    marginTop: 10,
                  }}>
                  {route.params?.venue}
                </Text>
              </View>
              <View style={styles.notesContent} />
              <View>
                <Text style={styles.notes}>地址</Text>
                <Text
                  style={{
                    // height: 25,
                    fontSize: 19,
                    marginTop: 10,
                  }}>
                  {route.params?.address}
                </Text>
              </View>
              <View style={styles.notesContent} />
              <View>
                <Text style={styles.notes}>種類</Text>
                <Text
                  style={{
                    // height: 25,
                    fontSize: 19,
                    marginTop: 10,
                  }}>
                  {route.params?.detail}
                </Text>
              </View>
              <View style={styles.notesContent} />
              <View>
                <Text
                  style={{
                    color: '#9CAAC4',
                    fontSize: 16,
                    fontWeight: '600',
                  }}>
                  時間
                </Text>
                <TouchableOpacity
                  onPress={showDatePicker}
                  style={{
                    height: 25,
                    marginTop: 10,
                  }}>
                  <Text style={{fontSize: 19}}>{timeSelected}</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.notesContent} />
              <View>
                <Text
                  style={{
                    color: '#9CAAC4',
                    fontSize: 16,
                    fontWeight: '600',
                  }}>
                  日期
                </Text>
                <Text
                  style={{
                    // height: 25,
                    fontSize: 19,
                    marginTop: 10,
                  }}>
                  {currentDay}
                </Text>
              </View>
              <View style={styles.notesContent} />
              <View>
                <Text style={styles.notes}>信息</Text>
                <TextInput
                  style={{
                    minHeight: 25,
                    fontSize: 19,
                    marginTop: 10,
                    color: 'black',
                  }}
                  onChangeText={text => setNotesText(text)}
                  value={notesText}
                  placeholder="可詳細描述見面內容"
                />
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.createTaskButton,
                {
                  backgroundColor: /* 'rgba(46, 102, 231,0.5)' :  */ '#2E66E7',
                },
              ]}
              onPress={() => {
                submitSchedule();
              }}>
              <Text
                style={{
                  fontSize: 18,
                  textAlign: 'center',
                  color: '#fff',
                }}>
                邀請他/她吧！
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </>
    </SafeAreaView>
  );
}

export default SchedulingScreen;

const styles = StyleSheet.create({
  createTaskButton: {
    width: '80%',
    height: 48,
    alignSelf: 'center',
    marginTop: 40,
    borderRadius: 20,
    justifyContent: 'center',
  },
  separator: {
    height: 0.5,
    width: '100%',
    backgroundColor: '#979797',
    alignSelf: 'center',
    marginVertical: 20,
  },
  notes: {
    color: '#9CAAC4',
    fontSize: 16,
    fontWeight: '600',
  },
  notesContent: {
    height: 1,
    width: '100%',
    backgroundColor: '#979797',
    alignSelf: 'center',
    marginVertical: 20,
  },
  learn: {
    height: 23,
    width: 51,
    backgroundColor: '#F8D557',
    justifyContent: 'center',
    borderRadius: 5,
  },
  design: {
    height: 23,
    width: 59,
    backgroundColor: '#62CCFB',
    justifyContent: 'center',
    borderRadius: 5,
    marginRight: 7,
  },
  readBook: {
    height: 23,
    width: 83,
    backgroundColor: '#4CD565',
    justifyContent: 'center',
    borderRadius: 5,
    marginRight: 7,
  },
  title: {
    height: 25,
    borderColor: '#5DD976',
    borderLeftWidth: 1,
    paddingLeft: 8,
    fontSize: 19,
  },
  taskContainer: {
    // height: 400,
    width: '80%',
    alignSelf: 'center',
    borderRadius: 20,
    shadowColor: '#2E66E7',
    backgroundColor: '#ffffff',
    shadowOffset: {
      width: 3,
      height: 3,
    },
    shadowRadius: 20,
    shadowOpacity: 0.2,
    elevation: 5,
    padding: 22,
  },
  calenderContainer: {
    marginTop: 30,
    width: 350,
    height: 350,
    alignSelf: 'center',
  },
  newTask: {
    alignSelf: 'center',
    fontSize: 20,
    width: 120,
    height: 25,
    textAlign: 'center',
  },
  backButton: {
    flexDirection: 'row',
    marginTop: 60,
    width: '100%',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundGray,
  },
});
