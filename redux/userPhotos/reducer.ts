import {UserPhotosState, initialState} from './state';
import {UserPhotosAction} from './action';

export const UserPhotosReducer = (
  state: UserPhotosState = initialState,
  action: UserPhotosAction,
): UserPhotosState => {
  switch (action.type) {
    case '@@User_photos/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@User_photos/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@User_photos/loadedUserPhotos': {
      return {
        ...state,
        status: 'loadedPhotos',
        userGallery: action.userGallery,
      };
    }
    case '@@User_photos/insertedUserPhotos': {
      return {
        ...state,
        status: 'insertedPhotos',
        userGallery: action.userGallery,
      };
    }
    case '@@User_photos/deletedUserPhotos': {
      return {
        ...state,
        status: 'deletedPhotos',
        userGallery: action.userGallery,
      };
    }
    default:
      return state;
  }
};
