/* eslint-disable react-native/no-inline-styles */
import React from 'react';
// import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Alert, ScrollView} from 'react-native';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';

import {MARGIN} from '../drag/Config';
import Tile from '../drag/DragTile';
import SortableList from '../drag/SortableList';

const tiles = [
  {
    id: 'google',
    uri: 'https://google.com',
  },

  {
    id: 'expo',
    uri: 'https://expo.io',
  },
  {
    id: 'facebook',
    uri: 'https://facebook.com',
  },
  {
    id: 'reanimated',
    uri: 'https://docs.swmansion.com/react-native-reanimated/',
  },
  {
    id: 'github',
    uri: 'https://github.com',
  },
  {
    id: 'rnnavigation',
    uri: 'https://reactnavigation.org/',
  },
  {
    id: 'youtube',
    uri: 'https://youtube.com',
  },
  {
    id: 'twitter',
    uri: 'https://twitter.com',
  },
];

const Chrome = () => {
  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        // <SafeAreaProvider>
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: 'black',
            paddingHorizontal: MARGIN,
            marginBottom: tabBarHeight,
          }}>
          <SortableList
            editing={true}
            onDragEnd={positions => {
              console.log(JSON.stringify(positions, null, 2));
            }}>
            {tiles.map((tile, index) => (
              <Tile
                onLongPress={() => {
                  Alert.alert('hey');
                }}
                key={tile.id + '-' + index}
                id={tile.id + '-' + index}
                uri={tile.uri}
              />
            ))}
          </SortableList>
        </ScrollView>
        // </SafeAreaProvider>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
};

export default Chrome;
