import {crushHappened, crushHappenHandled, failed} from './action';
import {IRootThunkDispatch} from '../store';
import {CrushFriendInfo} from './state';

export function crushHappen(friendInfo: CrushFriendInfo) {
  return async (dispatch: IRootThunkDispatch) => {
    try {
      console.log('friendInfo', friendInfo);
      dispatch(crushHappened(friendInfo));
      // console.log('insert location', res.status, message.data.created_at);
    } catch (e) {
      dispatch(failed('failed to happen crush ' + e.msg + e.message));
      // console.log('insert location failed at first', e);
    }
  };
}

export function crushHappenHandle() {
  return async (dispatch: IRootThunkDispatch) => {
    try {
      dispatch(crushHappenHandled());
      // console.log('insert location', res.status, message.data.created_at);
    } catch (e) {
      dispatch(failed('failed to happen crush ' + e.msg + e.message));
      // console.log('insert location failed at first', e);
    }
  };
}
