import React from 'react';
import {StyleSheet, View} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';

const StarRating = (props: any) => {
  // This array will contain our star tags. We will include this
  // array between the view tag.
  let stars = [];
  // Loop 5 times
  for (let i = 1; i <= 5; i++) {
    // set the path to filled stars
    let name = 'ios-star';
    // console.log(props.ratings - i < 0, i - props.ratings > 0.1);
    // If ratings is lower, set the path to unfilled stars
    if (i > props.ratings) {
      name = 'ios-star-outline';
    }
    if (i - props.ratings > 0.1 && i - props.ratings < 1) {
      name = 'ios-star-half-sharp';
    }

    stars.push(<Ionicons name={name} size={15} style={styles.star} key={i} />);
  }

  return (
    <View style={styles.container}>
      {stars}
      {/* <Text style={styles.text}>({props.reviews})</Text> */}
    </View>
  );
};

export default StarRating;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  star: {
    color: '#FF8C00',
  },
  text: {
    fontSize: 12,
    marginLeft: 5,
    color: '#444',
  },
});
