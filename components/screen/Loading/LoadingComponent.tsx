/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ActivityIndicator, Button, SafeAreaView} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';

function LoadingComponent(props: {navigation?: any}) {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: Colors.backgroundGray,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <ActivityIndicator size="large" color="#333333" />
      {props.navigation && (
        <Button onPress={() => props.navigation.goBack()} title="返回" />
      )}
    </SafeAreaView>
  );
}
export default LoadingComponent;
