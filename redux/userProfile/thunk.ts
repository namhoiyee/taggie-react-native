import {api} from '../../helpers/api';
import {
  editedUserProfile,
  failed,
  loadedUserProfile,
  loadedUserProfilePic,
  editedUserProfilePic,
  loading,
} from './action';
// import {updateToken} from '../auth/thunk';

import {IRootThunkDispatch, IRootState} from '../store';
import {Alert} from 'react-native';
import {UserEditable} from './state';
import {ImageUpload} from '../../helpers/types';
// import {loading, loaded} from '../loading/action';

export function loadUserProfile() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/getUserProfile', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result = await res.json();
      console.log('loadUserProfile', result);
      // console.log('loadUserWishTags', result.data.wishTags);
      dispatch(loadedUserProfile(result.data));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取你的資料時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed('failed to get user profile: ' + error.msg + error.message),
      );
      // dispatch(loaded());
      return;
    }
  };
}

export function loadUserProfilePic() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/getUserProfilePic', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result = await res.json();
      // console.log('loadUserWishTags', result.data.wishTags);
      console.log('loadedUserProfilePic', result.data);
      dispatch(loadedUserProfilePic(result.data));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取你的資料時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed('failed to get user profile: ' + error.msg + error.message),
      );
      // dispatch(loaded());
      return;
    }
  };
}

export function editUserProfile(userProfile: UserEditable) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userOriginalProfile = getState().userProfile.userProfile;
      if (!userOriginalProfile) {
        throw new Error('no original user profile');
      }
      if (
        userOriginalProfile.info.username === userProfile.username &&
        userOriginalProfile.info.birthday === userProfile.birthday &&
        userOriginalProfile.info.text_description ===
          userProfile.text_description &&
        userOriginalProfile.info.gender === userProfile.gender.DB
      ) {
        return;
      }
      console.log('userProfile', userProfile);
      const res = await api.post(
        '/updateProfile',
        {
          gender: userProfile.gender.DB,
          birthday: userProfile.birthday,
          username: userProfile.username,
          text: userProfile.text_description,
        },
        userToken,
      );
      if (res.status !== 202) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {
          profile: {
            gender: string;
            birthday: Date;
            username: string;
            text_description: string;
            userId: number;
          };
          msg: 'please update token';
        };
      } = await res.json();
      console.log('result', result.data?.profile);

      dispatch(
        editedUserProfile(result.isSuccess, {
          info: {
            username: result.data.profile.username,
            email: userOriginalProfile.info.email,
            coins: userOriginalProfile.info.coins,
            text_description: result.data.profile.text_description,
            gender: result.data.profile.gender,
            birthday: result.data.profile.birthday,
          },
          matchNum: userOriginalProfile.matchNum,
        }),
      );
      console.log('edit user profile update token');
      // dispatch(updateToken());
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert(
      //   '編輯個人資料時失敗',
      //   '請稍後重試' + error.message + error.msg,
      //   [{text: '確認', style: 'default'}],
      // );
      dispatch(
        failed('failed to edit user profile: ' + error.message + error.msg),
      );
      // dispatch(loaded());
      return;
    }
  };
}

export function editUserProfilePic(userProfilePic: ImageUpload[]) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const formData = new FormData();
      formData.append('originalProfile', userProfilePic[0]);
      formData.append('profile', userProfilePic[1]);
      // console.log(formData._parts);
      // console.log(userToken);
      const res = await api.post('/uploadProfilePic', formData, userToken);
      if (res.status === 406) {
        Alert.alert('無法辨認你', '請重新選擇相片', [
          {text: '確認', style: 'default'},
        ]);
        dispatch(failed('failed to insert profile pic: '));
        return;
      }
      if (res.status !== 201) {
        const error = await res.json();
        console.log('edit profile pic Error', error);
        throw error;
      }
      const result = await res.json();
      console.log(
        'edit profile pic success',
        result.data.profiles.userProfilePic,
      );
      dispatch(
        editedUserProfilePic(result.isSuccess, {
          userProfilePic: result.data.profiles.userProfilePic,
        }),
      );
      // dispatch(updateToken());
      // dispatch(loaded());
      // Alert.alert('修改icon', '成功', [{text: '確認', style: 'default'}]);
    } catch (error) {
      // Alert.alert('修改個人相片失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);

      dispatch(failed('failed to insert profile pic: ' + error.message));
      // dispatch(loaded());
      return;
    }
  };
}
