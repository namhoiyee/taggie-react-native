import {TagClass, UserTags, UserWishTags} from '../../helpers/types';

export function loadedUserTags(userTags: UserTags) {
  return {
    type: '@@Tags/loadedUserTags' as const,
    userTags,
  };
}

export function loadedUserWishTags(userWishTags: UserWishTags) {
  return {
    type: '@@Tags/loadedUserWishTags' as const,
    userWishTags,
  };
}

export function loadedAllClassesAndTags(tagClassesAndTags: TagClass[]) {
  return {
    type: '@@Tags/loadedAllClassesAndTags' as const,
    tagClassesAndTags,
  };
}

export function editedWishTags(isSuccess: boolean, userWishTags: UserWishTags) {
  return {
    type: '@@Tags/editedWishTags' as const,
    isSuccess,
    userWishTags,
  };
}

export function editedUserTags(isSuccess: boolean, userTags: UserTags) {
  return {
    type: '@@Tags/editedUserTags' as const,
    isSuccess,
    userTags,
  };
}

export function addedWishTags(isSuccess: boolean, userWishTags: UserWishTags) {
  return {
    type: '@@Tags/addedWishTags' as const,
    isSuccess,
    userWishTags,
  };
}

export function addedUserTags(isSuccess: boolean, userTags: UserTags) {
  return {
    type: '@@Tags/addedUserTags' as const,
    isSuccess,
    userTags,
  };
}

export function deletedWishTags(
  isSuccess: boolean,
  userWishTags: UserWishTags,
) {
  return {
    type: '@@Tags/deletedWishTags' as const,
    isSuccess,
    userWishTags,
  };
}

export function deletedUserTags(isSuccess: boolean, userTags: UserTags) {
  return {
    type: '@@Tags/deletedUserTags' as const,
    isSuccess,
    userTags,
  };
}

export function loading() {
  return {
    type: '@@Tags/loading' as const,
  };
}

export function failed(msg: string) {
  console.log('tag', msg);
  return {
    type: '@@Tags/failed' as const,
    msg,
  };
}

export type TagsAction =
  | ReturnType<typeof loadedUserTags>
  | ReturnType<typeof loadedUserWishTags>
  | ReturnType<typeof loadedAllClassesAndTags>
  | ReturnType<typeof editedWishTags>
  | ReturnType<typeof addedWishTags>
  | ReturnType<typeof deletedWishTags>
  | ReturnType<typeof editedUserTags>
  | ReturnType<typeof addedUserTags>
  | ReturnType<typeof deletedUserTags>
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>;
