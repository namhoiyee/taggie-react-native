import React from 'react';
import {StyleSheet, View, Text, Pressable} from 'react-native';
// import {WebView} from 'react-native-webview';

import {MARGIN, SIZE} from './Config';

interface TileProps {
  id: string;
  uri: string;
  onLongPress: () => void;
}

const Tile = ({id, onLongPress}: TileProps) => {
  return (
    <Pressable
      style={styles.container}
      pointerEvents="none"
      onLongPress={onLongPress}>
      <View style={styles.wrapper}>
        <Text>{id}</Text>
        {/* <WebView
        source={{uri}}
        style={{flex: 1, margin: MARGIN * 2, borderRadius: MARGIN}}
      /> */}
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: SIZE,
    height: SIZE,
    backgroundColor: 'transparent',
  },
  wrapper: {
    width: '90%',
    height: '90%',
    margin: MARGIN,
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 20,
  },
});

export default Tile;
