import 'react-native-gesture-handler';
import React from 'react';
import {LogBox} from 'react-native';
import store from './redux/store';
import {Provider} from 'react-redux';
import AppNavigator from './components/navigator/AppNavigator';

LogBox.ignoreAllLogs();

const App = () => {
  return (
    <>
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    </>
  );
};

export default App;
