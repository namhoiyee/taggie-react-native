import {ChatroomDetail, ChatroomMessage} from './state';

export function loadedChatroom(chatroom: ChatroomDetail[]) {
  return {
    type: '@@Chatroom/loadedChatroom' as const,
    chatroom,
  };
}

export function updatedUnreadCount(unread: number) {
  return {
    type: '@@Chatroom/updatedUnreadCount' as const,
    unread,
  };
}

export function gotUnreadCount(unread: number) {
  return {
    type: '@@Chatroom/gotUnreadCount' as const,
    unread,
  };
}

export function loadedChatroomMessages(messages: ChatroomMessage[]) {
  return {
    type: '@@Chatroom/loadedChatroomMessages' as const,
    messages,
  };
}

export function postedChatroomMessage() {
  return {
    type: '@@Chatroom/postedChatroomMessage' as const,
    msg: 'posted',
  };
}

export function readMessagesAlready(unread: number) {
  return {
    type: '@@Chatroom/readMessagesAlready' as const,
    unread,
  };
}

export function wentOutFromChatroom() {
  return {
    type: '@@Chatroom/wentOutFromChatroom' as const,
  };
}

export function loadingChatrooms() {
  return {
    type: '@@Chatroom/loadingChatrooms' as const,
  };
}

export function updatedRoomId(currentRoomId: string) {
  return {
    type: '@@Chatroom/updatedRoomId' as const,
    currentRoomId,
  };
}

export function failed(msg: string) {
  console.log('chatroom failed', msg);
  return {
    type: '@@Chatroom/failed' as const,
    msg,
  };
}

export type ChatroomAction =
  | ReturnType<typeof loadedChatroom>
  | ReturnType<typeof loadedChatroomMessages>
  | ReturnType<typeof postedChatroomMessage>
  | ReturnType<typeof updatedUnreadCount>
  | ReturnType<typeof gotUnreadCount>
  | ReturnType<typeof readMessagesAlready>
  | ReturnType<typeof wentOutFromChatroom>
  | ReturnType<typeof updatedRoomId>
  | ReturnType<typeof loadingChatrooms>
  | ReturnType<typeof failed>;
