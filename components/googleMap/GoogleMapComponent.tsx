import React from 'react';
import {StyleSheet, Animated, Dimensions, Platform} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';

// import {markers} from './mapData';
import {mapDarkStyle, mapStandardStyle} from './googleMapStyle';

import {useTheme} from '@react-navigation/native';

const {width} = Dimensions.get('window');
const CARD_WIDTH = width * 0.8;

const markers = [{id: 0}];

const GoogleMapComponent = (props: {location: {x: number; y: number}}) => {
  const theme = useTheme();

  const initialMapState = {
    markers,
    region: {
      latitude: props?.location?.y,
      longitude: props?.location?.x,
      latitudeDelta: 0.004864195044303443,
      longitudeDelta: 0.0040142817690068,
    },
  };

  const [state] = React.useState(initialMapState);

  let mapAnimation = new Animated.Value(0);

  const interpolations = state.markers.map((marker, index) => {
    const inputRange = [
      (index - 1) * CARD_WIDTH,
      index * CARD_WIDTH,
      (index + 1) * CARD_WIDTH,
    ];

    const scale = mapAnimation.interpolate({
      inputRange,
      outputRange: [1, 1.5, 1],
      extrapolate: 'clamp',
    });

    return {scale};
  });

  const _map = React.useRef(null);

  return (
    // <View style={styles.container}>
    <MapView
      ref={_map}
      initialRegion={state.region}
      style={styles.container}
      provider={PROVIDER_GOOGLE}
      customMapStyle={theme.dark ? mapDarkStyle : mapStandardStyle}>
      {state.markers.map((marker, index) => {
        const scaleStyle = {
          transform: [
            {
              scale: interpolations[index].scale,
            },
          ],
        };
        return (
          <Marker
            key={index}
            coordinate={{
              latitude: props?.location?.y,
              longitude: props?.location?.x,
            }}
            onPress={() => {}}>
            <Animated.View style={[styles.markerWrap]}>
              <Animated.Image
                source={require('../../src/image/map_marker.png')}
                style={[styles.marker, scaleStyle]}
                resizeMode="cover"
              />
            </Animated.View>
          </Marker>
        );
      })}
    </MapView>
    // </View>
  );
};

export default GoogleMapComponent;

const styles = StyleSheet.create({
  container: {
    height: 300,
    width: '100%',
    borderRadius: 20,
  },
  searchBox: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  chipsScrollView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 90 : 80,
    paddingHorizontal: 10,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  cardImage: {
    flex: 3,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  textContent: {
    flex: 2,
    padding: 10,
  },
  cardtitle: {
    fontSize: 12,
    // marginTop: 5,
    fontWeight: 'bold',
  },
  cardDescription: {
    fontSize: 12,
    color: '#444',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
  },
  marker: {
    width: 30,
    height: 30,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
  },
  signIn: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
});
