import {FriendProfile, ILocation} from '../../helpers/types';

export function loadFriendProfile(
  friendProfile: FriendProfile,
  coordinate: ILocation,
  text_description: string,
) {
  return {
    type: '@@FriendProfile/loadFriendProfile' as const,
    friendProfile,
    coordinate,
    text_description,
  };
}

export function failed(msg: string) {
  console.log('failed to load friend profile', msg);
  return {
    type: '@@FriendProfile/failed' as const,
    msg,
  };
}

export function loaded() {
  console.log('loaded friend profile');
  return {
    type: '@@FriendProfile/loaded' as const,
  };
}

export function loading() {
  console.log('loading friend profile');
  return {
    type: '@@FriendProfile/loading' as const,
  };
}

export type FriendProfileAction =
  | ReturnType<typeof loadFriendProfile>
  | ReturnType<typeof failed>
  | ReturnType<typeof loaded>
  | ReturnType<typeof loading>;
