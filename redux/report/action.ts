export function submitReport() {
  return {
    type: '@@Report/submit_report' as const,
  };
}

export function resumeNormal() {
  return {
    type: '@@Report/resume_normal' as const,
  };
}

export type ReportAction =
  | ReturnType<typeof submitReport>
  | ReturnType<typeof resumeNormal>;
