import React, {useState} from 'react';
import {
  Animated,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  ListRenderItemInfo,
} from 'react-native';

import {SwipeListView} from 'react-native-swipe-list-view';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';

import Tags, {Tag} from '../../model/tags';

const rowTranslateAnimatedValues: any = {};
Tags.forEach(tag => {
  rowTranslateAnimatedValues[`${tag.id}`] = new Animated.Value(1);
});

export default function SwipeToDelete() {
  const [listData, setListData] = useState(
    Tags.map(tag => ({
      key: `${tag.id}`,
      id: `${tag.id}`,
      tag: tag.tag,
      category: tag.category,
    })),
  );

  const onSwipeValueChange = (swipeData: {
    key: string;
    value: number;
    direction: 'left' | 'right';
    isOpen: boolean;
  }) => {
    const {key, value} = swipeData;
    if (value < -Dimensions.get('window').width * 0.85) {
      // setListData(prevData =>
      //   prevData.filter(item => {
      //     console.log(item.id, key);
      //     return item.id !== key;
      //   }),
      // );
      const newData = [...listData];
      const prevIndex = listData.findIndex(item => {
        console.log(item.id, key);
        item.id === key;
      });
      newData.splice(prevIndex, 1);
      setListData(newData);
    }
  };

  const renderItem = (data: ListRenderItemInfo<Tag>) => (
    <Animated.View
      style={[
        // styles.rowFrontContainer,
        {
          height: rowTranslateAnimatedValues[data.item.id].interpolate({
            inputRange: [0, 1],
            outputRange: [0, 50],
          }),
        },
      ]}>
      <TouchableHighlight
        onPress={() => console.log('You touched me')}
        style={styles.rowFront}
        underlayColor={'#AAA'}>
        <View>
          <Text>I am {data.item.tag} in a SwipeListView</Text>
        </View>
      </TouchableHighlight>
    </Animated.View>
  );

  const renderHiddenItem = () => (
    <View style={styles.rowBack}>
      <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
        <Text style={styles.backTextWhite}>Delete</Text>
      </View>
    </View>
  );

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View style={[styles.container, {marginBottom: tabBarHeight}]}>
          <SwipeListView
            disableRightSwipe
            data={listData}
            renderItem={renderItem}
            renderHiddenItem={renderHiddenItem}
            rightOpenValue={/* -Dimensions.get('window').width */ -150}
            // previewRowKey={'0'}
            // previewOpenValue={-40}
            // previewOpenDelay={3000}
            onSwipeValueChange={onSwipeValueChange}
            useNativeDriver={false}
          />
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#CCC',
    flex: 1,
  },
  backTextWhite: {
    color: '#FFF',
  },
  rowFront: {
    alignItems: 'center',
    backgroundColor: '#CCC',
    // borderBottomColor: 'transparent',
    // borderBottomWidth: 10,
    justifyContent: 'center',
    height: 50,
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 0.99,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
  },
  backRightBtnRight: {
    backgroundColor: 'red',
    right: 0,
  },
});
