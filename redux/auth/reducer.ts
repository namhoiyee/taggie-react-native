import {AuthState, initialState} from './state';
import {AuthAction} from './action';
import {JWTPayload} from '../../helpers/types';
import jwt_decode from 'jwt-decode';
// import {checkRole} from './helpers';

export const authReducer = (
  state: AuthState = initialState,
  action: AuthAction,
): AuthState => {
  switch (action.type) {
    case '@@Auth/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Auth/logout':
      return {
        ...state,
        status: 'logout',
        user: undefined,
        token: undefined,
      };
    case '@@Auth/load_token': {
      try {
        const payload: JWTPayload = jwt_decode(action.token);
        return {
          ...state,
          status: 'ready',
          user: payload,
          token: action.token,
        };
      } catch (error) {
        return {
          ...state,
          status: 'error',
          msg: 'invalid JWT Token',
        };
      }
    }
    case '@@Auth/updatedToken': {
      const payload: JWTPayload = jwt_decode(action.token);
      console.log('updated user', payload);
      return {
        ...state,
        status: 'updatedToken',
        user: payload,
        token: action.token,
      };
    }
    case '@@Auth/confirmedToBeRegStatThree': {
      return {
        ...state,
        status: 'confirmedToBeRegStatThree',
      };
    }
    case '@@Auth/loading':
      return {
        ...state,
        status: 'loading',
      };
    default:
      return state;
  }
};
