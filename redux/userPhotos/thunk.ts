import {api} from '../../helpers/api';
import {
  failed,
  loadedUserPhotos,
  insertedUserPhotos,
  deletedUserPhotos,
  loading,
} from './action';
import {IRootThunkDispatch, IRootState} from '../store';
import {Alert} from 'react-native';
import {ImageUpload, Photo} from '../../helpers/types';
// import {loading, loaded} from '../loading/action';

export function loadUserPhotos() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/getGallery', userToken);
      if (res.status !== 200) {
        throw new Error(await res.json());
      }
      const result = await res.json();
      dispatch(loadedUserPhotos(result));
      // dispatch(loaded());
    } catch (error) {
      // Alert.alert('讀取相簿失敗', '請稍後重試' + error, [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to get gallery: ' + error.msg + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function insertGallery(photo: ImageUpload) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userGallery = getState().userPhotos.userGallery?.data;
      const formData = new FormData();
      formData.append('photo', photo);
      // console.log(formData._parts);
      // console.log(userToken);
      const res = await api.post('/addPhotos', formData, userToken);
      if (res.status !== 201) {
        const error = await res.json();
        console.log('insertError', error);
        throw error;
      }
      const result: {data: {id: number; url: string}[]} = await res.json();
      // console.log('insert result', result);
      dispatch(
        insertedUserPhotos(
          userGallery ? {data: [...userGallery, ...result.data]} : result,
        ),
      );
      // dispatch(loaded());
    } catch (error) {
      if (error && error.msg === 'exceeded photos number') {
        Alert.alert('修改相簿失敗', '不能上傳超過9張照片', [
          {text: '確認', style: 'default'},
        ]);
      } /* else {
        Alert.alert('修改相簿失敗', '請稍後重試', [
          {text: '確認', style: 'default'},
        ]);
      } */

      dispatch(failed('failed to insert gallery: ' + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function deletePhoto(photo: Photo) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const userGallery = getState().userPhotos.userGallery?.data;
      // console.log(formData._parts);
      // console.log(userToken);
      const res = await api.delete('/delPhotos', {photos: [photo]}, userToken);
      if (res.status !== 201) {
        const error = await res.json();
        console.log('deleteError', error);
        throw error;
      }
      const result: {data: number[]} = await res.json();
      console.log('delete result', result.data);
      dispatch(
        deletedUserPhotos(
          userGallery
            ? {
                data: userGallery.filter(
                  onePhoto => !result.data.includes(onePhoto.id),
                ),
              }
            : {data: [{id: 0, url: 'bug'}]},
        ),
      );
      // dispatch(loaded());
      Alert.alert('刪除相簿', '成功', [{text: '確認', style: 'default'}]);
    } catch (error) {
      // Alert.alert('刪除相簿失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);

      dispatch(
        failed('failed to delete gallery: ' + error.msg + error.message),
      );
      // dispatch(loaded());
      return;
    }
  };
}
