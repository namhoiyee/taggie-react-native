/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';

export default function UserSettingScreen() {
  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View style={[styles.container, {marginBottom: tabBarHeight}]}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.titleBar}>
              <Ionicons name="ios-arrow-back" size={24} color="#52575d" />
              <MaterialIcons name="more-vert" size={24} color="#52575d" />
            </View>
            <View style={styles.alignSelf}>
              <View style={styles.profileImage}>
                <Image source={require('../../src/image/template.jpg')} />
              </View>
              <View style={styles.dm}>
                <MaterialIcons name="chat" size={18} color="#DFD8C8" />
              </View>
              <View style={styles.active} />
              <View style={styles.add}>
                <Ionicons
                  name="ios-add"
                  size={48}
                  color="#DFD8C8"
                  style={styles.smallIconMargin}
                />
              </View>
            </View>

            <View style={styles.infoContainer}>
              <Text style={[styles.text, styles.name]}>Julie</Text>
              <Text style={[styles.text, styles.userDetail]}>Photographer</Text>
            </View>

            <View style={styles.statsContainer}>
              <View style={styles.statsBox}>
                <Text style={[styles.text, styles.detailText]}>483</Text>
                <Text style={[styles.text, styles.subText]}>Posts</Text>
              </View>
              <View style={[styles.statsBox, styles.followBorder]}>
                <Text style={[styles.text, styles.detailText]}>45,844</Text>
                <Text style={[styles.text, styles.subText]}>Followers</Text>
              </View>
              <View style={[styles.statsBox]}>
                <Text style={[styles.text, styles.detailText]}>302</Text>
                <Text style={[styles.text, styles.subText]}>Following</Text>
              </View>
            </View>
            <View style={styles.marginSmall}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View style={styles.mediaImageContainer}>
                  <Image
                    source={require('../../src/image/template2.jpg')}
                    resizeMode="cover"
                    style={styles.image}
                  />
                </View>
                <View style={styles.mediaImageContainer}>
                  <Image
                    source={require('../../src/image/template2.jpg')}
                    resizeMode="cover"
                    style={styles.image}
                  />
                </View>
                <View style={styles.mediaImageContainer}>
                  <Image
                    source={require('../../src/image/template2.jpg')}
                    resizeMode="cover"
                    style={styles.image}
                  />
                </View>
              </ScrollView>
              <View style={styles.mediaCount}>
                <Text style={[styles.text, styles.mediaText]}>70</Text>
                <Text style={[styles.text, styles.mediaTitle]}>Media</Text>
              </View>
            </View>

            <Text style={[styles.subText, styles.recent]}>Recent Activity</Text>

            <View style={{alignItems: 'center'}}>
              <View style={styles.recentItem}>
                <View style={styles.recentItemIndicator} />
                <View style={{width: 250}}>
                  <Text
                    style={[
                      styles.text,
                      {color: '#41444B', fontWeight: '300'},
                    ]}>
                    Started following{' '}
                    <Text style={{fontWeight: '400'}}>
                      Jake Chhhhh and{' '}
                      <Text style={{fontWeight: '400'}}>DesignIntoCode</Text>
                    </Text>
                  </Text>
                </View>
              </View>

              <View style={styles.recentItem}>
                <View style={styles.recentItemIndicator} />
                <View style={{width: 250}}>
                  <Text
                    style={[
                      styles.text,
                      {color: '#41444B', fontWeight: '300'},
                    ]}>
                    Started following{' '}
                    <Text style={{fontWeight: '400'}}>Cake Chhhh</Text>
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: '#ffffff',
  },
  titleBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 24,
    marginHorizontal: 16,
  },
  alignSelf: {
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'HelveticaNeue',
    color: '#52575d',
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 100,
    overflow: 'hidden',
  },
  dm: {
    backgroundColor: '#41444B',
    position: 'absolute',
    top: 20,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  active: {
    backgroundColor: '#34FFB9',
    position: 'absolute',
    bottom: 28,
    left: 10,
    padding: 4,
    height: 20,
    width: 20,
    borderRadius: 10,
  },
  add: {
    backgroundColor: '#41444B',
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  smallIconMargin: {
    marginTop: 6,
    marginLeft: 2,
  },
  infoContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 16,
  },
  name: {
    fontWeight: '200',
    fontSize: 36,
  },
  userDetail: {
    color: '#AEB5BC',
    fontSize: 14,
  },
  statsContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 32,
  },
  statsBox: {
    alignItems: 'center',
    flex: 1,
  },
  detailText: {
    fontSize: 24,
  },
  subText: {
    fontSize: 12,
    color: '#AEB5BC',
    textTransform: 'uppercase',
    fontWeight: '500',
  },
  followBorder: {
    borderColor: '#DFD8C8',
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },
  marginSmall: {
    marginTop: 32,
  },
  mediaImageContainer: {
    width: 180,
    height: 200,
    borderRadius: 12,
    overflow: 'hidden',
    marginHorizontal: 10,
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  mediaCount: {
    backgroundColor: '#41444B',
    position: 'absolute',
    top: '50%',
    marginTop: -50,
    marginLeft: 30,
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
    shadowColor: 'rgba(0,0,0,0.38)',
    shadowOffset: {width: 0, height: 10},
    shadowRadius: 20,
    shadowOpacity: 1,
  },
  mediaText: {
    fontSize: 24,
    color: '#DFD8C8',
    fontWeight: '300',
  },
  mediaTitle: {
    fontSize: 12,
    color: '#DFD8C8',
    textTransform: 'uppercase',
  },
  recent: {
    marginLeft: 78,
    marginTop: 32,
    marginBottom: 6,
    fontSize: 10,
  },
  recentItem: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
  },
  recentItemIndicator: {
    backgroundColor: '#CABFAB',
    padding: 4,
    height: 12,
    width: 12,
    borderRadius: 6,
    marginTop: 3,
    marginRight: 20,
  },
});
