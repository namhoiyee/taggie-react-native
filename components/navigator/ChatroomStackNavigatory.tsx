import React from 'react';
import {useTheme} from 'react-native-paper';
import {createStackNavigator} from '@react-navigation/stack';
import ChatroomScreen from '../screen/ChatroomScreen';

const ChatroomStack = createStackNavigator();

export const ChatroomStackNavigator = ({_navigation}: any) => {
  const {colors} = useTheme();

  return (
    <ChatroomStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#efefef',
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
      }}>
      <ChatroomStack.Screen
        name="Chatroom"
        component={ChatroomScreen}
        options={{
          title: '好友列表',
          }}
      />
    </ChatroomStack.Navigator>
  );
};
