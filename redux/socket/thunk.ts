import {REACT_APP_API_SERVER} from '../../helpers/api';
import {failed, loadedSocket, loading} from './action';
import {IRootThunkDispatch, IRootState} from '../store';
import io from 'socket.io-client';

export function loadSocket() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    const userToken = getState().auth?.token;
    const socket = io(REACT_APP_API_SERVER, {
      auth: {
        token: userToken,
      },
      path: '/chat',
    });
    if (socket) {
      dispatch(loadedSocket(socket));
    } else {
      dispatch(failed('fail to load socket'));
    }
  };
}
