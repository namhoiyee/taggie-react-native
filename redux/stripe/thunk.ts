import {failed, resetStripe, cancel, paymentSucceed} from './action';
import {IRootState, IRootThunkDispatch} from '../store';
import {Alert} from 'react-native';
import {api, REACT_APP_API_SERVER} from '../../helpers/api';
import {loading} from '../stripe/action';
import {editCoins, loadedUserProfile} from '../userProfile/action';

// export type WhoLikeUInfo = {
//   id: number;
//   match_coordinates: {x: number; y: number};
//   user_id: number;
//   username: string;
//   profile: string;
//   oProfile: string;
// };

export function payment(
  creditCardData: any,
  coinsAmount: number,
  navigation: any,
) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    const userToken = getState().auth.token;

    const STRIPE_PUBLISHABLE_KEY =
      'pk_test_51J56IKCOH5HFpq1vXG6FxtiTwJIbZuZHpn3PeVfJ3ElnE75eos5Co2ETSI2Dsi07bo5QBTBVVyHAlauKHDlNP8s600gYopQv0T';

    let cardMap = new Map();
    cardMap.set('card[number]', creditCardData.values.number.replace(/ /g, ''));
    cardMap.set('card[exp_month]', creditCardData.values.expiry.split('/')[0]);
    cardMap.set('card[exp_year]', creditCardData.values.expiry.split('/')[1]);
    cardMap.set('card[cvc]', creditCardData.values.cvc);
    let form: string[] = [];
    cardMap.forEach(makeForm);
    function makeForm(value: string, key: string) {
      form.push(key + '=' + value);
    }
    let formBody = form.join('&');
    try {
      let token = await fetch('https://api.stripe.com/v1/tokens', {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`,
        },
        method: 'post',
        body: formBody,
      });
      let result = await token.json();
      console.log('token', result);
      if (!result.id) {
        dispatch(failed('no token'));
        Alert.alert('信用卡輸入錯誤', '請重新輸入', [
          {
            text: '重試',
            onPress: () => {
              dispatch(resetStripe());
            },
          },
          {
            text: '取消',
            onPress: () => {
              dispatch(cancel());
            },
            style: 'cancel',
          },
        ]);
      }
      let paymentRes = await api.post(
        `${REACT_APP_API_SERVER}/charge`,
        {coins: coinsAmount, cardToken: result.id},
        userToken,
      );
      let paymentResult = await paymentRes.json();
      console.log('payment', paymentResult);
      if (!paymentResult.isSuccess) {
        dispatch(failed('unable to make payment' + paymentResult.msg));
        Alert.alert('未能完成付款', '請再試一次', [
          {
            text: '重試',
            onPress: () => {
              dispatch(resetStripe());
            },
          },
          {
            text: '取消',
            onPress: () => {
              dispatch(cancel());
            },
            style: 'cancel',
          },
        ]);
      } else {
        dispatch(paymentSucceed(paymentResult.data));
        dispatch(editCoins(paymentResult.data));
        Alert.alert('多謝購買Taggie幣', '', [
          {
            text: '確認',
            onPress: () => {
              dispatch(resetStripe());
              navigation.goBack();
            },
          },
        ]);
        if (getState().userProfile.status === 'editedUserProfile') {
          dispatch(loadedUserProfile(getState().userProfile.userProfile!));
        }
      }
    } catch (error) {
      dispatch(failed(error));
      Alert.alert('未能完成付款', '請再試一次', [
        {
          text: '重試',
          onPress: () => {
            dispatch(resetStripe());
          },
        },
        {
          text: '取消',
          onPress: () => {
            dispatch(cancel());
          },
          style: 'cancel',
        },
      ]);
      console.log(error);
    }
  };
}
