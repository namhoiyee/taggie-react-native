import {api} from '../../helpers/api';
import {IRootState, IRootThunkDispatch} from '../store';
import {failed, loading} from './action';
import {Alert} from 'react-native';
import {updateToken} from '../auth/thunk';
import {editedUserProfile} from '../userProfile/action';
// import {loading, loaded} from '../loading/action';
// import {ImageUpload} from '../../helpers/types';
// import {updatedToken} from '../auth/action';
// import AsyncStorage from '@react-native-async-storage/async-storage';

export function submitQuestionnaireThunk(input: {
  gender: '' | 'male' | 'female' | '其他';
  birthday: Date | null;
  smoke: '' | '常吸煙' | '少吸煙' | '唔吸煙';
  drink: '' | '常飲酒' | '少飲酒' | '唔飲酒';
  textDescription: string;
}) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      console.log('input: ', input);
      let token = getState().auth.token;
      const user = getState().auth.user;
      const userProfile = getState().userProfile.userProfile;
      let res = await api.post('/submitQuestionnaire', input, token);
      if (res.status !== 200) {
        throw await res.json();
      }
      Alert.alert('提交成功', '最後一步，揀返張靚相', [
        {text: '確認', style: 'default'},
      ]);
      // dispatch(updateRegistrationStatus(2))
      Promise.all([
        dispatch(updateToken()),
        dispatch(
          editedUserProfile(true, {
            info: {
              username: user?.username!,
              email: userProfile?.info.email!,
              coins: userProfile?.info.coins!,
              text_description: input.textDescription,
              gender: input.gender,
              birthday: input.birthday!,
            },
            matchNum: 0,
          }),
        ),
      ]);
      // dispatch(loaded());
    } catch (e) {
      dispatch(failed('submitQuestionnaire failed' + e.msg + e.message));
      // dispatch(loaded());
      Alert.alert('提交失敗', '請檢查資料有否遺漏', [
        {text: '確認', style: 'default'},
      ]);
      return;
    }
  };
}

// export function submitFirstPicThunk(userProfilePic: ImageUpload[]) {
//   return async (dispatch: IRootThunkDispatch) => {
//     dispatch(loading());
//     try {
//       console.log('userProfilePic: ', userProfilePic);
//       let res = await api.post('/submitFirstPic', userProfilePic);
//       if (res.status !== 200) {
//         const error = await res.json();
//         console.log('edit profile pic Error', error);
//         throw error;
//       }
//       const result = await res.json();
//       if (result.token) {
//         dispatch(updatedToken(result.token));
//         await AsyncStorage.setItem('token', result.token);
//         Alert.alert('提交成功', '即將為你進行配對!', [
//           {text: '確認', style: 'default'},
//         ]);
//       }
//     } catch (e) {
//       dispatch(failed('submitFirstPic failed' + e.msg + e.message));
//       Alert.alert('提交失敗', '請檢查資料有否遺漏', [
//         {text: '確認', style: 'default'},
//       ]);
//       return;
//     }
//   };
// }
