import {initialState, MapState} from './state';
import {MapAction} from './action';
import {ILocation, MarkerInfo} from '../../helpers/types';

export const mapReducer = (
  state: MapState = initialState,
  action: MapAction,
): MapState => {
  switch (action.type) {
    case '@@Map/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Map/whoLikeYouSocket':
      let userInfo = action.userInfo;
      let location: ILocation = {
        latitude: userInfo.match_coordinates.y,
        longitude: userInfo.match_coordinates.x,
      };
      console.log('in dispatch who like U');
      let newMarker: MarkerInfo = {
        coordinate: location,
        username: userInfo.username,
        profilePic: userInfo.profile,
        originalPic: userInfo.oProfile,
        userId: userInfo.user_id,
      };
      const whoLikeU = state.whoLikeU
        ? [...state.whoLikeU, newMarker]
        : [newMarker];
      return {
        ...state,
        status: 'ready',
        whoLikeU: whoLikeU,
      };
    case '@@Map/putMarker':
      return {
        ...state,
        status: 'ready',
        whoLikeU: action.userMarker,
        region: action.region,
      };
    case '@@Map/loading':
      return {
        ...state,
        status: 'loading',
      };
    default:
      return state;
  }
};
